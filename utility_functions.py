from datetime import *
import wx
from database import *
import os.path



def pydate2wxdate(dt):
     assert isinstance(dt, (datetime, date))
     tt = dt.timetuple()
     dmy = (tt[2], tt[1]-1, tt[0])
     return wx.DateTimeFromDMY(*dmy)

def GlobalColorPicker(parent, CustomColors=None, SelectedColor=None):
  if CustomColors == None:
    CustomColors = (
        '#FF3E96', '#f97904', '#04f90e', '#148559', '#5a1485', '#080a98')
  data = wx.ColourData()
  data.SetChooseFull(False)

  # set custom Colors
  index = 0
  for color in CustomColors:
    data.SetCustomColour(index, color)
    index += 1

  # set the default color in the chooser
  if SelectedColor != None:
    ret = SelectedColor
    data.SetColour(SelectedColor)
  else:
    ret = '#FF3E96'

  # construct the chooser
  dlg = wx.ColourDialog(parent, data)

  if dlg.ShowModal() == wx.ID_OK:
    # set the panel background color
    newcolor = dlg.GetColourData().Colour
    strcolor = u"#%02X%02X%02X" % (
        newcolor[0], newcolor[1],        newcolor[2])
    ret = strcolor
  dlg.Destroy()

  return ret


def ReportDirectory(parent):
    try:
        thisSetting = settings.query(Settings).filter_by(
            name='Report_Path').one()
    except NoResultFound:
        filename = os.path.join(os.path.expanduser('~'), 'Desktop')
        filename = os.path.join(filename, reportname)
        thisSetting.value = filename
    if thisSetting.value is None:
        filename = os.path.join(os.path.expanduser('~'), 'Desktop')
        filename = os.path.join(filename, reportname)
        thisSetting.value = filename

    dlg = wx.DirDialog(parent, "Choose a directory:", defaultPath=thisSetting.value,
                       style=wx.DD_DEFAULT_STYLE
                       #| wx.DD_DIR_MUST_EXIST
                       #| wx.DD_CHANGE_DIR
                       )
    if dlg.ShowModal() == wx.ID_OK:
        thisSetting.value = dlg.GetPath()
    dlg.Destroy()

def reportsaveDir():
    #try to load reprot directory from database
    try:
        report_path = settings.query(Settings).filter_by(name='Report_Path').one()
        report_path = report_path.value
    except:
        MessageClass.Message('Unable to load Report Save directory.\nUsing Desktop.', Caption='Error', Type=MESS_PANIC)
        report_path = os.path.join(os.path.expanduser('~'), 'Desktop')
    print report_path
    return report_path

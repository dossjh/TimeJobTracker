import wx
import re
from sqlalchemy import or_

from database import *
from DatabaseWindow import *
from utility_functions import pydate2wxdate
from message_class import *

JOBWIN_LISTBOX_WIDTH = 300
JOBWIN_DATE_WIDTH = 70
JOBWIN_ACTIVE_WIDTH = 45

class AddClientWindow(wx.Dialog):

  def __init__(self, parent, Client_id = None):
    super(AddClientWindow, self).__init__(
        parent, style=wx.CAPTION, title='Add Client')
    self.Client_id = Client_id
    if Client_id != None:
      Cheif_Obj = session.query(Client).filter_by(id=Client_id).first()
      Cheif_Name = Cheif_Obj.name
      Cheif_Active = Cheif_Obj.active
    else:
      Cheif_Name = ''
      Cheif_Active = True

    self.panel = wx.Panel(self, wx.ID_ANY)
    vbox = wx.BoxSizer(wx.VERTICAL)
    entrybox = wx.BoxSizer(wx.HORIZONTAL)
    buttonbox = wx.BoxSizer(wx.HORIZONTAL)
    editlbl = wx.StaticText(self.panel, label="Name:")
    self.editname = wx.TextCtrl(self.panel, value=Cheif_Name, size=(140, -1))
    self.activeBox = wx.CheckBox(self.panel, label='Active?')
    if Cheif_Active:
      self.activeBox.SetValue(True)
    cancelbutton = wx.Button(self.panel, wx.ID_CANCEL, "&Cancel")
    savebutton = wx.Button(self.panel, wx.ID_OK, "&SAVE")

    entrybox.Add(editlbl, 0, wx.CENTER | wx.ALL, 5)
    entrybox.Add(self.editname, 0, wx.CENTER | wx.ALL, 5)
    entrybox.Add(self.activeBox, 0, wx.CENTER)

    buttonbox.Add(cancelbutton, 0, wx.CENTER, 10)
    buttonbox.Add(savebutton, 0, wx.CENTER, 10)

    vbox.Add(entrybox, 0, wx.CENTER, 0)
    vbox.Add(buttonbox, 0, wx.CENTER, 0)

    self.panel.SetSizer(vbox)
    vbox.Fit(self)
    self.editname.SetFocus()


    self.Bind(wx.EVT_BUTTON, self.cancel, id=wx.ID_CANCEL)
    self.Bind(wx.EVT_BUTTON, self.save, id=wx.ID_OK)


  def save(self, e):
    if self.Client_id == None:
      if len(self.editname.GetValue()) > 0:
        newchief = Client()
        newchief.name = unicode(self.editname.GetValue())
        newchief.active = self.activeBox.GetValue()
        session.add(newchief)
        session.flush()
        session.commit()
        # self.Destory()
      else:
        wx.MessageBox('Information not complete.', 'Error',
                      wx.OK | wx.ICON_ERROR)
    else:
      Cheif_Obj = session.query(Client).filter_by(id=self.Client_id).first()
      Cheif_Obj.name = unicode(self.editname.GetValue())
      Cheif_Obj.active = self.activeBox.GetValue()
      session.commit()


    e.Skip()

  def cancel(self, e):
    session.rollback()
    e.Skip()

class ClientWindow(DatabaseWindow2):
  def __init__(self, parent):

    DataObj = Client
    ColInfo = [{'Column': 0, 'Field':'Client.name', 'Short Field': 'name', 'FieldName': 'Name', 'Type':'Unicode'},
      {'Column': 1, 'Field':'TaskList.active', 'Short Field': 'active', 'FieldName': 'Active', 'Type':'Bool','Width': 70}]
    super(ClientWindow, self).__init__(parent, 'Edit Client', DataObj, ColInfo)

    self.Setup()

  def add(self, e):
    mywin = AddClientWindow(self)
    mywin.CenterOnParent()
    if mywin.ShowModal() == wx.ID_OK:
      self.textEntered(1)


  def EditFunction(self):
    if self.listbox.GetSelectedItemCount() == 1:
      currentItem = self.listbox.GetNextSelected(-1)
      Client_id = self.listbox.GetItemData(currentItem)
      addwin = AddClientWindow(self, Client_id)
      addwin.CenterOnParent()
      if addwin.ShowModal() == wx.ID_OK:
        self.textEntered(1)

  def deleteItem(self, e):
     if self.listbox.GetSelectedItemCount() == 1:
            currentItem = self.listbox.GetNextSelected(-1)
            Job_Id = self.listbox.GetItemData(currentItem)
            results = session.query(Client).join(Jobs).filter(Jobs.client_id == Job_Id).count()
            JobObj = session.query(Client).filter(Client.id == Job_Id).first()

            if results == 0:
                ret = wx.MessageBox('Are you sure you want to delete Client: ' + JobObj.name, 'Comfirm Delete', wx.YES_NO | wx.ICON_QUESTION)
                if ret == wx.YES:
                    session.delete(JobObj)
                    try:
                      session.commit()
                    except:
                        MessageClass.Message('There was an error, deleting the Client', Type=MESS_PANIC)
                    wx.CallAfter(self.textEntered,1)
            else:
                    wx.MessageBox('This Client is associated with Timesheets and cannot be deleted.\nTry marking as Inactive instead.', 'Cannot Delete', wx.OK | wx.ICON_INFORMATION)


     else:
        wx.MessageBox('You can only delete one Client at a time.','Error', wx.OK | wx.ICON_ERROR)

class ManyJobMatches(DatabaseWindow2):
  def __init__(self, parent, JobIdList, Job_Info):

    self.JobIdList = JobIdList
    self.Job_Info = Job_Info
    DataObj = Jobs
    ColInfo = [{'Column': 0, 'Field':'Jobs.name', 'Short Field': 'name', 'FieldName': 'Job', 'Type':'Unicode'},
      {'Column': 1, 'Field':'Jobs.create_date', 'Short Field': 'create_date', 'FieldName': 'Date', 'Type':'Date','Width': 85},
      {'Column': 2, 'Field':'Jobs.active', 'Short Field': 'active', 'FieldName': 'Active', 'Type':'Bool','Width': 70}]
    super(ManyJobMatches, self).__init__(parent, 'Muiltable Job Matches', DataObj, ColInfo)

    self.Setup()

  def MakeQuery(self):
      results = session.query(self.DataObj).order_by(self.order_by)
      results = results.filter(Jobs.id.in_(self.JobIdList))
      return results

  def Setup(self):
        self.panel = wx.Panel(self, wx.ID_ANY)

        topSize = wx.BoxSizer(wx.VERTICAL)
        topRowSizer = wx.BoxSizer(wx.HORIZONTAL)
        dateSizer = wx.BoxSizer(wx.HORIZONTAL)
        middleSizer = wx.BoxSizer(wx.HORIZONTAL)
        midButtonSizer = wx.BoxSizer(wx.VERTICAL)
        textBoxSizer = wx.BoxSizer(wx.HORIZONTAL)
        bottomRowSizer = wx.BoxSizer(wx.HORIZONTAL)
        buttonSizer = wx.BoxSizer(wx.HORIZONTAL)


        self.showInActive = False
        self.order_by = self.DataObj.name


        textTitle = wx.StaticText(self.panel, label="Search:")
        self.textbox = wx.TextCtrl(self.panel, size=(140, -1))

        titleLabel = 'Job you entered: ' + self.Job_Info['Name'] + ' Date: ' + self.Job_Info['Date']

        listTitle = wx.StaticText(self.panel, label=titleLabel)
        # self.listbox = wx.ListBox(self.panel, size=(300, 200))
        self.listbox = wx.ListCtrl(self.panel, size=(self.ListBoxWidth(), 150), style=wx.LC_REPORT| wx.BORDER_SUNKEN)
        # self.listbox = ULC.UltimateListCtrl(self.panel,size=(self.ListBoxWidth, 200), agwStyle = wx.LC_REPORT | wx.LC_VRULES | wx.LC_HRULES)
        infoText = wx.StaticText(self.panel, label='These Jobs are simular to the Job you entered.\nYou can make one active or edit it.\nYou can click Save to add the Job you just entered.')

        topRowSizer.AddStretchSpacer(1)
        topRowSizer.Add(textTitle, 1, wx.LEFT | wx.ALIGN_CENTER, 15)
        topRowSizer.Add(self.textbox, 1, wx.LEFT | wx.ALIGN_CENTER, 10)
        topRowSizer.AddStretchSpacer(3)
        topRowSizer.AddStretchSpacer(1)


        topSize.AddSpacer(10)
        topSize.Add(topRowSizer, 1, 0,0)
        topSize.AddSpacer(10)




        DelButton = wx.Button(self.panel, wx.ID_ANY, 'Delete')
        EditButton = wx.Button(self.panel, wx.ID_ANY, 'Edit')
        InactiveButton = wx.Button(self.panel, wx.ID_ANY, 'Set Active')


        # midButtonSizer.AddStretchSpacer(1)
        # midButtonSizer.AddSpacer(8)
        # midButtonSizer.AddStretchSpacer(1)
        midButtonSizer.Add(DelButton, 1, 0, 0)
        midButtonSizer.AddStretchSpacer(1)
        midButtonSizer.Add(EditButton, 1, 0, 0)
        midButtonSizer.AddStretchSpacer(1)
        midButtonSizer.Add(InactiveButton,1,0,0)

        middleSizer.AddStretchSpacer(1)
        middleSizer.Add(self.listbox, 1, 0, 0)
        middleSizer.AddSpacer(15)
        middleSizer.Add(midButtonSizer, 1, 0, 0)
        middleSizer.AddStretchSpacer(1)

        textBoxSizer.Add((25,0),0,0,0)
        textBoxSizer.Add(infoText, 1, 0, 0)





        topSize.Add(listTitle, 1, wx.LEFT, 25)
        topSize.Add(middleSizer, 1, 0, 0)
        topSize.Add(textBoxSizer, 1, 0,0)
        topSize.AddSpacer(5)

        exportbutton = wx.Button(self.panel, wx.ID_OK, "&Save Entered Job")
        cancelbutton = wx.Button(self.panel, wx.ID_CANCEL, "Do Not Save")
        buttonSizer.AddStretchSpacer(3)
        buttonSizer.Add(cancelbutton, 1, wx.ALIGN_CENTER, 0)
        buttonSizer.AddStretchSpacer(1)
        buttonSizer.Add(exportbutton, 1, wx.ALL | wx.ALIGN_CENTER, 5)
        buttonSizer.AddStretchSpacer(3)

        topSize.AddStretchSpacer(1)
        topSize.Add(buttonSizer, 1, wx.ALL | wx.ALIGN_CENTER, 1)
        topSize.AddStretchSpacer(1)

        self.panel.SetSizer(topSize)
        topSize.Fit(self.panel)
        self.panel.Fit()

        self.textbox.Bind(wx.EVT_TEXT, self.textEntered)
        self.listbox.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.EditJob)
        DelButton.Bind(wx.EVT_BUTTON, self.deleteItem)
        EditButton.Bind(wx.EVT_BUTTON, self.EditJob)
        InactiveButton.Bind(wx.EVT_BUTTON, self.setJobInactive)
        self.listbox.Bind(wx.EVT_LIST_COL_CLICK, self.headerClick)

        for each in self.ColInfo:
          self.listbox.InsertColumn(each['Column'], each['FieldName'])


        results = self.MakeQuery()
        results.all()
        index = 0
        for each in results:
            self.MakeRow(index, each)
            index += 1


        for each in self.ColInfo:
          self.listbox.SetColumnWidth(each['Column'], self.AutoSizeCol(each['Column']))

class AddJobWindow(wx.Dialog):

  def __init__(self, parent):
    super(AddJobWindow, self).__init__(
        parent, style=wx.CAPTION, title='Add Job', size=(300, 150))  # size=(380, 225))

    self.panel = wx.Panel(self, wx.ID_ANY)
    self.parent = parent


    newjobDate = wx.DateTime.Now()
    results = session.query(Client).filter_by(active=True).all()
    client_choices = list()
    for each in results:
        client_choices.append(each.name)
    client_choices.append('none')


    namelabel = wx.StaticText(self.panel, label='Job Name:')
    self.jobname = wx.TextCtrl(self.panel, size=(175, -1))
    datelabel = wx.StaticText(self.panel, label="Start Date:")
    self.jobDate = wx.DatePickerCtrl(
        self.panel, dt=newjobDate, style=wx.DP_DROPDOWN)
    self.client = wx.Choice(self.panel, choices=client_choices)
    clientlabel = wx.StaticText(self.panel, label='Client:')

    cancelbutton = wx.Button(self.panel, wx.ID_CANCEL, "&Cancel")
    savebutton = wx.Button(self.panel, wx.ID_OK, "&SAVE")
    top = wx.BoxSizer(wx.VERTICAL)
    row1 = wx.BoxSizer(wx.HORIZONTAL)
    row2 = wx.BoxSizer(wx.HORIZONTAL)
    row2a = wx.BoxSizer(wx.HORIZONTAL)
    row3 = wx.BoxSizer(wx.HORIZONTAL)
    row1.AddSpacer(10)
    row1.Add(namelabel,0,wx.LEFT,5)
    row1.Add(datelabel,0,wx.LEFT,125)
    row2.AddSpacer(10)
    row2.Add(self.jobname, 0, wx.RIGHT, 5)
    row2.Add(self.jobDate, 0,0,0)
    row2a.Add(clientlabel, 0, wx.LEFT, 5)
    row2a.Add(self.client, 0,0,0)
    row3.Add(cancelbutton, 0,wx.LEFT,10)
    row3.Add(savebutton, 0,wx.LEFT,10)

    top.AddStretchSpacer(1)
    top.Add(row1,1,wx.ALIGN_CENTER,0)

    top.Add(row2,1,wx.ALIGN_CENTER,0)
    top.AddStretchSpacer(1)
    top.Add(row2a,1,wx.ALIGN_CENTER,0)
    top.AddStretchSpacer(1)
    top.Add(row3,1,wx.ALIGN_CENTER,0)
    top.AddStretchSpacer(1)
    self.panel.SetSizer(top)
    top.Fit(self.panel)
    self.panel.Fit()

    self.Bind(wx.EVT_BUTTON, self.cancel, id=wx.ID_CANCEL)
    self.Bind(wx.EVT_BUTTON, self.save, id=wx.ID_OK)

    self.jobname.SetFocus()

  def setChiefName(self, e):
    self.chiefname = e.GetString()
    e.Skip()


  def save(self, e):
    error_flag = 'False'
    newJob = Jobs()

    newJob.name = unicode(self.jobname.GetValue())


    newDatetime = datetime.fromtimestamp(self.jobDate.GetValue().GetTicks())
    newJob.create_date = newDatetime
    newJob.active = True

    Client_Name = self.client.GetStringSelection()
    if Client_Name != '' or Client_Name != 'none':
        clientObj = session.query(Client).filter(Client.name.like(Client_Name)).first()
        newJob.Client = clientObj

    #check to make sure Job is unique and not inactive
    name_values = newJob.name
    name_values = re.sub(',','',name_values)
    name_values = name_values.split()
    index = 0
    for each in name_values:
        name_values[index] = "%{}%".format(each)
        index += 1

    # for each in name_values:
    #     print each


    strict_jobCheck = session.query(Jobs).filter(Jobs.name.like(newJob.name)).filter(Jobs.create_date == newDatetime)
    if strict_jobCheck.count() > 0:
        wx.MessageBox('The same Job name has already been used on this date please consider another', 'Duplicate Job Warning', wx.OK | wx.ICON_ERROR)
        error_flag = 'Failed Strict'


    if error_flag == 'False':
        days10before = newDatetime - timedelta(days = 10)
        days10after = newDatetime + timedelta(days = 10)
        jobCheck = session.query(Jobs).filter(or_(*[Jobs.name.like(each) for each in name_values]))
        jobCheck = jobCheck.filter(Jobs.create_date.between(days10before, days10after))
        # print jobCheck.count()
        if jobCheck.count() == 1:
            jobObj = jobCheck.first()
            ret = wx.MessageBox('A Job already exist with a simular name: ' + jobObj.name +
                '\nWith a creation date of ' + jobObj.create_date.strftime('%x') +
                '.  \nDo you want to Create this job anyway?' , 'Duplicate Job Warning', wx.YES_NO|wx.ICON_QUESTION)
            if ret == wx.NO:
                error_flag = 'Failed Lose'


        elif jobCheck.count() > 1:
            error_flag = 'Failed Lose'
            JobIdList = list()
            for each in jobCheck:
                JobIdList.append(each.id)
            Job_Info = {'Name': newJob.name, 'Date': newDatetime.strftime('%x')}
            mywin = ManyJobMatches(self, JobIdList, Job_Info)
            mywin.CenterOnParent()
            ret_val = mywin.ShowModal()
            if ret_val == wx.ID_OK:
                error_flag = 'False'

            # wx.MessageBox('Database Error, Please contact Doss with error code: JobAdd1', 'Database Error', wx.OK | wx.ICON_ERROR)

    if error_flag == 'False':
        session.add(newJob)
        session.flush()
        GlobalVars.JobSelection = (newJob.id, newJob.name)
        session.commit()

    if 'Failed' in error_flag:
        session.rollback()

    if error_flag == 'Failed Lose' or error_flag == 'False':
        self.Destroy()


  def cancel(self, e):
    session.rollback()
    e.Skip()
    self.Destroy()

class EditJobWindow(wx.Dialog):

  def __init__(self, parent, Job_Id):
    super(EditJobWindow, self).__init__(
        parent, style=wx.CAPTION, title='Edit Job', size=(300, 150))  # size=(380, 225))

    self.panel = wx.Panel(self, wx.ID_ANY)

    self.JobObj = session.query(Jobs).filter(Jobs.id == Job_Id).first()

    newjobDate = pydate2wxdate(self.JobObj.create_date)
    results = session.query(Client).filter_by(active=True).all()
    client_choices = list()
    for each in results:
        client_choices.append(each.name)
    client_choices.append('none')

    if not self.JobObj.Client is None:
        if not self.JobObj.Client.active:
            client_choices.append(self.JobObj.Client.name)




    namelabel = wx.StaticText(self.panel, label='Job Name:')
    self.jobname = wx.TextCtrl(self.panel, size=(175, -1))
    self.jobname.ChangeValue(self.JobObj.name)
    datelabel = wx.StaticText(self.panel, label="Start Date:")
    self.jobDate = wx.DatePickerCtrl(
        self.panel, dt=newjobDate, style=wx.DP_DROPDOWN)
    self.client = wx.Choice(self.panel, choices=client_choices)
    if not self.JobObj.Client is None:
        self.client.SetStringSelection(self.JobObj.Client.name)
    clientlabel = wx.StaticText(self.panel, label='Client:')
    self.activeBox = wx.CheckBox(self.panel, label="Active")
    self.activeBox.SetValue(self.JobObj.active)

    cancelbutton = wx.Button(self.panel, wx.ID_CANCEL, "&Cancel")
    savebutton = wx.Button(self.panel, wx.ID_OK, "&SAVE")
    top = wx.BoxSizer(wx.VERTICAL)
    row1 = wx.BoxSizer(wx.HORIZONTAL)
    row2 = wx.BoxSizer(wx.HORIZONTAL)
    row2a = wx.BoxSizer(wx.HORIZONTAL)
    row3 = wx.BoxSizer(wx.HORIZONTAL)
    row1.AddSpacer(10)
    row1.Add(namelabel,0,wx.LEFT,5)
    row1.Add(datelabel,0,wx.LEFT,125)
    row2.AddSpacer(10)
    row2.Add(self.jobname, 0, wx.RIGHT, 5)
    row2.Add(self.jobDate, 0,0,0)
    row2a.AddStretchSpacer(2)
    row2a.Add(clientlabel, 0, wx.LEFT, 5)
    row2a.Add(self.client, 0,0,0)
    row2a.AddStretchSpacer(1)
    row2a.Add(self.activeBox,1,wx.ALIGN_CENTER,0)
    row2a.AddStretchSpacer(1)
    row3.Add(cancelbutton, 0,wx.LEFT,10)
    row3.Add(savebutton, 0,wx.LEFT,10)

    top.AddStretchSpacer(1)
    top.Add(row1,1,wx.ALIGN_CENTER,0)

    top.Add(row2,1,wx.ALIGN_CENTER,0)
    top.AddStretchSpacer(1)
    top.Add(row2a,1,wx.ALIGN_CENTER,0)
    top.AddStretchSpacer(1)
    top.Add(row3,1,wx.ALIGN_CENTER,0)
    top.AddStretchSpacer(1)
    self.panel.SetSizer(top)
    top.Fit(self.panel)
    self.panel.Fit()

    self.Bind(wx.EVT_BUTTON, self.cancel, id=wx.ID_CANCEL)
    self.Bind(wx.EVT_BUTTON, self.save, id=wx.ID_OK)

    self.jobname.SetFocus()

  def setChiefName(self, e):
    self.chiefname = e.GetString()
    e.Skip()


  def save(self, e):

    self.JobObj.name = unicode(self.jobname.GetValue())
    newDatetime = datetime.fromtimestamp(self.jobDate.GetValue().GetTicks())
    self.JobObj.create_date = newDatetime
    # print self.activeBox.GetValue()
    self.JobObj.active = self.activeBox.GetValue()

    Client_Name = self.client.GetStringSelection()
    if Client_Name != '' or Client_Name != 'none':
        clientObj = session.query(Client).filter(Client.name.like(Client_Name)).first()
        self.JobObj.Client = clientObj
    else:
        self.JobObj.client_id = None

    session.flush()
    session.commit()
    self.Destroy()


  def cancel(self, e):
    session.rollback()
    e.Skip()
    self.Destroy()

class JobWindow(wx.Dialog):

  def __init__(self, parent):
    # self.DataObj = Jobs
    self.parent = parent
    # ColInfo = [
    #     #   col value              title         type            color if one
    #     (0, 'name', 'Name', 'Unicode'),
    #     (1, 'active','Active','CheckBox'),
    #     (2, 'delete.png', 'Delete', 'Delete')
    # ]

    super(JobWindow, self).__init__(parent, style=wx.CAPTION , title='Edit Job', size=(450,400))

    self.Setup()



  def Setup(self):
      self.panel = wx.Panel(self, wx.ID_ANY)

      topSize = wx.BoxSizer(wx.VERTICAL)
      topRowSizer = wx.BoxSizer(wx.HORIZONTAL)
      dateSizer = wx.BoxSizer(wx.HORIZONTAL)
      middleSizer = wx.BoxSizer(wx.HORIZONTAL)
      midButtonSizer = wx.BoxSizer(wx.VERTICAL)
      bottomRowSizer = wx.BoxSizer(wx.HORIZONTAL)
      buttonSizer = wx.BoxSizer(wx.HORIZONTAL)


      self.filterDates = True
      self.showInActive = False
      self.order_by = Jobs.name
      beforewxDT = pydate2wxdate(datetime.now().date() - timedelta(days = 30))
      afterwxDT = pydate2wxdate(datetime.now() + timedelta(days=1))


      textTitle = wx.StaticText(self.panel, label="Search:")
      self.textbox = wx.TextCtrl(self.panel, size=(140, -1))
      self.activeBox = wx.CheckBox(self.panel, label='Show Inactive?')
      monthdate = wx.DateTime.Now()
      beforeText = wx.StaticText(self.panel, label='After: ')
      self.beforeDate = wx.DatePickerCtrl(
          self.panel, dt=beforewxDT, style=wx.DP_DROPDOWN)
      afterText = wx.StaticText(self.panel, label='Before: ')
      self.afterDate = wx.DatePickerCtrl(
          self.panel, dt=afterwxDT, style=wx.DP_DROPDOWN)
      self.clearDateButton = wx.Button(self.panel, wx.ID_ANY, 'Unfilter Dates')
      listTitle = wx.StaticText(self.panel, label='Jobs:')
      # self.listbox = wx.ListBox(self.panel, size=(300, 200))
      self.listbox = wx.ListCtrl(self.panel, size=(JOBWIN_LISTBOX_WIDTH, 200), style=wx.LC_REPORT| wx.BORDER_SUNKEN)
      # self.listbox = ULC.UltimateListCtrl(self.panel,size=(JOBWIN_LISTBOX_WIDTH, 200), agwStyle = wx.LC_REPORT | wx.LC_VRULES | wx.LC_HRULES)
      self.total_jobsText = wx.StaticText(self.panel, label='')

      topRowSizer.AddStretchSpacer(1)
      topRowSizer.Add(textTitle, 1, wx.LEFT | wx.ALIGN_CENTER, 15)
      topRowSizer.Add(self.textbox, 1, wx.LEFT | wx.ALIGN_CENTER, 10)
      topRowSizer.AddStretchSpacer(3)
      topRowSizer.Add(self.activeBox, 1,  wx.ALIGN_CENTER,0)
      topRowSizer.AddStretchSpacer(1)

      topSize.AddSpacer(10)
      topSize.Add(topRowSizer, 1, 0,0)
      topSize.AddSpacer(10)


      dateSizer.AddStretchSpacer(5)
      dateSizer.Add(beforeText, 1 , wx.LEFT | wx.ALIGN_CENTER, 0)
      dateSizer.Add(self.beforeDate, 1, wx.LEFT | wx.ALIGN_CENTER, 0)
      dateSizer.AddStretchSpacer(3)
      dateSizer.Add(afterText, 1, wx.LEFT | wx.ALIGN_CENTER, 0)
      dateSizer.Add(self.afterDate, 1, wx.LEFT | wx.ALIGN_CENTER, 0)
      dateSizer.AddStretchSpacer(2)
      dateSizer.Add(self.clearDateButton, 1, wx.LEFT | wx.ALIGN_CENTER, 0)
      dateSizer.AddStretchSpacer(5)

      AddButton = wx.Button(self.panel, wx.ID_ANY, 'Add Job')
      AddButton.SetFocus()

      DelButton = wx.Button(self.panel, wx.ID_ANY, 'Delete Job')
      EditButton = wx.Button(self.panel, wx.ID_ANY, 'Edit Job')
      InactiveButton = wx.Button(self.panel, wx.ID_ANY, 'Set Inactive')


      # midButtonSizer.AddStretchSpacer(1)
      midButtonSizer.AddSpacer(8)
      midButtonSizer.Add(AddButton, 1, 0, 0)
      midButtonSizer.AddStretchSpacer(1)
      midButtonSizer.Add(DelButton, 1, 0, 0)
      midButtonSizer.AddStretchSpacer(1)
      midButtonSizer.Add(EditButton, 1, 0, 0)
      midButtonSizer.AddStretchSpacer(1)
      midButtonSizer.Add(InactiveButton,1,0,0)

      middleSizer.AddStretchSpacer(1)
      middleSizer.Add(self.listbox, 1, 0, 0)
      middleSizer.AddSpacer(15)
      middleSizer.Add(midButtonSizer, 1, 0, 0)
      middleSizer.AddStretchSpacer(1)



      topSize.Add(dateSizer)
      topSize.Add(listTitle, 1, wx.LEFT, 25)
      topSize.Add(middleSizer, 1, 0, 0)
      topSize.AddSpacer(3)
      topSize.Add(self.total_jobsText, 1, wx.ALIGN_LEFT | wx.LEFT , 25)
      topSize.AddSpacer(5)

      cancelbutton = wx.Button(self.panel, wx.ID_CANCEL, "&Cancel")
      exportbutton = wx.Button(self.panel, wx.ID_OK, "&Ok")
      buttonSizer.Add(cancelbutton, 1, wx.ALL | wx.ALIGN_CENTER, 5)
      buttonSizer.Add(exportbutton, 1, wx.ALL | wx.ALIGN_CENTER, 5)

      topSize.AddStretchSpacer(1)
      topSize.Add(buttonSizer, 1, wx.ALL | wx.ALIGN_CENTER, 1)
      topSize.AddStretchSpacer(1)

      self.panel.SetSizer(topSize)
      topSize.Fit(self.panel)
      self.panel.Fit()

      self.clearDateButton.Bind(wx.EVT_BUTTON, self.clearDate)
      self.activeBox.Bind(wx.EVT_CHECKBOX, self.toggleActive)
      self.textbox.Bind(wx.EVT_TEXT, self.textEntered)
      self.listbox.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.EditJob)
      AddButton.Bind(wx.EVT_BUTTON, self.add)
      DelButton.Bind(wx.EVT_BUTTON, self.deleteJob)
      cancelbutton.Bind(wx.EVT_BUTTON, self.cancel)
      EditButton.Bind(wx.EVT_BUTTON, self.EditJob)
      InactiveButton.Bind(wx.EVT_BUTTON, self.setJobInactive)
      self.beforeDate.Bind(wx.EVT_DATE_CHANGED, self.dateChanged)
      self.afterDate.Bind(wx.EVT_DATE_CHANGED,  self.dateChanged)
      self.listbox.Bind(wx.EVT_LIST_COL_CLICK, self.headerClick)
      # self.listbox.Bind(wx.EVT_LIST_KEY_DOWN, self.listKeyDown)

      self.IndexDict = dict()
      beforeDate = datetime.fromtimestamp(self.beforeDate.GetValue().GetTicks()).date()
      afterDate = datetime.fromtimestamp(self.afterDate.GetValue().GetTicks()).date()


      self.listbox.InsertColumn(0, 'Job Name')
      self.listbox.InsertColumn(1, 'Date')
      self.listbox.InsertColumn(3, 'Active')

      results = session.query(Jobs).order_by(self.order_by).filter(Jobs.create_date.between(beforeDate, afterDate))

      if self.showInActive == False:
          results.filter_by(active=True)

      displayed_jobs = results.count()
      total_jobs = session.query(Jobs).count()
      total_jobs_text = 'Total Jobs: ' + str(total_jobs) + '          Jobs Displayed: ' + str(displayed_jobs)
      self.total_jobsText.SetLabel(total_jobs_text)

      results.all()
      index = 0
      for each in results:
          self.listbox.InsertStringItem(index, each.name)
          self.listbox.SetStringItem(index, 1, each.create_date.strftime('%m/%d/%Y'))
          if each.active:
            self.listbox.SetStringItem(index, 2, 'True')
          else:
            self.listbox.SetStringItem(index, 2, 'False')
          self.listbox.SetItemData(index, each.id)
          index += 1

      self.listbox.SetColumnWidth(0, JOBWIN_LISTBOX_WIDTH - JOBWIN_ACTIVE_WIDTH - JOBWIN_DATE_WIDTH - 25)
      self.listbox.SetColumnWidth(1, JOBWIN_DATE_WIDTH)
      self.listbox.SetColumnWidth(2, JOBWIN_ACTIVE_WIDTH)



  def headerClick(self, e):
    old_orderby = self.order_by

    if e.GetColumn() == 0:
      if self.order_by == Jobs.name:
        self.order_by = Jobs.name.desc()
      else:
        self.order_by = Jobs.name
    if e.GetColumn() == 1:
      if self.order_by == Jobs.create_date:
        self.order_by = Jobs.create_date.desc()
      else:
        self.order_by = Jobs.create_date
    if e.GetColumn() == 2:
      if self.order_by == Jobs.active:
        self.order_by = Jobs.active.desc()
      else:
        self.order_by = Jobs.active

    if old_orderby != self.order_by:
      self.textEntered(1)

  def EditJob(self, e):

        if self.listbox.GetSelectedItemCount() == 1:
            currentItem = self.listbox.GetNextSelected(-1)
             # print currentItem
            addwin = EditJobWindow(self, self.listbox.GetItemData(currentItem))
            addwin.CenterOnParent()
            # if addwin.ShowModal() == wx.ID_OK:
            addwin.ShowModal()
            wx.CallAfter(self.textEntered,1)

        elif self.listbox.GetSelectedItemCount() < 1:
          wx.MessageBox('Please select a Job to Edit.', 'No Job Selected', wx.OK | wx.ICON_INFORMATION)

        else:
           wx.MessageBox('You can only Edit one job at a time.','Error', wx.OK | wx.ICON_ERROR)



  def clearDate(self, e):
      if self.filterDates:
          self.clearDateButton.SetLabel('Filter Dates')
          self.filterDates = False
          self.textEntered(1)
      else:
          self.clearDateButton.SetLabel('Unfilter Dates')
          self.filterDates = True
          self.textEntered(1)

  def dateChanged(self, e):
      wx.CallAfter(self.textEntered, 1)

  def toggleActive(self, e):
      self.showInActive = self.activeBox.GetValue()
      wx.CallAfter(self.textEntered, 1)


  def setJobInactive(self, e):
    currentItem = -1
    while self.listbox.GetNextSelected(currentItem) != -1:
       currentItem = self.listbox.GetNextSelected(currentItem)
       Job_Id = self.listbox.GetItemData(currentItem)

       jobObj = session.query(Jobs).filter(Jobs.id == Job_Id).first()
       if jobObj.active:
          jobObj.active = False
          session.commit()
          wx.CallAfter(self.textEntered,1)
          MessageClass.Message('Job {} set inactive.'.format(jobObj.name))



  def textEntered(self, e):
      value = self.textbox.GetValue()

      timesheetList = session.query(Jobs).order_by(self.order_by).filter(Jobs.name.like('%' + value + '%'))
      # print timesheetList.count()

      if self.filterDates:
          beforeDate = datetime.fromtimestamp(self.beforeDate.GetValue().GetTicks()).date()
          afterDate = datetime.fromtimestamp(self.afterDate.GetValue().GetTicks()).date()
          timesheetList = timesheetList.filter(Jobs.create_date.between(beforeDate, afterDate))

      if self.showInActive == False:
          timesheetList = timesheetList.filter(Jobs.active == True)

      displayed_jobs = timesheetList.count()
      total_jobs = session.query(Jobs).count()
      total_jobs_text = 'Total Jobs: ' + str(total_jobs) + '          Jobs Displayed: ' + str(displayed_jobs)
      self.total_jobsText.SetLabel(total_jobs_text)

      # print timesheetList.count()
      timesheetList.all()

      self.listbox.ClearAll()
      self.listbox.InsertColumn(0, 'Job Name')
      self.listbox.InsertColumn(1, 'Date')
      self.listbox.InsertColumn(3, 'Active')


      index = 0
      for each in timesheetList:
          self.listbox.InsertStringItem(index, each.name)
          self.listbox.SetStringItem(index, 1, each.create_date.strftime('%m/%d/%Y'))
          if each.active:
            self.listbox.SetStringItem(index, 2, 'True')
          else:
            self.listbox.SetStringItem(index, 2, 'False')
          self.listbox.SetItemData(index, each.id)
          index += 1
      self.listbox.SetColumnWidth(0, JOBWIN_LISTBOX_WIDTH - JOBWIN_ACTIVE_WIDTH - JOBWIN_DATE_WIDTH - 25)
      self.listbox.SetColumnWidth(1, JOBWIN_DATE_WIDTH)
      self.listbox.SetColumnWidth(2, JOBWIN_ACTIVE_WIDTH)


  def deleteJob(self, e):
     if self.listbox.GetSelectedItemCount() == 1:
            currentItem = self.listbox.GetNextSelected(-1)
            Job_Id = self.listbox.GetItemData(currentItem)
            results = session.query(Hours).join(Jobs).filter(Jobs.id == Job_Id).count()
            try:
              JobObj = session.query(Jobs).filter(Jobs.id == Job_Id).scalar()
            except:
              MessageClass.Message('Database Error: Job not found.', Type=MESS_PANIC)

            if results == 0:
                ret = wx.MessageBox('Are you sure you want to delete Job: ' + JobObj.name, 'Comfirm Delete', wx.YES_NO | wx.ICON_QUESTION)
                if ret == wx.YES:
                    try:
                      session.delete(JobObj)
                      session.commit()
                    except:
                        MessageClass.Message('There was an error deleting the job.', Type=MESS_PANIC)
                    wx.CallAfter(self.textEntered,1)
            else:
                    MessageClass.Message('This Job is associated with Timesheets and cannot be deleted.\nTry marking as Inactive instead.', Caption='Cannot Delete', Type=MESS_PANIC)




     else:
        MessageClass.Message('You can only delete one job at a time.', Type=MESS_PANIC)

  def add(self, e):
    addwin = AddJobWindow(self)
    addwin.CenterOnParent()
    # if addwin.ShowModal() == wx.ID_OK:
    addwin.ShowModal()
    wx.CallAfter(self.textEntered, 1)

  def cancel(self, e):
    self.Destroy()

class AddTaskWindow(wx.Dialog):
  def __init__(self, parent, Task_id = None):

     super(AddTaskWindow, self).__init__(parent, style=wx.CAPTION, title='Add Task', size=(350,150))

     self.Task_id = Task_id

     if self.Task_id != None:
       taskObj = session.query(TaskList).filter(TaskList.id == self.Task_id).first()
       self.taskObj = taskObj
     self.panel = wx.Panel(self, wx.ID_ANY)
     vbox = wx.BoxSizer(wx.VERTICAL)

     row1 = wx.BoxSizer(wx.HORIZONTAL)
     row2 = wx.BoxSizer(wx.HORIZONTAL)
     row3 = wx.BoxSizer(wx.HORIZONTAL)

     namelabel = wx.StaticText(self.panel, label='Name:')
     self.name = wx.TextCtrl(self.panel, size=(140, -1))
     short_namelabel = wx.StaticText(self.panel, label='Short Name:')
     self.short_name = wx.TextCtrl(self.panel, size=(95,-1))
     self.short_name.SetMaxLength(5)
     self.activeBox = wx.CheckBox(self.panel, label='Active?')
     self.activeBox.SetValue(True)

     if self.Task_id != None:
      self.name.SetValue(taskObj.name)
      self.short_name.SetValue(taskObj.short_name)
      if taskObj.active:
        self.activeBox.SetValue(True)
      else:
        self.activeBox.SetValue(False)

     cancelbutton= wx.Button(self.panel, label='Cancel', id=wx.ID_CANCEL)
     okButton = wx.Button(self.panel, label='Ok', id=wx.ID_OK)

     okButton.Bind(wx.EVT_BUTTON, self.Save)

     row1.AddStretchSpacer(3)
     row1.Add(namelabel, 1, wx.ALIGN_CENTER, 0)
     row1.AddStretchSpacer(1)
     row1.Add(self.name, 1, wx.ALIGN_CENTER, 0)
     row1.AddStretchSpacer(3)
     row2.AddStretchSpacer(3)
     row2.Add(short_namelabel, 1, wx.ALIGN_CENTER, 0)
     row2.AddStretchSpacer(1)
     row2.Add(self.short_name, 1, wx.ALIGN_CENTER, 0)
     row2.AddStretchSpacer(1)
     row2.Add(self.activeBox, 1, wx.ALIGN_CENTER, 0)
     row2.AddStretchSpacer(3)
     row3.Add(cancelbutton, 1, wx.ALIGN_CENTER, 0)
     row3.Add(okButton, 1, wx.ALIGN_CENTER, 0)


     vbox.Add(row1, 1, wx.ALIGN_CENTER, 0)
     vbox.Add(row2, 1, wx.ALIGN_CENTER, 0)
     vbox.Add(row3, 1, wx.ALIGN_CENTER, 0)

     self.panel.SetSizer(vbox)

  def Save(self, e):
    if self.Task_id != None:
      newTask = self.taskObj
    else:
      newTask = TaskList()


    newTask.name = unicode(self.name.GetValue())

    newTask.short_name = unicode(self.short_name.GetValue())

    newTask.active = self.activeBox.GetValue()

    try:
      if self.Task_id == None:
        session.add(newTask)
      session.commit()
    except:
      session.rollback()
      wx.MessageBox('Task not created.\nDid you use a Duplicate task name or short name?','Error', wx.OK|wx.ICON_ERROR)
    e.Skip()

class TaskWindow(DatabaseWindow2):
  def __init__(self, parent):

    DataObj = TaskList
    ColInfo = [{'Column': 0, 'Field':'TaskList.name', 'Short Field': 'name', 'FieldName': 'Name', 'Type':'Unicode'},
      {'Column': 1, 'Field':'TaskList.short_name', 'Short Field': 'short_name', 'FieldName': 'Short Name', 'Type':'Unicode','Width': 85},
      {'Column': 2, 'Field':'TaskList.active', 'Short Field': 'active', 'FieldName': 'Active', 'Type':'Bool','Width': 70}]
    super(TaskWindow, self).__init__(parent, 'Edit Task', DataObj, ColInfo)

    self.Setup()

  def add(self, e):
    mywin = AddTaskWindow(self)
    mywin.CenterOnParent()
    if mywin.ShowModal() == wx.ID_OK:
      self.textEntered(1)


  def EditFunction(self):
    if self.listbox.GetSelectedItemCount() == 1:
      currentItem = self.listbox.GetNextSelected(-1)
      Client_id = self.listbox.GetItemData(currentItem)
      addwin = AddTaskWindow(self, Client_id)
      addwin.CenterOnParent()
      if addwin.ShowModal() == wx.ID_OK:
        self.textEntered(1)

  def deleteItem(self, e):
     if self.listbox.GetSelectedItemCount() == 1:
            currentItem = self.listbox.GetNextSelected(-1)
            Job_Id = self.listbox.GetItemData(currentItem)
            results = session.query(TaskList).join(Hours).filter(Hours.task_id == Job_Id).count()
            JobObj = session.query(TaskList).filter(TaskList.id == Job_Id).first()

            if results == 0:
                ret = wx.MessageBox('Are you sure you want to delete Task: ' + JobObj.name, 'Comfirm Delete', wx.YES_NO | wx.ICON_QUESTION)
                if ret == wx.YES:
                    session.delete(JobObj)
                    try:
                      session.commit()
                    except:
                        MessageClass.Message('There was an error, deleting the Task', Type=MESS_PANIC)
                    wx.CallAfter(self.textEntered,1)
            else:
                    wx.MessageBox('This Task is associated with Timesheets and cannot be deleted.\nTry marking as Inactive instead.', 'Cannot Delete', wx.OK | wx.ICON_INFORMATION)




     else:
        wx.MessageBox('You can only delete one Task at a time.','Error', wx.OK | wx.ICON_ERROR)

class AddCheifWindow(wx.Dialog):

  def __init__(self, parent, Cheif_id = None):
    super(AddCheifWindow, self).__init__(
        parent, style=wx.CAPTION, title='Add Employee')
    self.Cheif_id = Cheif_id
    if Cheif_id != None:
      Cheif_Obj = session.query(CrewNames).filter_by(id=Cheif_id).first()
      Cheif_Name = Cheif_Obj.name
      Cheif_Active = Cheif_Obj.active
    else:
      Cheif_Name = ''
      Cheif_Active = True

    self.panel = wx.Panel(self, wx.ID_ANY)
    vbox = wx.BoxSizer(wx.VERTICAL)
    entrybox = wx.BoxSizer(wx.HORIZONTAL)
    buttonbox = wx.BoxSizer(wx.HORIZONTAL)
    editlbl = wx.StaticText(self.panel, label="Name:")
    self.editname = wx.TextCtrl(self.panel, value=Cheif_Name, size=(140, -1))
    self.activeBox = wx.CheckBox(self.panel, label='Active?')
    if Cheif_Active:
      self.activeBox.SetValue(True)
    cancelbutton = wx.Button(self.panel, wx.ID_CANCEL, "&Cancel")
    savebutton = wx.Button(self.panel, wx.ID_OK, "&SAVE")

    entrybox.Add(editlbl, 0, wx.CENTER | wx.ALL, 5)
    entrybox.Add(self.editname, 0, wx.CENTER | wx.ALL, 5)
    entrybox.Add(self.activeBox, 0, wx.CENTER)

    buttonbox.Add(cancelbutton, 0, wx.CENTER, 10)
    buttonbox.Add(savebutton, 0, wx.CENTER, 10)

    vbox.Add(entrybox, 0, wx.CENTER, 0)
    vbox.Add(buttonbox, 0, wx.CENTER, 0)

    self.panel.SetSizer(vbox)
    vbox.Fit(self)
    self.editname.SetFocus()


    self.Bind(wx.EVT_BUTTON, self.cancel, id=wx.ID_CANCEL)
    self.Bind(wx.EVT_BUTTON, self.save, id=wx.ID_OK)


  def save(self, e):
    if self.Cheif_id == None:
      if len(self.editname.GetValue()) > 0:
        newchief = CrewNames()
        newchief.name = unicode(self.editname.GetValue())
        newchief.active = self.activeBox.GetValue()
        session.add(newchief)
        session.flush()
        session.commit()
        # self.Destory()
      else:
        wx.MessageBox('Information not complete.', 'Error',
                      wx.OK | wx.ICON_ERROR)
    else:
      Cheif_Obj = session.query(CrewNames).filter_by(id=self.Cheif_id).first()
      Cheif_Obj.name = unicode(self.editname.GetValue())
      Cheif_Obj.active = self.activeBox.GetValue()
      session.commit()


    e.Skip()

  def cancel(self, e):
    session.rollback()
    e.Skip()

class CheifWindow(DatabaseWindow2):

  def __init__(self, parent):

    DataObj = CrewNames
    ColInfo = [{'Column': 0, 'Field':'CrewNames.name', 'Short Field': 'name', 'FieldName': 'Employees', 'Type':'Unicode'},
      {'Column': 1, 'Field':'CrewNames.active', 'Short Field': 'active', 'FieldName': 'Active', 'Type':'Bool','Width': 70}]
    super(CheifWindow, self).__init__(parent, 'Edit Employees', DataObj, ColInfo)

    self.Setup()

  def add(self, e):
    addwin = AddCheifWindow(self)
    addwin.CenterOnParent()
    if addwin.ShowModal() == wx.ID_OK:
      self.textEntered(1)

  def EditFunction(self):
    if self.listbox.GetSelectedItemCount() == 1:
      currentItem = self.listbox.GetNextSelected(-1)
      Cheif_id = self.listbox.GetItemData(currentItem)
      addwin = AddCheifWindow(self, Cheif_id)
      addwin.CenterOnParent()
      if addwin.ShowModal() == wx.ID_OK:
        self.textEntered(1)

  def deleteItem(self, e):
     if self.listbox.GetSelectedItemCount() == 1:
            currentItem = self.listbox.GetNextSelected(-1)
            Job_Id = self.listbox.GetItemData(currentItem)
            results = session.query(TimeSheet).join(CrewNames).filter(CrewNames.id == Job_Id).count()
            JobObj = session.query(CrewNames).filter(CrewNames.id == Job_Id).first()

            if results == 0:
                ret = wx.MessageBox('Are you sure you want to delete Employee: ' + JobObj.name, 'Comfirm Delete', wx.YES_NO | wx.ICON_QUESTION)
                if ret == wx.YES:
                    session.delete(JobObj)
                    try:
                      session.commit()
                    except:
                        MessageClass.Message('There was an error deleting the Employee', Type=MESS_PANIC)
                    wx.CallAfter(self.textEntered,1)
            else:
                    wx.MessageBox('This Employee is associated with Timesheets and cannot be deleted.\nTry marking as Inactive instead.', 'Cannot Delete', wx.OK | wx.ICON_INFORMATION)




     else:
        wx.MessageBox('You can only delete one job at a time.','Error', wx.OK | wx.ICON_ERROR)

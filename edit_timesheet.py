import wx

from database import *
from main_window import *
from datetime import datetime, date, timedelta
from utility_functions import pydate2wxdate

# row constants
ROW_START = 1
ROW_END = 3
ROW_JOB = 5
ROW_TASK = 7

TIME_EMPLOYEE_WIDTH = 145


class EditTimeSheet(MainWindow):
    def __init__(self, parent, title, style, timesheet_id, UpdateFunction):
        # self.timesheet_id = timesheet_id
        my_x,my_y = parent.GetPositionTuple()
        msg = "Please wait loading TimeSheet..."
        self.busyDlg = wx.BusyInfo(msg)
        self.parent = parent
        self.UpdateFunction = UpdateFunction
        super(EditTimeSheet, self).__init__(parent, style=style, title=title, pos=(my_x+100,my_y+10))
        self.Edit = True
        self.GridLabelValue = 'Edit TimeSheet'
        self.timesheet_id = timesheet_id
        self.FilterInactive = False

        self.InitUI()

        self.Show(True)


    def InitUI(self):
        self.newEntryPanel()
        self.busyDlg = None

    def DeleteTimeSheet(self, e):
        deleteMessage = "Are you sure you want to delete the\nTimeSheet for {} for {}".format(self.timesheet.__timesheetObj__.CrewName.name, self.timesheet.__timesheetObj__.date.strftime('%x'))
        ret = wx.MessageBox(deleteMessage, 'Comfirm Delete', wx.YES_NO | wx.ICON_QUESTION)
        if ret == wx.YES:
            if self.UpdateFunction:
                wx.CallAfter(self.UpdateFunction, 1)
            timesheetid = self.timesheet.__timesheetObj__.id
            hourRows = session.query(Hours).filter(Hours.timesheet_id == timesheetid)
            deletecount = 0
            if hourRows.count() > 0:
                for each in hourRows:
                    try:
                        session.delete(each)
                        deletecount += 1
                    except:
                        MessageClass.Message('Error Deleting a row associated with timesheet.\nTell Doss', Type=MESS_PANIC)
                        deletecount -= 1

                MessageClass.Message('Deleted {} Hour Rows.'.format(deletecount), Duration=2500)



            try:
                session.delete(self.timesheet.__timesheetObj__)
                session.commit()
            except:
                session.rollback()
                MessageClass.Message('There was an error deleting the TimeSheet', Type=MESS_PANIC)
            self.Destroy()

class EditTimeWindow(wx.Dialog):

    def __init__(self, parent):
        super(EditTimeWindow, self).__init__(
            parent, style=wx.CAPTION, title='Edit TimeSheet', size=(280, 375))

        self.panel = wx.Panel(self, wx.ID_ANY)
        self.parent = parent
        self.order_by = TimeSheet.date


        topSize = wx.BoxSizer(wx.VERTICAL)
        dateSizer = wx.BoxSizer(wx.HORIZONTAL)
        buttonSizer = wx.BoxSizer(wx.HORIZONTAL)

        self.filterDates = True

        textTitle = wx.StaticText(self.panel, label="Search:")
        self.textbox = wx.TextCtrl(self.panel, size=(140, -1))
        pyfristDate = datetime.now() - timedelta(days = 30)
        pysecondDate = datetime.now() + timedelta(days = 1)
        date_firstDate = pydate2wxdate(pyfristDate)
        date_secondDate = pydate2wxdate(pysecondDate)

        self.firstDate = wx.DatePickerCtrl(self.panel, dt=date_firstDate, style=wx.DP_DROPDOWN)
        self.secondDate = wx.DatePickerCtrl(self.panel, dt=date_secondDate, style=wx.DP_DROPDOWN)
        # clearDateButton = wx.Button(self.panel, wx.ID_ANY, 'Clear Date')
        listTitle = wx.StaticText(self.panel, label='Pick a TimeSheet:')
        self.listbox = wx.ListCtrl(self.panel, size=(250, 150), style=wx.LC_REPORT| wx.BORDER_SUNKEN)
        self.checkClose = wx.CheckBox(self.panel, label='Close this Window after open?')
        self.checkClose.SetValue(True)

        dateSizer.Add(self.firstDate, 1, wx.LEFT, 25)
        dateSizer.Add(self.secondDate, 1, wx.LEFT, 20)
        # dateSizer.Add(clearDateButton, 1, wx.LEFT, 15)

        topSize.AddSpacer(10)
        topSize.Add(dateSizer)
        topSize.AddSpacer(15)
        topSize.Add(listTitle, 1, wx.LEFT , 25)
        topSize.Add(self.listbox, 1, wx.ALIGN_CENTER, 0)

        topSize.AddSpacer(10)
        topSize.Add(textTitle, 1, wx.LEFT, 30)
        topSize.Add(self.textbox, 1, wx.LEFT, 25)
        topSize.AddSpacer(10)
        topSize.Add(self.checkClose, 1, wx.LEFT, 25)

        cancelbutton = wx.Button(self.panel, wx.ID_CANCEL, "&Cancel")
        exportbutton = wx.Button(self.panel, wx.ID_OK, "&Ok")
        buttonSizer.Add(cancelbutton, 1, wx.ALL | wx.ALIGN_CENTER, 5)
        buttonSizer.Add(exportbutton, 1, wx.ALL | wx.ALIGN_CENTER, 5)

        topSize.AddStretchSpacer()
        topSize.AddStretchSpacer()
        topSize.Add(buttonSizer, 1, wx.ALL | wx.ALIGN_CENTER, 1)

        self.panel.SetSizer(topSize)
        topSize.Fit(self.panel)
        self.panel.Fit()

        # clearDateButton.Bind(wx.EVT_BUTTON, self.clearDate)
        self.textbox.Bind(wx.EVT_TEXT, self.textEntered)
        self.listbox.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.editTimeSheet)
        cancelbutton.Bind(wx.EVT_BUTTON, self.cancel)
        exportbutton.Bind(wx.EVT_BUTTON, self.editTimeSheet)
        self.firstDate.Bind(wx.EVT_DATE_CHANGED, self.onDateChange)
        self.secondDate.Bind(wx.EVT_DATE_CHANGED, self.onDateChange)
        self.listbox.Bind(wx.EVT_LIST_COL_CLICK, self.headerClick)

        # self.timesheetIndex = dict()

        self.listbox.InsertColumn(0, 'Employee')
        self.listbox.InsertColumn(1, 'Date')
        index = 0

        timesheetList = session.query(TimeSheet).filter(TimeSheet.date.between(pyfristDate, pysecondDate)).all()
        for each in timesheetList:
            self.listbox.InsertStringItem(index, each.CrewName.name)
            self.listbox.SetStringItem(index, 1, each.date.strftime('%x'))
            self.listbox.SetItemData(index, each.id)
            index += 1

        self.listbox.SetColumnWidth(0, TIME_EMPLOYEE_WIDTH)


    def editTimeSheet(self, e):
        if self.listbox.GetSelectedItemCount() == 1:
            timesheetId = self.listbox.GetItemData(self.listbox.GetNextSelected(-1))
            self.loadTimeSheet(timesheetId)


    def loadTimeSheet(self, timesheetId):
        mywin = EditTimeSheet(self.parent,style=wx.MAXIMIZE_BOX | wx.RESIZE_BORDER
                            | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX, title='TimeSheet Editor',timesheet_id=timesheetId, UpdateFunction=self.textEntered)
        if self.checkClose.GetValue():
            self.Destroy()



    def headerClick(self, e):
        old_orderby = self.order_by

        if e.GetColumn() == 0:
          if self.order_by == CrewNames.name:
            self.order_by = CrewNames.name.desc()
          else:
            self.order_by = CrewNames.name
        if e.GetColumn() == 1:
          if self.order_by == TimeSheet.date:
            self.order_by = TimeSheet.date.desc()
          else:
            self.order_by = TimeSheet.date

        if old_orderby != self.order_by:
          self.textEntered(1)

    def clearDate(self, e):
        self.filterDates = False
        self.textEntered(1)

    def onDateChange(self, e):
        # print "called"
        self.filterDates = True
        self.textEntered(1)

    def textEntered(self, e):
        try:
            value = self.textbox.GetValue()
            DontSkip = True
        except wx._core.PyDeadObjectError:
            #window already closed skip event handler
            DontSkip = False

        if DontSkip:
            timesheetList = session.query(TimeSheet).join(CrewNames).order_by(self.order_by).filter(CrewNames.name.like('%' + value + '%'))
            # print timesheetList.count()

            if self.filterDates:
                pyfirstDate = datetime.fromtimestamp(self.firstDate.GetValue().GetTicks()).date()
                pysecondDate = datetime.fromtimestamp(self.secondDate.GetValue().GetTicks()).date()
                timesheetList = timesheetList.filter(TimeSheet.date.between(pyfirstDate, pysecondDate))

            # print timesheetList.count()
            timesheetList.all()

            self.listbox.ClearAll()
            self.listbox.InsertColumn(0, 'Employee')
            self.listbox.InsertColumn(1, 'Date')

            index = 0
            for each in timesheetList:
                self.listbox.InsertStringItem(index, each.CrewName.name)
                self.listbox.SetStringItem(index, 1, each.date.strftime('%x'))
                self.listbox.SetItemData(index, each.id)
                index += 1
            self.listbox.SetColumnWidth(0, TIME_EMPLOYEE_WIDTH)

    def cancel(self, e):
        self.Destroy()

import wx
import  wx.calendar
import calendar
from database import *
from datetime import datetime, timedelta
import os.path
import xlsxwriter
import decimal
from decimal import Decimal
from sqlalchemy import extract
from sqlalchemy.orm.exc import NoResultFound
from xlsxwriter.utility import xl_rowcol_to_cell
from report_class import Report
from message_class import *
from edit_timesheet import EditTimeSheet
from utility_functions import pydate2wxdate, reportsaveDir
from itertools import groupby
# import wx.lib.agw.ultimatelistctrl as ULC

#report column types
listbox_TotalWidth = 650
COL_AUTOWIDTH = -1
COL_REPORT_ADD = 2
COL_REPORT_STRING = 4
COL_REPORT_PERCENT = 8
COL_REPORT_ROWHEADER = 16
COL_REPORT_TOTALCOL = 32
COL_ROWHEADERID = -1
COL_TOTALCOLID = -2


class Columns(object):
    def __init__(self, listbox_TotalWidth):
        #all stored by col_ids
        decimal.getcontext().prec = 3
        self.__ColumnHeaders__ = dict()
        self.__ColumnTypes__ = dict() # COL_REPORT_STRING or COL_REPORT_ADD or COL_REPORT_PERCENT
        self.__ColumnWidths__ = dict()
        self.__ColumnHeaderWidths__ = dict()
        self.__ColumnShortHeaderWidths__ = dict()
        self.__ShortNameSet__ = False
        self.AutoWidth = False
        self.listbox_TotalWidth = listbox_TotalWidth
        dc = wx.MemoryDC()
        dc.SelectObject(wx.NullBitmap)
        dc.SetFont(wx.SystemSettings_GetFont(wx.SYS_DEFAULT_GUI_FONT))
        self.DrawingContext =  dc

    @property
    def listbox_TotalWidth(self):
        if self.AutoWidth:
            #calculate listbox width
            # print "using AutoWidth"
            useShort = self.UseShortNames
            thisWidth = self.GetTotalWidth(useShort) + 23
            # print thisWidth
            return thisWidth
        else:
            return self.__listbox_TotalWidth__

    @listbox_TotalWidth.setter
    def listbox_TotalWidth(self, value):
        if value == COL_AUTOWIDTH:
            self.AutoWidth = True
            self.__listbox_TotalWidth__ = -1
        else:
            self.__listbox_TotalWidth__ = value


    def getWidth(self, col_id):
        if col_id in self.__ColumnWidths__:
            if self.UseShortNames() and col_id in self.__ColumnShortHeaderWidths__:
                ret_val = self.__ColumnWidths__[col_id] if self.__ColumnWidths__[col_id] > self.__ColumnShortHeaderWidths__[col_id] else self.__ColumnShortHeaderWidths__[col_id]
            else:
                ret_val = self.__ColumnWidths__[col_id] if self.__ColumnWidths__[col_id] > self.__ColumnHeaderWidths__[col_id] else self.__ColumnHeaderWidths__[col_id]
            return ret_val + 15
        else:
            raise KeyError(str(col_id))

    # def setDC(self, DC):
    #     self.DrawingContext = DC

    def GetTotalWidth(self, UseShortNames=False):
        TotalWidth = 0
        for each in self.__ColumnWidths__:
            if UseShortNames and each in self.__ColumnShortHeaderWidths__:
                headerWidth = self.__ColumnShortHeaderWidths__[each]
            else:
                headerWidth = self.__ColumnHeaderWidths__[each]

            TotalWidth += max(self.__ColumnWidths__[each], headerWidth) + 15
        return TotalWidth

    def UseShortNames(self):
        # print "Short Name Check: {} vs total width {}".format(self.GetTotalWidth(), self.listbox_TotalWidth)
        if self.GetTotalWidth() > self.listbox_TotalWidth and self.__ShortNameSet__:
            ret_val = True
        else:
            ret_val =  False
        return ret_val

    def GetColumnType(self, Col_id):
        return self.__ColumnTypes__[Col_id]

    def __iter__(self):
        MessageClass.Debug('Using Short Names: {}'.format(self.UseShortNames()))
        LocalUseShortNames = self.UseShortNames()
        dict_keys = self.__ColumnHeaders__.keys()
        dict_keys = list(dict_keys)
        dict_keys.sort()
        if COL_ROWHEADERID in self.__ColumnHeaders__:
            yield (self.__ColumnHeaders__[COL_ROWHEADERID][0], COL_ROWHEADERID)
        for each in dict_keys:
            if each == COL_ROWHEADERID or each == COL_TOTALCOLID:
                continue
            #use short names if check function is true and one is set else use long name
            NameIndex = 1 if LocalUseShortNames and self.__ColumnHeaders__[each][1] else 0
            yield (self.__ColumnHeaders__[each][NameIndex], each)
        if COL_TOTALCOLID in self.__ColumnHeaders__:
            yield (self.__ColumnHeaders__[COL_TOTALCOLID][0], COL_TOTALCOLID)

    def HasTotalCol(self):
        if COL_TOTALCOLID in self.__ColumnHeaders__:
            return len(self.__ColumnHeaders__) - 1
        else:
            return False

    def AddColumn(self, Name, Short_Name=None, Col_id=None, Type=COL_REPORT_ADD):
        MessageClass.Debug('Calling AddColumn {}'.format(Name))
        self.__ShortNameSet__ = True if Short_Name else self.__ShortNameSet__
        if Col_id == None:
            raise ValueError('Column Cannot be added without a value')
            return None
        if self.DrawingContext:
            if not Col_id in self.__ColumnHeaders__:
                self.__ColumnHeaders__[Col_id] = (Name, Short_Name)
                self.__ColumnWidths__[Col_id] = 0
                self.__ColumnTypes__[Col_id] = Type
                self.__ColumnHeaderWidths__[Col_id], discard_height = self.DrawingContext.GetTextExtent(Name)
                if Short_Name:
                    self.__ColumnShortHeaderWidths__[Col_id], discard_height = self.DrawingContext.GetTextExtent(Short_Name)
                MessageClass.Debug('Column Added:{} {} [{}]'.format(Col_id, self.__ColumnHeaders__[Col_id], self.__ColumnTypes__[Col_id]))
            else:
                raise ValueError('Cannot have duplicate Column Ids')
                return None
        else:
            raise ValueError('DrawingContext not valid')
            return None

        return True

    def SubmitColumWidth(self, value, Col_id):
        if not self.DrawingContext.IsOk():
            raise UserWarning("ERROR!!! DrawingContext FAILURE!")
        newWidth, discard_height = self.DrawingContext.GetTextExtent(value)
        # print "checking width for:{} {} {}".format(value, Col_id, newWidth)
        if Col_id in self.__ColumnWidths__:
            if newWidth > self.__ColumnWidths__[Col_id]:
                self.__ColumnWidths__[Col_id] = newWidth
        else:
            MessageClass.Debug('KeyError: {}, Column does not exist.'.format(Col_id))

class RowData(object):

    def __init__(self, Column_Class):
        self.__RowHeader__ = ''
        self.DataObj = None
        self.__RowData__ = dict()
        self.Columns = Column_Class


    @property
    def name(self):
        return self.__RowHeader__

    @name.setter
    def name(self, value):
        self.__RowHeader__ = value
        self.Columns.SubmitColumWidth(value, COL_ROWHEADERID)

    def addData(self, col_id, value):
        Col_Type = self.Columns.GetColumnType(col_id)
        if Col_Type & COL_REPORT_ADD == COL_REPORT_ADD:
            # if not task_id in Employee_Hours.usedtask:
            #     Employee_Hours.usedtask.append(task_id)
            self.__RowData__[col_id] = self.__RowData__.get(col_id, 0) + value
            self.Columns.SubmitColumWidth(str(Decimal(self.__RowData__[col_id])/3600), col_id)
        if Col_Type & COL_REPORT_STRING == COL_REPORT_STRING:
            self.__RowData__[col_id] = value
            self.Columns.SubmitColumWidth(value, col_id)

    def SetDataObject(self, Data):
        self.DataObj = Data

    def GetDataObject(self):
        return self.DataObj


    def getRowTotal(self):
        total = 0
        for each in self.__RowData__:
            total += self.__RowData__[each]
        return total

    def __iter__(self):
        dict_keys = self.__RowData__.keys()
        dict_keys = list(dict_keys)
        dict_keys.sort()
        for each in dict_keys:
            yield (self.__RowData__[each], each)


    def getData(self):
        return self.__RowData__

class ReportViewWindow(wx.Dialog):
    def __init__(self, parent, Columns_Obj, Rows, Title, GridTitle, DetailWindow, HighlightAltRows=True):
        self.parent = parent
        self.DetailWindow = DetailWindow
        decimal.getcontext().prec = 3

        #build gui
        style = wx.MAXIMIZE_BOX | wx.RESIZE_BORDER | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX

        super(ReportViewWindow, self).__init__(parent, title=Title, style=style, size=(Columns_Obj.listbox_TotalWidth + 50,400))

        self.frameVbox = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.frameVbox)
        self.panel = wx.Panel(self)
        self.frameVbox.Add(self.panel, 1, flag=wx.EXPAND)


        #sizers
        vbox = wx.BoxSizer(wx.VERTICAL)
        buttonSizer = wx.BoxSizer(wx.HORIZONTAL)

        gridLabel = wx.StaticText(self.panel, label=GridTitle)
        font = wx.Font(16, wx.MODERN, wx.NORMAL, wx.NORMAL)
        gridLabel.SetFont(font)
        vbox.Add(gridLabel, 1, wx.ALIGN_CENTER, 1)



        self.listbox = wx.ListCtrl(self.panel, size=(Columns_Obj.listbox_TotalWidth, 300), style=wx.LC_REPORT| wx.BORDER_SUNKEN | wx.LC_SINGLE_SEL | wx.LC_HRULES | wx.LC_VRULES)

        DetailsButton = wx.Button(self.panel, wx.ID_ANY, 'View Details')
        CloseButton = wx.Button(self.panel, wx.ID_CANCEL, 'Close')
        SaveButton = wx.Button(self.panel, wx.ID_OK, 'Save')

        DetailsButton.Bind(wx.EVT_BUTTON, self.ViewDetails)
        self.listbox.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.ViewDetails)
        SaveButton.Bind(wx.EVT_BUTTON, self.exportReport)

        buttonSizer.Add(DetailsButton, 1, wx.ALIGN_RIGHT | wx.RIGHT, 15)
        buttonSizer.Add(CloseButton, 1, wx.ALIGN_RIGHT | wx.RIGHT, 15)
        buttonSizer.Add(SaveButton, 1, wx.ALIGN_RIGHT | wx.RIGHT, 15)

        vbox.Add(self.listbox, 1, wx.ALIGN_CENTER, 1)
        vbox.AddSpacer(5)
        vbox.Add(buttonSizer, 1, wx.ALIGN_RIGHT | wx.RIGHT , 25)

        self.panel.SetSizer(vbox)

        ColumnId_index = dict()
        index = 0
        for Column_name, Column_Id in Columns_Obj:
            self.listbox.InsertColumn(index, Column_name)
            MessageClass.Debug("Setting Column {} width to {}".format(index, Columns_Obj.getWidth(Column_Id)))
            self.listbox.SetColumnWidth(index, Columns_Obj.getWidth(Column_Id))
            ColumnId_index[Column_Id] = index
            index += 1

        TotalRow = dict()
        row = 0
        for eachRow in Rows:
            self.listbox.InsertStringItem(row, Rows[eachRow].name)
            DataObj = Rows[eachRow].GetDataObject()
            if DataObj:
                self.listbox.SetItemData(row, DataObj)
            else:
                self.listbox.SetItemData(row, eachRow)
            for eachCol in Rows[eachRow]:
                if Columns_Obj.HasTotalCol():
                    TotalRow[ColumnId_index[eachCol[1]]] = TotalRow.get( ColumnId_index[eachCol[1]], 0) + eachCol[0]
                if Columns_Obj.GetColumnType(eachCol[1]) & COL_REPORT_STRING == COL_REPORT_STRING:
                    self.listbox.SetStringItem(row, ColumnId_index[eachCol[1]], eachCol[0])
                else:
                    self.listbox.SetStringItem(row, ColumnId_index[eachCol[1]], str(Decimal(eachCol[0])/3600))
            if row % 2 and HighlightAltRows:
                self.listbox.SetItemBackgroundColour(row, "grey")
            else:
                self.listbox.SetItemBackgroundColour(row, "white")
            TotalCol = Columns_Obj.HasTotalCol()
            if TotalCol:
                self.listbox.SetStringItem(row, TotalCol, str(Decimal(Rows[eachRow].getRowTotal())/3600))
            row += 1


        if TotalCol:
            self.listbox.InsertStringItem(row, 'Totals')
            TotalTotal = int()
            for each in TotalRow:
                TotalTotal += TotalRow[each]
                self.listbox.SetStringItem(row, each, str(Decimal(TotalRow[each])/3600))
            self.listbox.SetStringItem(row, TotalCol, str(Decimal(TotalTotal)/3600))
            if row % 2 and HighlightAltRows:
                self.listbox.SetItemBackgroundColour(row, "grey")
            else:
                self.listbox.SetItemBackgroundColour(row, "white")


    def ViewDetails(self, e):
        sel = self.listbox.GetNextSelected(-1)
        if sel != -1:
            job_id = self.listbox.GetItemData(sel)
            if job_id != 0:
                self.DetailWindow(job_id)

    def exportReport(self, e):
        self.parent.exportReport()

class ReportDailyWindow(wx.Dialog):
    def __init__(self, parent):
        super(ReportDailyWindow, self).__init__(parent, style=wx.CAPTION, title='Daily Report', size=(425,250))

        self.panel = wx.Panel(self, wx.ID_ANY)
        self.parent = parent

        monthdate = wx.DateTime.Now()

        self.reportDate = wx.calendar.CalendarCtrl(self.panel, -1, monthdate)

        openButton = wx.Button(self.panel, wx.ID_ANY, "Open")
        cancelbutton = wx.Button(self.panel, wx.ID_CANCEL, "&Cancel")
        exportbutton = wx.Button(self.panel, wx.ID_OK, "&Export")

        top = wx.BoxSizer(wx.VERTICAL)
        row1 = wx.BoxSizer(wx.HORIZONTAL)
        row2 = wx.BoxSizer(wx.HORIZONTAL)

        row1.Add(self.reportDate,0, wx.ALIGN_CENTER |wx.RIGHT,10)
        row2.Add(openButton, 0, wx.ALIGN_CENTER |wx.LEFT, 10)
        row2.Add(cancelbutton, 0 , wx.ALIGN_CENTER |wx.LEFT, 10)
        row2.Add(exportbutton,0, wx.ALIGN_CENTER |wx.LEFT, 10)

        top.AddStretchSpacer()
        top.Add(row1,0,wx.ALIGN_CENTER,0)
        top.AddSpacer(15)
        top.Add(row2,0,wx.ALIGN_CENTER ,0)
        top.AddStretchSpacer()

        self.panel.SetSizer(top)
        top.Fit(self.panel)
        self.panel.Fit()

        openButton.Bind(wx.EVT_BUTTON, self.openReport)
        cancelbutton.Bind(wx.EVT_BUTTON, self.cancel)
        exportbutton.Bind(wx.EVT_BUTTON, self.EvtExportReport)

    def openReport(self, e):
        monthDate = datetime.fromtimestamp(self.reportDate.GetDate().GetTicks())
        hourlist = session.query(Hours).filter(Hours.date == monthDate)
        if hourlist.count() >= 1:
            self.monthDate = monthDate
            decimal.getcontext().prec = 3
            try:
                self.jobList = session.query(Jobs).join(Hours).filter(Hours.date == monthDate).all()
                self.hourResults = session.query(Hours).filter(Hours.date == monthDate)
                tasklist = session.query(Hours.task_id).group_by(Hours.task_id).filter(Hours.date == monthDate).all()
                self.tasklist = [i[0] for i in tasklist]
                self.tasklist = session.query(TaskList).filter(TaskList.id.in_(self.tasklist)).order_by(TaskList.name)

            except:
                MessageClass.Message('Failed to load job.', Type=MESS_PANIC)
                return

            Title = "Report for: {}".format(self.monthDate.strftime('%x'))
            GridTitle = 'Daily Report {}'.format(self.monthDate.strftime('%x'))


            self.Columns = Columns(COL_AUTOWIDTH)
            self.Columns.AddColumn('Jobs', Col_id=COL_ROWHEADERID, Type=COL_REPORT_STRING | COL_REPORT_ROWHEADER)
            self.Columns.AddColumn('Total', Col_id=COL_TOTALCOLID, Type=COL_REPORT_TOTALCOL)
            #add all the task we are going to use to the Columns class
            for each in self.tasklist:
                self.Columns.AddColumn(each.name, Short_Name=each.short_name, Col_id=each.id)

            # self.NumRows = self.crewnames.count()
            self.Job_Rows = dict()
            for each in self.jobList:
                self.Job_Rows[each.id] = RowData(self.Columns)
                self.Job_Rows[each.id].name = each.name
                # print self.Job_Rows[each.id].name

            for each in self.hourResults:
                self.Job_Rows[each.job_id].addData(each.task_id, each.number_seconds)

            mywin = ReportViewWindow(self, self.Columns, self.Job_Rows, Title, GridTitle, self.ViewDetails)
            mywin.CenterOnParent()
            mywin.ShowModal()
        else:
            MessageClass.Message('No Hour Entires for this date.')

    def ViewDetails(self, rowData):
        decimal.getcontext().prec = 3
        job_id = rowData
        try:
            self.jobObj = session.query(Jobs).filter(Jobs.id == job_id).first()
            self.hourList = session.query(Hours).join(TimeSheet).join(CrewNames).filter(Hours.date == self.monthDate).filter(Hours.job_id == job_id).order_by(CrewNames.name).order_by(Hours.date).all()
        except:
            MessageClass.Message('Database Error: Reports DailyWindow:ViewDetails', Type=MESS_PANIC)
            return

        self.job_title = self.jobObj.name
        if len(self.job_title) > 25:
            self.job_title = self.job_title[0:22] + "..."

        title = "{} Report for {}".format(self.job_title, self.monthDate.strftime("%x"))

        # COLEmployee = 1
        COLTask = 1
        COLStart = 2
        COLFinish = 3
        COLHours = 4

        self.Columns = Columns(COL_AUTOWIDTH)

        self.Columns.AddColumn('Employee', Col_id=COL_ROWHEADERID, Type=COL_REPORT_STRING | COL_REPORT_ROWHEADER)
        self.Columns.AddColumn('Task', Col_id=COLTask, Type=COL_REPORT_STRING)
        self.Columns.AddColumn('Start', Col_id=COLStart, Type=COL_REPORT_STRING)
        self.Columns.AddColumn('Finish', Col_id=COLFinish, Type=COL_REPORT_STRING)
        self.Columns.AddColumn('Hours', Col_id=COLHours, Type=COL_REPORT_STRING)

        Rows = dict()
        for each in self.hourList:
            Rows[each.id] = RowData(self.Columns)
            Rows[each.id].SetDataObject(each.timesheet_id)
            Rows[each.id].name = each.TimeSheet.CrewName.name
            thistask = session.query(TaskList).filter(TaskList.id == each.task_id).first()
            Rows[each.id].addData(COLTask, thistask.name)
            Rows[each.id].addData(COLStart, each.time_start.strftime('%H%M'))
            Rows[each.id].addData(COLFinish, each.time_finish.strftime('%H%M'))
            Rows[each.id].addData(COLHours, str(Decimal(each.number_seconds)/3600))

        mywin = ReportViewWindow(self, self.Columns, Rows, self.job_title, title, self.ShowTimeSheet)
        mywin.CenterOnParent()
        mywin.ShowModal()

    def ShowTimeSheet(self, timesheet_id):
        print "Showing timesheet {}".format(timesheet_id)
        mywin = EditTimeSheet(self,style=wx.MAXIMIZE_BOX | wx.RESIZE_BORDER | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX, title='TimeSheet Editor',timesheet_id=timesheet_id, UpdateFunction=None)

    def EvtExportReport(self, e):
        self.exportReport()
        self.Destroy()

    def exportReport(self):
        monthDate = datetime.fromtimestamp(self.reportDate.GetDate().GetTicks())
        reportname = 'DailyReport_' + monthDate.strftime('%b_%d_%Y') + '.xlsx'

        #load report path from settings table
        report_path = reportsaveDir()
        filename = os.path.join(report_path, reportname)

        #set reportname to full path name
        reportname = filename
        if not os.path.isfile(reportname):
            self.genReport(reportname,monthDate)
        else:
            question = 'The report ' + reportname + ' already exist.\nDo you want to overwrite it?'
            dlg = wx.MessageDialog(self, question, 'Report Exist!', wx.YES_NO | wx.ICON_ERROR)
            result = dlg.ShowModal()
            dlg.Destroy()
            if result == wx.ID_YES:
                self.genReport(reportname,monthDate)

    def genReport(self, reportname, monthDate):

        format_bold = {'bold' : True,'border' : 1}
        myReport = Report(self.parent)
        myReport.InitReport(reportname)
        myReport.setReportName('Daily Report', format_bold)
        myReport.setSubject(monthDate.strftime('%B, %d %Y'))
        myReport.setRowHeader('Job Names', format_bold)
        myReport.add_Format('SeperatorColHeader_Format', {'bold' : True,'border' : 1, 'align': 'right'})
        myReport.add_Format('RowHeader_Format', {'align' : 'center', 'bold': True, 'border' : 1})
        myReport.add_Format('RowNames_Format', {'align': 'center', 'border' : 1})
        myReport.add_Format('ColData_Format',{'align':'center', 'border': 1})
        myReport.add_Format('TotalRows_Format', {'align' : 'center', 'border': 1})
        myReport.add_Format('TotalCols_Format', {'align' : 'center', 'border': 1})
        myReport.add_Format('SeperatorCol_Format', {'bg_color' : '#D8D8D8'})
        myReport.add_Format('ColHeaders_Format', {'align' : 'center', 'bold': True, 'border' : 1})

        myTaskList = session.query(TaskList).all()

        TaskCols = dict()
        for each in myTaskList:
            TaskCols[each.id] = myReport.addColHeader(each.id, each.name, each.short_name)


        jobList = session.query(Jobs).join(Hours)
        jobList = jobList.filter(Hours.date == monthDate).all()

        JobDict = dict()
        for index, each in enumerate(jobList):
            #verify len of job name: excel max lenght is 31
            if len(each.name) > 31:
                jobList[index].name = jobList[index].name[:28] + "..."
                each.name = jobList[index].name
            jobid = myReport.addRowName(each.name, each.id)
            myReport.RowNameLinks[jobid] = """internal:'{}'!A1""".format(each.name)
            JobDict[each.id] = jobid

        hourList = session.query(Hours).filter(Hours.date == monthDate)
        if hourList.count() >= 1:
            hourList = hourList.all()

            monthly_total_seconds = list()
            for eachHour in hourList:
                print "doing job {}".format(eachHour.job_id)
                #skip adding if job was done after hours
                if eachHour.time_finish <= eachHour.TimeSheet.time_out:
                    # print monthly_total_seconds," plus ",eachHour.number_seconds
                    print "job added whole time"
                    add_time =  eachHour.time_finish - eachHour.time_start
                else:
                    job_start = eachHour.time_start
                    clockout_time = eachHour.TimeSheet.time_out

                    #if this job is completely after hours dont add anytime.
                    if job_start >= clockout_time:
                        print "job added no time"
                        add_time = None
                    else:
                        print "job added partial time"
                        add_time = clockout_time - job_start
                if add_time:
                    monthly_total_seconds.append(add_time.seconds)


                myReport.addColData(eachHour.job_id, eachHour.task_id, eachHour.number_seconds)



            timesheetList = session.query(TimeSheet).filter(TimeSheet.date == monthDate).all()

            monthly_Clk_time = list()
            for eachSheet in timesheetList:
                td = eachSheet.time_out - eachSheet.time_in
                if eachSheet.lunch:
                    lunch_td = timedelta(minutes = eachSheet.lunch)
                    # print 'lunch td {}'.format(lunch_td)
                    td = td - lunch_td
                    # print 'subtracting lunch'
                # print eachSheet.id, Decimal(td.total_seconds())/3600
                monthly_Clk_time.append(td.total_seconds())

            monthly_Clk_time_sum = sum(monthly_Clk_time)
            monthly_total_seconds_sum = sum(monthly_total_seconds)
            shop_hours = Decimal(monthly_Clk_time_sum - monthly_total_seconds_sum)/3600

            #lets add extra headers and such
            myReport.addCellMergeData(0, myReport.getNumCols() - 1, 0, myReport.getNumCols()+1, 'Shop Hours:',{'bold' : True, 'align': 'right'})
            myReport.addCellData(0, myReport.getNumCols()+2, shop_hours)


            myReport.addCellData(len(JobDict) + 3,0,'Totals: ',{'bold':True,'align':'right','border':1})
            newFormat = myReport.Formats['SeperatorCol_Format'].copy()
            newFormat['border'] = 1
            myReport.addCellData(len(JobDict) + 3, 1, '', newFormat)
            myReport.addCellData(2,myReport.getNumCols()+2,'Totals',{'bold':True,'align':'center','border':1})
            total_string ='=SUM(' + str(xl_rowcol_to_cell(3,myReport.getNumCols()+2)) + ':' + str(xl_rowcol_to_cell(len(JobDict) + 2,myReport.getNumCols()+2)) + ')'
            myReport.addCellData(len(JobDict) + 3, myReport.getNumCols()+2, total_string, {'bold':True,'align':'center','border':1})
            myReport.createReport(False)


            #add extra pages for Details
            COL_DATE = 1
            COL_TASK = 2
            COL_TIMEIN = 3
            COL_TIMEOUT = 4
            COL_HOURS = 5


            for eachJob in jobList:
                # print 'adding page for:', eachJob.name
                # myReport.addWorkSheet(each.TimeSheet.CrewName.name)
                myReport.addWorkSheet(eachJob.name)
                myReport.TotalCols = False
                myReport.TotalRows = False
                myReport.SeperatorCol = ''
                myReport.setReportName(eachJob.name,format_bold)
                myReport.setSubject(monthDate.strftime('%B, %d %Y'), link="""internal:'Sheet1'!A1""")
                myReport.setRowHeader('Employee', format_bold)

                myReport.SetColumnWidth(COL_DATE, 9)
                myReport.SetColumnWidth(COL_TASK, 14)
                myReport.addColHeader(COL_DATE, 'Date','Date')
                myReport.addColHeader(COL_TASK, 'Task', 'Task')
                myReport.addColHeader(COL_TIMEIN, 'Time In', 'Time In')
                myReport.addColHeader(COL_TIMEOUT, 'Time Out', 'Time Out')
                myReport.addColHeader(COL_HOURS, 'Hours','Hours')

                hourList = session.query(Hours).join(TimeSheet).filter(Hours.job_id == eachJob.id)
                hourList = hourList.filter(Hours.date == monthDate).all()

                #set up row names

                #put in data
                row = 0
                for eachHour in hourList:
                    # print "add row",employee.TimeSheet.CrewName.name,eachHour.Task.name
                    rowid = myReport.addRowName(eachHour.TimeSheet.CrewName.name, row)
                    myReport.addColData(rowid, COL_DATE, eachHour.date.strftime('%x'))
                    myReport.addColData(rowid, COL_TASK, eachHour.Task.name)
                    myReport.addColData(rowid, COL_TIMEIN, str(eachHour.time_start.strftime('%I:%M%p')))
                    myReport.addColData(rowid, COL_TIMEOUT, str(eachHour.time_finish.strftime('%I:%M%p')))
                    myReport.addColData(rowid, COL_HOURS, eachHour.number_seconds)
                    row += 1

                myReport.createReport(False)

            myReport.saveReport()

            # self.Destroy()

        else:
            MessageClass.Message('No Hour entries for this date.', Duration=3000)

    def cancel(self, e):
        self.Destroy()

class ReportDetailJobViewWindow(wx.Dialog):
    def __init__(self, parent, jobObj, employee_id):
        decimal.getcontext().prec = 3
        self.jobObj = jobObj
        self.employeeOBJ = session.query(CrewNames).filter(CrewNames.id == employee_id).one()
        self.hourList = session.query(Hours).join(TimeSheet).filter(Hours.Job == self.jobObj).filter(TimeSheet.CrewNames_id == employee_id).order_by(Hours.date.desc()).order_by(Hours.time_start.desc())

        style = wx.MAXIMIZE_BOX | wx.RESIZE_BORDER | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX
        title = "{}".format(self.jobObj.name)
        super(ReportDetailJobViewWindow, self).__init__(parent, title=title, style=style, size=(550,400))

        self.InitUI()

    def InitUI(self):
        decimal.getcontext().prec = 3
        self.frameVbox = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.frameVbox)
        self.panel = wx.Panel(self)
        self.frameVbox.Add(self.panel, 1, flag=wx.EXPAND)

        #sizers
        vbox = wx.BoxSizer(wx.VERTICAL)
        buttonSizer = wx.BoxSizer(wx.HORIZONTAL)

        gridLabel = wx.StaticText(self.panel, label='Report for {}'.format(self.employeeOBJ.name))
        font = wx.Font(14, wx.MODERN, wx.NORMAL, wx.NORMAL)
        gridLabel.SetFont(font)
        vbox.Add(gridLabel, 1, wx.ALIGN_CENTER, 1)

        self.listbox = wx.ListCtrl(self.panel, size=(400, 300), style=wx.LC_REPORT| wx.BORDER_SUNKEN | wx.LC_SINGLE_SEL)

        self.listbox.InsertColumn(0, 'Task')
        self.listbox.InsertColumn(1, 'Date')
        self.listbox.InsertColumn(2, 'Start')
        self.listbox.InsertColumn(3, 'Finish')
        self.listbox.InsertColumn(4, 'Hours')

        self.listbox.SetColumnWidth(0, 100)
        self.listbox.SetColumnWidth(1, 65)
        self.listbox.SetColumnWidth(2, 65)
        self.listbox.SetColumnWidth(3, 65)
        self.listbox.SetColumnWidth(4, 65)

        row = 0
        for each in self.hourList:
            self.listbox.InsertStringItem(row, each.Task.name)
            self.listbox.SetStringItem(row, 1, each.date.strftime('%x'))
            self.listbox.SetStringItem(row, 2, each.time_start.strftime('%H%M'))
            self.listbox.SetStringItem(row, 3, each.time_finish.strftime('%H%M'))
            self.listbox.SetStringItem(row, 4, str(Decimal(each.number_seconds)/3600))
            self.listbox.SetItemData(row, each.timesheet_id)

        CloseButton = wx.Button(self.panel, wx.ID_CANCEL, 'Close')

        self.listbox.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.ViewTimesheet)

        buttonSizer.Add(CloseButton, 1, wx.ALIGN_RIGHT | wx.RIGHT, 15)

        vbox.Add(self.listbox, 1, wx.ALIGN_CENTER, 1)
        vbox.AddSpacer(5)
        vbox.Add(buttonSizer, 1, wx.ALIGN_RIGHT | wx.RIGHT , 25)

        self.panel.SetSizer(vbox)

    def ViewTimesheet(self, e):
        sel = self.listbox.GetNextSelected(-1)
        if sel != -1:
            timesheet_id = self.listbox.GetItemData(sel)
            mywin = EditTimeSheet(self,style=wx.MAXIMIZE_BOX | wx.RESIZE_BORDER | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX, title='TimeSheet Editor',timesheet_id=timesheet_id, UpdateFunction=None)

class ReportJobWindow(wx.Dialog):
    def __init__(self, parent, job_id=-1):
        super(ReportJobWindow, self).__init__(parent, style=wx.CAPTION, title='Job Report', size=(250, 350))

        if job_id != -1:
            # we were sent a job id
            self.openReport(job_id)
        else:
            #if we dont have a job id lets get one
            self.parent = parent
            self.panel = wx.Panel(self, wx.ID_ANY)

            topSize = wx.BoxSizer(wx.VERTICAL)
            dateSizer = wx.BoxSizer(wx.HORIZONTAL)
            buttonSizer = wx.BoxSizer(wx.HORIZONTAL)


            pyfristDate = datetime.now() - timedelta(days = 30)
            pysecondDate = datetime.now() + timedelta(days = 1)
            date_firstDate = pydate2wxdate(pyfristDate)
            date_secondDate = pydate2wxdate(pysecondDate)

            self.firstDate = wx.DatePickerCtrl(self.panel, dt=date_firstDate, style=wx.DP_DROPDOWN)
            self.secondDate = wx.DatePickerCtrl(self.panel, dt=date_secondDate, style=wx.DP_DROPDOWN)

            dateSizer.AddStretchSpacer(2)
            dateSizer.Add(self.firstDate, 1, wx.LEFT, 0)
            dateSizer.AddStretchSpacer(1)
            dateSizer.Add(self.secondDate, 1, wx.LEFT, 0)
            dateSizer.AddStretchSpacer(2)

            textTitle = wx.StaticText(self.panel, label="Search:")
            self.textbox = wx.TextCtrl(self.panel, size=(140, -1))
            listTitle = wx.StaticText(self.panel, label='Pick a Job:')
            self.listbox =  wx.ListCtrl(self.panel, size=(225, 175), style=wx.LC_REPORT| wx.BORDER_SUNKEN | wx.LC_SINGLE_SEL)


            topSize.AddSpacer(10)
            topSize.Add(dateSizer)
            topSize.AddSpacer(10)
            topSize.Add(listTitle, 1, wx.LEFT , 15)
            topSize.Add(self.listbox, 1, wx.LEFT, 10)
            topSize.AddStretchSpacer()
            topSize.Add(textTitle, 1, wx.LEFT, 15)
            topSize.Add(self.textbox, 1, wx.LEFT, 10)


            SaveButton = wx.Button(self.panel, wx.ID_ANY, "Report", style=wx.BU_EXACTFIT)
            cancelbutton = wx.Button(self.panel, wx.ID_CANCEL, "&Close", style=wx.BU_EXACTFIT)
            exportbutton = wx.Button(self.panel, wx.ID_OK, "&Open", style=wx.BU_EXACTFIT)
            buttonSizer.Add(SaveButton, 1, wx.ALL|wx.ALIGN_CENTER, 5)
            buttonSizer.Add(cancelbutton, 1, wx.ALL|wx.ALIGN_CENTER, 5)
            buttonSizer.Add(exportbutton, 1, wx.ALL|wx.ALIGN_CENTER, 5)

            topSize.AddStretchSpacer()
            topSize.Add(buttonSizer, 1, wx.ALL|wx.ALIGN_CENTER, 1)
            topSize.AddStretchSpacer()

            self.panel.SetSizer(topSize)
            topSize.Fit(self.panel)
            self.panel.Fit()

            self.textbox.Bind(wx.EVT_TEXT, self.textEntered)
            self.listbox.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.EVTopenReport)
            self.firstDate.Bind(wx.EVT_DATE_CHANGED, self.textEntered)
            self.secondDate.Bind(wx.EVT_DATE_CHANGED, self.textEntered)
            cancelbutton.Bind(wx.EVT_BUTTON, self.cancel)
            exportbutton.Bind(wx.EVT_BUTTON, self.EVTopenReport)
            SaveButton.Bind(wx.EVT_BUTTON, self.EvtExportReport)

            self.listbox.InsertColumn(0, 'Job')
            self.listbox.InsertColumn(1, 'Date')
            self.listbox.SetColumnWidth(0, 135)
            self.listbox.SetColumnWidth(1, 225-158)

            jobList = session.query(Jobs).order_by(Jobs.name).order_by(Jobs.create_date).filter(Jobs.create_date.between(pyfristDate, pysecondDate)).all()
            row = 0
            for eachJob in jobList:
                self.listbox.InsertStringItem(row, eachJob.name)
                self.listbox.SetStringItem(row, 1, eachJob.create_date.strftime('%x'))
                self.listbox.SetItemData(row, eachJob.id)
                row += 1

    def EvtExportReport(self, e):
        self.exportReport()
        self.Destroy()

    def exportReport(self):
        sel = self.listbox.GetNextSelected(-1)
        Jobid = self.listbox.GetItemData(sel)

        JobObj = session.query(Jobs).filter(Jobs.id == Jobid).first()


        job_date = JobObj.create_date
        reportname = 'JobReport_' + JobObj.name + job_date.strftime('%B') + str(job_date.date().year) + '.xlsx'

        #load report path from settings table
        report_path = reportsaveDir()
        filename = os.path.join(report_path, reportname)



        #set reportname to full path name
        reportname = filename


        if not os.path.isfile(reportname):
            self.genReport(reportname, JobObj)
        else:
            question = 'The report ' + reportname + ' already exist.\nDo you want to overwrite it?'
            dlg = wx.MessageDialog(self, question, 'Report Exist!', wx.YES_NO | wx.ICON_ERROR)
            result = dlg.ShowModal()
            dlg.Destroy()
            if result == wx.ID_YES:
                self.genReport(reportname, JobObj)

    def genReport(self, filename, JobObj):
        format_bold = {'bold' : True,'border' : 1}
        myReport = Report(self.parent)
        myReport.InitReport(filename)
        myReport.setReportName('Job Hour Report',format_bold)
        myReport.setSubject(JobObj.name, Format={'align' : 'right'})
        myReport.setRowHeader('Tech Names', format_bold)
        myReport.add_Format('SeperatorColHeader_Format', {'bold' : True,'border' : 1, 'align': 'right'})
        myReport.add_Format('RowHeader_Format', {'align' : 'center', 'bold': True, 'border' : 1})
        myReport.add_Format('RowNames_Format', {'align': 'center', 'border' : 1})
        myReport.add_Format('ColData_Format',{'align':'center', 'border': 1})
        myReport.add_Format('TotalRows_Format', {'align' : 'center', 'border': 1})
        myReport.add_Format('TotalCols_Format', {'align' : 'center', 'border': 1})
        myReport.add_Format('SeperatorCol_Format', {'bg_color' : '#D8D8D8'})
        myReport.add_Format('ColHeaders_Format', {'align' : 'center', 'bold': True, 'border' : 1})

        myTaskList = session.query(TaskList).all()

        TaskCols = dict()
        for each in myTaskList:
            TaskCols[each.id] = myReport.addColHeader(each.id, each.name, each.short_name)
            # print "added Task: " + str(each.id) + str(each.name)


        HourData = session.query(Hours).filter(Hours.job_id == JobObj.id).join(TimeSheet).group_by(TimeSheet.CrewNames_id).all()

        CrewDict = dict()
        for each in HourData:
            # print "added row", each.TimeSheet.CrewName.name
            rowid =  myReport.addRowName(each.TimeSheet.CrewName.name, each.TimeSheet.CrewNames_id)
            myReport.RowNameLinks[rowid] = """internal:'{}'!A1""".format(each.TimeSheet.CrewName.name)
            CrewDict[each.TimeSheet.CrewNames_id] = rowid


        hourList = session.query(Hours).filter_by(job_id = JobObj.id).all()

        for eachHour in hourList:
            # print eachHour.number_seconds
            # print eachHour.TimeSheet.CrewName.name
            myReport.addColData(eachHour.TimeSheet.CrewNames_id, eachHour.task_id,eachHour.number_seconds)

        #lets add extra headers and such
        myReport.addCellData(len(CrewDict) + 3,0,'Totals: ',{'bold':True,'align':'right','border':1})
        newFormat = myReport.Formats['SeperatorCol_Format'].copy()
        newFormat['border'] = 1
        myReport.addCellData(len(CrewDict) + 3, 1, '', newFormat)
        myReport.addCellData(2,myReport.getNumCols()+2,'Totals',{'bold':True,'align':'center','border':1})
        total_string ='=SUM(' + str(xl_rowcol_to_cell(3,myReport.getNumCols()+2)) + ':' + str(xl_rowcol_to_cell(len(CrewDict) + 2,myReport.getNumCols()+2)) + ')'
        myReport.addCellData(len(CrewDict) + 3, myReport.getNumCols()+2, total_string, {'bold':True,'align':'center','border':1})
        myReport.createReport(False)

        COL_DATE = 1
        COL_TIMEIN = 2
        COL_TIMEOUT = 3
        COL_HOURS = 4

        HourData = session.query(Hours).join(TimeSheet).filter(Hours.job_id == JobObj.id).group_by(TimeSheet.CrewNames_id)


        for employee in HourData:
            # print 'adding page for:', employee.TimeSheet.CrewName.name
            # myReport.addWorkSheet(each.TimeSheet.CrewName.name)
            myReport.addWorkSheet(employee.TimeSheet.CrewName.name)
            myReport.TotalCols = False
            myReport.TotalRows = False
            myReport.SeperatorCol = ''
            myReport.setReportName(employee.TimeSheet.CrewName.name,format_bold)
            myReport.setSubject(JobObj.name, Format={'align' : 'right'}, link="""internal:'Sheet1'!A1""")
            myReport.setRowHeader('Entries', format_bold)

            myReport.SetColumnWidth(COL_DATE, 9)
            myReport.addColHeader(COL_DATE, 'Date','Date')
            myReport.addColHeader(COL_TIMEIN, 'Time In', 'Time In')
            myReport.addColHeader(COL_TIMEOUT, 'Time Out', 'Time Out')
            myReport.addColHeader(COL_HOURS, 'Hours','Hours')

            hourList = session.query(Hours).join(TimeSheet).filter(Hours.job_id == JobObj.id)
            hourList = hourList.filter(TimeSheet.CrewNames_id == employee.TimeSheet.CrewNames_id).all()

            #set up row names

            #put in data
            row = 0
            for eachHour in hourList:
                # print "add row",employee.TimeSheet.CrewName.name,eachHour.Task.name
                rowid = myReport.addRowName(eachHour.Task.name, row)
                myReport.addColData(rowid, COL_DATE, eachHour.date.strftime('%x'))
                myReport.addColData(rowid, COL_TIMEIN, str(eachHour.time_start.strftime('%I:%M%p')))
                myReport.addColData(rowid, COL_TIMEOUT, str(eachHour.time_finish.strftime('%I:%M%p')))
                myReport.addColData(rowid, COL_HOURS, eachHour.number_seconds)
                row += 1

            myReport.createReport(False)

        myReport.saveReport()


        # self.Destroy()

    def EVTopenReport(self, e):
        if self.listbox.GetSelectedItemCount > 0:
            sel = self.listbox.GetNextSelected(-1)
            Jobid = self.listbox.GetItemData(sel)
            if Jobid != 0:
                self.openReport(Jobid)


    def openReport(self, job_id):
                        decimal.getcontext().prec = 3
                        print job_id
                        try:
                            self.jobObj = session.query(Jobs).filter(Jobs.id == job_id).one()
                            self.crewnames = session.query(CrewNames).join(TimeSheet).join(Hours).filter(Hours.job_id == job_id).group_by(CrewNames.name).order_by(CrewNames.name)
                            self.hourResults = session.query(Hours).join(TimeSheet).filter(Hours.job_id == job_id).all()
                            tasklist = session.query(Hours.task_id).filter(Hours.job_id == job_id).all()
                            self.tasklist = [i[0] for i in tasklist]
                            self.tasklist = session.query(TaskList).filter(TaskList.id.in_(self.tasklist)).order_by(TaskList.name)

                        except:
                            MessageClass.Message('Failed to load job.', Type=MESS_PANIC)
                            return

                        self.Columns = Columns(COL_AUTOWIDTH)

                        self.Columns.AddColumn('Jobs', Col_id=COL_ROWHEADERID, Type=COL_REPORT_STRING | COL_REPORT_ROWHEADER)
                        self.Columns.AddColumn('Total', Col_id=COL_TOTALCOLID, Type=COL_REPORT_TOTALCOL)
                        #add all the task we are going to use to the Columns class
                        for each in self.tasklist:
                            self.Columns.AddColumn(each.name, Short_Name=each.short_name, Col_id=each.id)

                        self.Rows = dict()
                        for each in self.crewnames:
                            self.Rows[each.id] = RowData(self.Columns)
                            self.Rows[each.id].name = each.name
                            # print self.Rows[each.id].name

                        for each in self.hourResults:
                            self.Rows[each.TimeSheet.CrewNames_id].addData(each.task_id, each.number_seconds)


                        title = "Report for: {}".format(self.jobObj.name)
                        gridLabel = 'Job Report'



                        mywin = ReportViewWindow(self, self.Columns, self.Rows, title, gridLabel, self.ViewDetails)
                        # mywin.CenterOnParent()
                        mywin.ShowModal()

    def ViewDetails(self, rowData):
        mywin = ReportDetailJobViewWindow(self, self.jobObj, rowData)
        mywin.CenterOnParent()
        mywin.ShowModal()

    def textEntered(self, e):
        value = self.textbox.GetValue()

        pyfirstDate = datetime.fromtimestamp(self.firstDate.GetValue().GetTicks())
        pysecondDate = datetime.fromtimestamp(self.secondDate.GetValue().GetTicks())

        jobList = session.query(Jobs).order_by(Jobs.name).order_by(Jobs.create_date).filter(Jobs.name.like('%' + value + '%'))
        jobList = jobList.filter(Jobs.create_date.between(pyfirstDate, pysecondDate)).all()
        self.listbox.ClearAll()
        self.listbox.InsertColumn(0, 'Job')
        self.listbox.InsertColumn(1, 'Date')

        self.listbox.SetColumnWidth(0, 135)
        self.listbox.SetColumnWidth(1, 225-158)

        row = 0
        for eachJob in jobList:
            self.listbox.InsertStringItem(row, eachJob.name)
            self.listbox.SetStringItem(row, 1, eachJob.create_date.strftime('%x'))
            self.listbox.SetItemData(row, eachJob.id)
            row += 1

    def cancel(self, e):
        self.Destroy()

class ReportMontlyWindow(wx.Dialog):
    def __init__(self, parent):
        super(ReportMontlyWindow, self).__init__(parent, style=wx.CAPTION, title='Monthly Report', size=(320,150))

        self.panel = wx.Panel(self, wx.ID_ANY)
        self.parent = parent
        # monthdate = wx.DateTime.Now()
        monthdate = datetime.now()

        datelabel = wx.StaticText(self.panel, label="Month:")


        #Build a list of years that are used in the Database
        yearlist = list()
        yearquery = session.query(Hours).order_by(Hours.date)
        for (year, items) in groupby(yearquery, lambda item: item.date.year):
            yearlist.append(str(year))



        self.reportMonth = wx.Choice(self.panel)
        for i in range(1,13):
            index = self.reportMonth.Append(calendar.month_name[i], i)

        self.reportMonth.SetSelection(self.reportMonth.FindString(monthdate.strftime('%B')))


        self.reportYear = wx.Choice(self.panel, choices=yearlist)
        self.reportYear.SetSelection(self.reportYear.FindString(str(monthdate.year)))


        cancelbutton = wx.Button(self.panel, wx.ID_CANCEL, "&Cancel")
        exportbutton = wx.Button(self.panel, wx.ID_OK, "&Export")
        openButton = wx.Button(self.panel, wx.ID_ANY, "Open")

        top = wx.BoxSizer(wx.VERTICAL)
        row1 = wx.BoxSizer(wx.HORIZONTAL)
        row2 = wx.BoxSizer(wx.HORIZONTAL)

        row1.Add(datelabel,0, wx.ALIGN_CENTER | wx.RIGHT,5)
        row1.Add(self.reportMonth,0, wx.ALIGN_CENTER |wx.RIGHT,10)
        row1.Add(self.reportYear,0, wx.ALIGN_CENTER |wx.RIGHT,10)
        row2.Add(cancelbutton, 0 , wx.ALIGN_CENTER |wx.LEFT, 10)
        row2.Add(exportbutton,0, wx.ALIGN_CENTER |wx.LEFT, 10)
        row2.Add(openButton, 0, wx.ALIGN_CENTER |wx.LEFT, 10)

        top.AddStretchSpacer()
        top.Add(row1,0,wx.ALIGN_CENTER,0)
        top.AddSpacer(15)
        top.Add(row2,0,wx.ALIGN_CENTER ,0)
        top.AddStretchSpacer()

        self.panel.SetSizer(top)
        top.Fit(self.panel)
        self.panel.Fit()

        cancelbutton.Bind(wx.EVT_BUTTON, self.cancel)
        exportbutton.Bind(wx.EVT_BUTTON, self.EvtExportReport)
        openButton.Bind(wx.EVT_BUTTON, self.openReport)

    def getMonthDate(self):
        thisMonth = self.reportMonth.GetClientData(self.reportMonth.GetSelection())
        thisYear = int(self.reportYear.GetStringSelection())
        return datetime(thisYear, thisMonth, 1)


    def openReport(self, e):

        monthDate = self.getMonthDate()#datetime.fromtimestamp(self.reportDate.GetValue().GetTicks())
        self.monthDate = monthDate
        reportTitle = "Monthly Report: {} {}".format(monthDate.strftime('%B'), str(monthDate.year))

        dateMonthDate = monthDate.date()
        first_day = date(dateMonthDate.year, dateMonthDate.month, 1)
        if(dateMonthDate.month != 12):
            last_day = date(dateMonthDate.year, dateMonthDate.month+1, 1)
        else:
            last_day = date(dateMonthDate.year+1,1,1)

        jobList = session.query(Jobs).join(Hours).filter(Hours.date.between(first_day, last_day)).all()
        hourList = session.query(Hours).filter(Hours.date.between(first_day, last_day)).all()

        taskquery = session.query(Hours.task_id).filter(Hours.date.between(first_day, last_day)).all()
        tasklist =  [i[0] for i in taskquery]
        tasklist = session.query(TaskList).filter(TaskList.id.in_(tasklist))

        # UseShortNames = True if tasklist.count() > 7 else False

        self.Columns = Columns(COL_AUTOWIDTH)

        self.Columns.AddColumn('Jobs', Col_id=COL_ROWHEADERID, Type=COL_REPORT_STRING | COL_REPORT_ROWHEADER)
        self.Columns.AddColumn('Total', Col_id=COL_TOTALCOLID, Type=COL_REPORT_TOTALCOL)
        #add all the task we are going to use to the Columns class
        for each in tasklist:
            self.Columns.AddColumn(each.name, Short_Name=each.short_name, Col_id=each.id)

        self.Rows = dict()
        for each in jobList:
            self.Rows[each.id] = RowData(self.Columns)
            self.Rows[each.id].name = each.name
            # print self.Rows[each.id].name

        for each in hourList:
            self.Rows[each.job_id].addData(each.task_id, each.number_seconds)

        mywin = ReportViewWindow(self, self.Columns, self.Rows, reportTitle, reportTitle, self.ViewDetails)
        mywin.CenterOnParent()
        mywin.ShowModal()

    def ViewDetails(self, rowData):
        if rowData != 0:
            monthDate = self.monthDate
            print monthDate
            dateMonthDate = monthDate.date()
            first_day = date(dateMonthDate.year, dateMonthDate.month, 1)
            if(dateMonthDate.month != 12):
                last_day = date(dateMonthDate.year, dateMonthDate.month+1, 1)
            else:
                last_day = date(dateMonthDate.year+1,1,1)

            try:
                self.jobObj = session.query(Jobs).filter(Jobs.id == rowData).one()
                self.hourList = session.query(Hours).join(TimeSheet).join(CrewNames).filter(Hours.date.between(first_day, last_day)).filter(Hours.job_id == rowData).order_by(CrewNames.name).order_by(Hours.date).all()

            except:
                MessageClass.Message('Database Error', Type=MESS_PANIC)
                session.rollback()
                return

            Gridtitle = self.jobObj.name
            if len(Gridtitle) > 25:
                Gridtitle = Gridtitle[0:22] + "..."

            title = "Monthly Report: {} for {} {}".format(Gridtitle, monthDate.strftime('%B'), str(monthDate.year))

            Report_Columns = Columns(COL_AUTOWIDTH)

            Report_Columns.AddColumn('Employee', Col_id=COL_ROWHEADERID, Type=COL_REPORT_ROWHEADER | COL_REPORT_STRING)



            mywin = ReportDetailMonthViewWindow(self.parent, rowData, self.monthDate)
            mywin.CenterOnParent()
            mywin.ShowModal()

    def EvtExportReport(self, e):
        self.exportReport()
        self.Destroy()

    def exportReport(self):
        monthDate = self.getMonthDate()


        reportname = 'MontlyReport_' + monthDate.strftime('%B') + str(monthDate.year) + '.xlsx'

        #load report path from settings table
        report_path = reportsaveDir()
        filename = os.path.join(report_path, reportname)

        #set reportname to full path name
        reportname = filename
        CallExport = False
        if not os.path.isfile(reportname):
            CallExport = True
        else:
            question = 'The report ' + reportname + ' already exist.\nDo you want to overwrite it?'
            dlg = wx.MessageDialog(self, question, 'Report Exist!', wx.YES_NO | wx.ICON_ERROR)
            result = dlg.ShowModal()
            dlg.Destroy()
            if result == wx.ID_YES:
                CallExport = True

        if CallExport:
            self.genReport(reportname,monthDate)

    def genReport(self, reportname, monthDate):

        format_bold = {'bold' : True,'border' : 1}
        myReport = Report(self.parent)
        myReport.InitReport(reportname)
        myReport.setReportName('Montly Hour Report', format_bold)
        myReport.setSubject(monthDate.strftime('%B'))
        myReport.setRowHeader('Job Names', format_bold)
        myReport.add_Format('SeperatorColHeader_Format', {'bold' : True,'border' : 1, 'align': 'right'})
        myReport.add_Format('RowHeader_Format', {'align' : 'center', 'bold': True, 'border' : 1})
        myReport.add_Format('RowNames_Format', {'align': 'center', 'border' : 1})
        myReport.add_Format('ColData_Format',{'align':'center', 'border': 1})
        myReport.add_Format('TotalRows_Format', {'align' : 'center', 'border': 1})
        myReport.add_Format('TotalCols_Format', {'align' : 'center', 'border': 1})
        myReport.add_Format('SeperatorCol_Format', {'bg_color' : '#D8D8D8'})
        myReport.add_Format('ColHeaders_Format', {'align' : 'center', 'bold': True, 'border' : 1})

        myTaskList = session.query(TaskList).all()

        TaskCols = dict()
        for each in myTaskList:
            TaskCols[each.id] = myReport.addColHeader(each.id, each.name, each.short_name)

        dateMonthDate = monthDate.date()
        first_day = date(dateMonthDate.year, dateMonthDate.month, 1)
        if(dateMonthDate.month != 12):
            last_day = date(dateMonthDate.year, dateMonthDate.month+1, 1)
        else:
            last_day = date(dateMonthDate.year+1,1,1)

        jobList = session.query(Jobs).join(Hours)
        jobList = jobList.filter(Hours.date.between(first_day, last_day)).all()

        JobDict = dict()
        for each in jobList:
            JobDict[each.id] = myReport.addRowName(each.name, each.id)


        hourList = session.query(Hours).filter(Hours.date.between(first_day, last_day)).all()

        monthly_total_seconds = 0
        for eachHour in hourList:
            if eachHour.time_finish <= eachHour.TimeSheet.time_out:
                monthly_total_seconds += eachHour.number_seconds
            myReport.addColData(eachHour.job_id, eachHour.task_id,eachHour.number_seconds)


        timesheetList = session.query(TimeSheet).filter(TimeSheet.date.between(first_day, last_day)).all()

        monthly_Clk_time = 0
        for eachSheet in timesheetList:
            td = eachSheet.time_out - eachSheet.time_in
            if eachSheet.lunch:
                lunch_td = timedelta(minutes = eachSheet.lunch)
                td = td - lunch_td
                # print 'lunch td {}'.format(lunch_td)
            # print eachSheet.id, Decimal(td.total_seconds())/3600
            monthly_Clk_time = monthly_Clk_time + td.total_seconds()

        monthly_Clk_time = Decimal(monthly_Clk_time)/3600
        # print monthly_Clk_time

        monthly_total_seconds = Decimal(monthly_total_seconds)/3600

        shop_hours = monthly_Clk_time - monthly_total_seconds

        #lets add extra headers and such
        myReport.addCellMergeData(0, myReport.getNumCols(), 0, myReport.getNumCols()+1, 'Shop Hours:',{'bold' : True, 'align': 'right'})
        myReport.addCellData(0, myReport.getNumCols()+2, shop_hours)


        myReport.addCellData(len(JobDict) + 3,0,'Totals: ',{'bold':True,'align':'right','border':1})
        newFormat = myReport.Formats['SeperatorCol_Format'].copy()
        newFormat['border'] = 1
        myReport.addCellData(len(JobDict) + 3, 1, '', newFormat)
        myReport.addCellData(2,myReport.getNumCols()+2,'Totals',{'bold':True,'align':'center','border':1})
        total_string ='=SUM(' + str(xl_rowcol_to_cell(3,myReport.getNumCols()+2)) + ':' + str(xl_rowcol_to_cell(len(JobDict) + 2,myReport.getNumCols()+2)) + ')'
        myReport.addCellData(len(JobDict) + 3, myReport.getNumCols()+2, total_string, {'bold':True,'align':'center','border':1})
        myReport.createReport()


    def cancel(self, e):
        self.Destroy()

class ReportDetailMonthViewWindow(wx.Dialog):
    def __init__(self, parent, job_id, monthDate):
        decimal.getcontext().prec = 3
        self.monthDate = monthDate
        dateMonthDate = monthDate.date()
        first_day = date(dateMonthDate.year, dateMonthDate.month, 1)
        if(dateMonthDate.month != 12):
            last_day = date(dateMonthDate.year, dateMonthDate.month+1, 1)
        else:
            last_day = date(dateMonthDate.year+1,1,1)

        try:
            self.jobObj = session.query(Jobs).filter(Jobs.id == job_id).one()
            self.hourList = session.query(Hours).join(TimeSheet).join(CrewNames).filter(Hours.date.between(first_day, last_day)).filter(Hours.job_id == job_id).order_by(CrewNames.name).order_by(Hours.date).all()
        except:
            MessageClass.Message('Database Error', Type=MESS_PANIC)
            session.rollback()
            return

        self.job_title = self.jobObj.name
        if len(self.job_title) > 25:
            self.job_title = self.job_title[0:22] + "..."

        style = wx.MAXIMIZE_BOX | wx.RESIZE_BORDER | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX
        self.title = "Monthly Report: {} for {} {}".format(self.job_title, monthDate.strftime('%B'), str(monthDate.year))
        super(ReportDetailMonthViewWindow, self).__init__(parent, title=self.title, style=style, size=(550,400))

        self.InitUI()


    def InitUI(self):
        decimal.getcontext().prec = 3
        self.frameVbox = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.frameVbox)
        self.panel = wx.Panel(self)
        self.frameVbox.Add(self.panel, 1, flag=wx.EXPAND)

        #sizers
        vbox = wx.BoxSizer(wx.VERTICAL)
        buttonSizer = wx.BoxSizer(wx.HORIZONTAL)

        gridLabel = wx.StaticText(self.panel, label=self.title)
        font = wx.Font(12, wx.MODERN, wx.NORMAL, wx.NORMAL)
        gridLabel.SetFont(font)
        vbox.Add(gridLabel, 1, wx.ALIGN_CENTER, 1)

        self.listbox = wx.ListCtrl(self.panel, size=(485, 300), style=wx.LC_REPORT| wx.BORDER_SUNKEN | wx.LC_SINGLE_SEL)

        self.listbox.InsertColumn(0, 'Employee')
        self.listbox.InsertColumn(1, 'Task')
        self.listbox.InsertColumn(2, 'Date')
        self.listbox.InsertColumn(3, 'Start')
        self.listbox.InsertColumn(4, 'Finish')
        self.listbox.InsertColumn(5, 'Hours')

        self.listbox.SetColumnWidth(0, 100)
        self.listbox.SetColumnWidth(1, 85)
        self.listbox.SetColumnWidth(2, 85)
        self.listbox.SetColumnWidth(3, 65)
        self.listbox.SetColumnWidth(4, 65)
        self.listbox.SetColumnWidth(5, 50)

        row = 0
        for each in self.hourList:
            self.listbox.InsertStringItem(row, each.TimeSheet.CrewName.name)
            self.listbox.SetStringItem(row, 1, each.Task.name)
            self.listbox.SetStringItem(row, 2, each.date.strftime('%x'))
            self.listbox.SetStringItem(row, 3, each.time_start.strftime('%H%M'))
            self.listbox.SetStringItem(row, 4, each.time_finish.strftime('%H%M'))
            self.listbox.SetStringItem(row, 5, str(Decimal(each.number_seconds)/3600))
            self.listbox.SetItemData(row, each.timesheet_id)

        CloseButton = wx.Button(self.panel, wx.ID_CANCEL, 'Close')

        self.listbox.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.ViewTimesheet)


        buttonSizer.Add(CloseButton, 1, wx.ALIGN_RIGHT | wx.RIGHT, 15)

        vbox.Add(self.listbox, 1, wx.ALIGN_CENTER, 1)
        vbox.AddSpacer(5)
        vbox.Add(buttonSizer, 1, wx.ALIGN_RIGHT | wx.RIGHT , 25)

        self.panel.SetSizer(vbox)



    def ViewTimesheet(self, e):
        sel = self.listbox.GetNextSelected(-1)
        if sel != -1:
            timesheet_id = self.listbox.GetItemData(sel)
            mywin = EditTimeSheet(self,style=wx.MAXIMIZE_BOX | wx.RESIZE_BORDER | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX, title='TimeSheet Editor',timesheet_id=timesheet_id, UpdateFunction=None)

class ReportTaskWindow(wx.Dialog):
    def __init__(self, parent):
        super(ReportTaskWindow, self).__init__(parent, style=wx.CAPTION, title='Task Report', size=(325,200))

        self.panel = wx.Panel(self, wx.ID_ANY)
        self.parent = parent

        pyfristDate = datetime.now() - timedelta(days = 30)
        pysecondDate = datetime.now() + timedelta(days = 1)
        date_firstDate = pydate2wxdate(pyfristDate)
        date_secondDate = pydate2wxdate(pysecondDate)

        self.firstDate = wx.DatePickerCtrl(self.panel, dt=date_firstDate, style=wx.DP_DROPDOWN)
        firstText = wx.StaticText(self.panel, label="Start:")
        self.secondDate = wx.DatePickerCtrl(self.panel, dt=date_secondDate, style=wx.DP_DROPDOWN)
        secondText = wx.StaticText(self.panel, label="End:")

        taskText = wx.StaticText(self.panel, label="Task:")
        task = session.query(TaskList).filter_by(active=True).all()

        self.taskBox = wx.Choice(self.panel)
        for each in task:
            self.taskBox.Append(each.name, each.id)

        openButton = wx.Button(self.panel, wx.ID_ANY, "Open")
        cancelbutton = wx.Button(self.panel, wx.ID_CANCEL, "&Cancel")
        exportbutton = wx.Button(self.panel, wx.ID_OK, "&Export")

        top = wx.BoxSizer(wx.VERTICAL)
        row1 = wx.BoxSizer(wx.HORIZONTAL)
        row2 = wx.BoxSizer(wx.HORIZONTAL)
        row3 = wx.BoxSizer(wx.HORIZONTAL)

        row1.Add(firstText, 0, wx.ALIGN_CENTER | wx.RIGHT, 10)
        row1.Add(self.firstDate,0, wx.ALIGN_CENTER |wx.RIGHT,10)
        row1.Add(secondText, 0, wx.ALIGN_CENTER | wx.RIGHT, 10)
        row1.Add(self.secondDate, 0, wx.ALIGN_CENTER | wx.RIGHT, 10)

        row2.Add(taskText)
        row2.Add((5,0))
        row2.Add(self.taskBox, 0, wx.ALIGN_CENTER, 0)

        row3.Add(openButton, 0, wx.ALIGN_CENTER |wx.LEFT, 10)
        row3.Add(cancelbutton, 0 , wx.ALIGN_CENTER |wx.LEFT, 10)
        row3.Add(exportbutton,0, wx.ALIGN_CENTER |wx.LEFT, 10)

        top.AddStretchSpacer()
        top.Add(row1,0,wx.ALIGN_CENTER,0)
        top.AddSpacer(15)
        top.Add(row2,0,wx.ALIGN_CENTER ,0)
        top.AddSpacer(15)
        top.Add(row3, 0, wx.ALIGN_CENTER, 0)
        top.AddStretchSpacer()

        self.panel.SetSizer(top)
        top.Fit(self.panel)
        self.panel.Fit()

        openButton.Bind(wx.EVT_BUTTON, self.openReport)
        cancelbutton.Bind(wx.EVT_BUTTON, self.cancel)
        exportbutton.Bind(wx.EVT_BUTTON, self.exportReport)

    def openReport(self, e):
        item_id = self.taskBox.GetSelection()
        if item_id != wx.NOT_FOUND:
            report_taskid = self.taskBox.GetClientData(item_id)
            monthDate_first = datetime.fromtimestamp(self.firstDate.GetValue().GetTicks())
            monthDate_second = datetime.fromtimestamp(self.secondDate.GetValue().GetTicks())
            worked = True
            try:
                hourlist = session.query(Hours).filter(Hours.task_id == report_taskid).filter(Hours.date.between(monthDate_first, monthDate_second))
                hourlist = hourlist.order_by(Hours.date).order_by(Hours.job_id)
                job_query = session.query(Hours.job_id).filter(Hours.task_id == report_taskid).filter(Hours.date.between(monthDate_first, monthDate_second))
                job_ids = [i[0] for i in job_query]
                joblist = session.query(Jobs).filter(Jobs.id.in_(job_ids))


            except:
                worked = False
                MessageClass.Message('Could not load Infomation.', Caption='Database Error', Type=MESS_PANIC)

            taskname = session.query(TaskList).filter(TaskList.id == report_taskid).first()
            taskname = taskname.name
            reportTitle = "{} Report Between: {} {}".format(taskname, monthDate_first.strftime('%x'),monthDate_second.strftime('%x'))

            if worked:
                if hourlist.count() >= 1:
                    decimal.getcontext().prec = 3
                    self.Columns = Columns(COL_AUTOWIDTH)
                    self.Columns.AddColumn('Jobs', Col_id=COL_ROWHEADERID, Type=COL_REPORT_STRING | COL_REPORT_ROWHEADER)

                    COLDate = 2
                    COLStart = 3
                    COLFinish = 4
                    COLHours = 5



                    self.Columns.AddColumn('Date', Col_id=COLDate, Type=COL_REPORT_STRING)
                    self.Columns.AddColumn('Start', Col_id=COLStart, Type=COL_REPORT_STRING)
                    self.Columns.AddColumn('Finish', Col_id=COLFinish, Type=COL_REPORT_STRING)
                    self.Columns.AddColumn('Hours', Col_id=COLHours, Type=COL_REPORT_STRING)

                    self.Job_Rows = dict()
                    for each in joblist:
                        self.Job_Rows[each.id] = RowData(self.Columns)
                        self.Job_Rows[each.id].name = each.name

                    for each in hourlist:
                        self.Job_Rows[each.job_id].addData(COLDate,  each.date.strftime('%x'))
                        self.Job_Rows[each.job_id].addData(COLStart, each.time_start.strftime('%I:%M%p'))
                        self.Job_Rows[each.job_id].addData(COLFinish, each.time_finish.strftime('%I:%M%p'))

                        jobHourQuery = session.query(Hours.number_seconds).filter(Hours.task_id == report_taskid).filter(Hours.job_id == each.job_id)
                        self.Job_Rows[each.job_id].addData(COLHours, str(Decimal(sum([i[0] for i in jobHourQuery]))/3600))

                    mywin = ReportViewWindow(self, self.Columns, self.Job_Rows, reportTitle, taskname, self.ViewDetails)
                    mywin.CenterOnParent()
                    mywin.ShowModal()


                    # mywin = ReportViewWindow(self, monthDate)
                    # mywin.ShowModal()
                else:
                    MessageClass.Message('No Hour Entires for this date.')
        else:
            MessageClass.Message('Please Selecte a Task.', Caption='No Task Selected', Type=MESS_PANIC)

    def exportReport(self, e):

        pass
        # monthDate = datetime.fromtimestamp(self.reportDate.GetDate().GetTicks())
        #
        #
        # reportname = 'DailyReport_' + monthDate.strftime('%b_%d_%Y') + '.xlsx'
        #
        # #load report path from settings table
        # report_path = reportsaveDir()
        # filename = os.path.join(report_path, reportname)

        # #set reportname to full path name
        # reportname = filename
        # if not os.path.isfile(reportname):
        #     self.genReport(reportname,monthDate)
        # else:
        #     question = 'The report ' + reportname + ' already exist.\nDo you want to overwrite it?'
        #     dlg = wx.MessageDialog(self, question, 'Report Exist!', wx.YES_NO | wx.ICON_ERROR)
        #     result = dlg.ShowModal()
        #     dlg.Destroy()
        #     if result == wx.ID_YES:
        #         self.genReport(reportname,monthDate)

    def genReport(self, reportname, monthDate):

        format_bold = {'bold' : True,'border' : 1}
        myReport = Report(self.parent)
        myReport.InitReport(reportname)
        myReport.setReportName('Daily Report', format_bold)
        myReport.setSubject(monthDate.strftime('%B, %d %Y'))
        myReport.setRowHeader('Job Names', format_bold)
        myReport.add_Format('SeperatorColHeader_Format', {'bold' : True,'border' : 1, 'align': 'right'})
        myReport.add_Format('RowHeader_Format', {'align' : 'center', 'bold': True, 'border' : 1})
        myReport.add_Format('RowNames_Format', {'align': 'center', 'border' : 1})
        myReport.add_Format('ColData_Format',{'align':'center', 'border': 1})
        myReport.add_Format('TotalRows_Format', {'align' : 'center', 'border': 1})
        myReport.add_Format('TotalCols_Format', {'align' : 'center', 'border': 1})
        myReport.add_Format('SeperatorCol_Format', {'bg_color' : '#D8D8D8'})
        myReport.add_Format('ColHeaders_Format', {'align' : 'center', 'bold': True, 'border' : 1})

        myTaskList = session.query(TaskList).all()

        TaskCols = dict()
        for each in myTaskList:
            TaskCols[each.id] = myReport.addColHeader(each.id, each.name, each.short_name)


        jobList = session.query(Jobs).join(Hours)
        jobList = jobList.filter(Hours.date == monthDate).all()

        JobDict = dict()
        for index, each in enumerate(jobList):
            #verify len of job name: excel max lenght is 31
            if len(each.name) > 31:
                jobList[index].name = jobList[index].name[:28] + "..."
                each.name = jobList[index].name
            jobid = myReport.addRowName(each.name, each.id)
            myReport.RowNameLinks[jobid] = """internal:'{}'!A1""".format(each.name)
            JobDict[each.id] = jobid

        hourList = session.query(Hours).filter(Hours.date == monthDate)
        if hourList.count() >= 1:
            hourList = hourList.all()

            monthly_total_seconds = list()
            for eachHour in hourList:
                print "doing job {}".format(eachHour.job_id)
                #skip adding if job was done after hours
                if eachHour.time_finish <= eachHour.TimeSheet.time_out:
                    # print monthly_total_seconds," plus ",eachHour.number_seconds
                    print "job added whole time"
                    add_time =  eachHour.time_finish - eachHour.time_start
                else:
                    job_start = eachHour.time_start
                    clockout_time = eachHour.TimeSheet.time_out

                    #if this job is completely after hours dont add anytime.
                    if job_start >= clockout_time:
                        print "job added no time"
                        add_time = None
                    else:
                        print "job added partial time"
                        add_time = clockout_time - job_start
                if add_time:
                    monthly_total_seconds.append(add_time.seconds)


                myReport.addColData(eachHour.job_id, eachHour.task_id, eachHour.number_seconds)



            timesheetList = session.query(TimeSheet).filter(TimeSheet.date == monthDate).all()

            monthly_Clk_time = list()
            for eachSheet in timesheetList:
                td = eachSheet.time_out - eachSheet.time_in
                if eachSheet.lunch:
                    lunch_td = timedelta(minutes = eachSheet.lunch)
                    # print 'lunch td {}'.format(lunch_td)
                    td = td - lunch_td
                    # print 'subtracting lunch'
                # print eachSheet.id, Decimal(td.total_seconds())/3600
                monthly_Clk_time.append(td.total_seconds())

            monthly_Clk_time_sum = sum(monthly_Clk_time)
            monthly_total_seconds_sum = sum(monthly_total_seconds)
            shop_hours = Decimal(monthly_Clk_time_sum - monthly_total_seconds_sum)/3600

            #lets add extra headers and such
            myReport.addCellMergeData(0, myReport.getNumCols() - 1, 0, myReport.getNumCols()+1, 'Shop Hours:',{'bold' : True, 'align': 'right'})
            myReport.addCellData(0, myReport.getNumCols()+2, shop_hours)


            myReport.addCellData(len(JobDict) + 3,0,'Totals: ',{'bold':True,'align':'right','border':1})
            newFormat = myReport.Formats['SeperatorCol_Format'].copy()
            newFormat['border'] = 1
            myReport.addCellData(len(JobDict) + 3, 1, '', newFormat)
            myReport.addCellData(2,myReport.getNumCols()+2,'Totals',{'bold':True,'align':'center','border':1})
            total_string ='=SUM(' + str(xl_rowcol_to_cell(3,myReport.getNumCols()+2)) + ':' + str(xl_rowcol_to_cell(len(JobDict) + 2,myReport.getNumCols()+2)) + ')'
            myReport.addCellData(len(JobDict) + 3, myReport.getNumCols()+2, total_string, {'bold':True,'align':'center','border':1})
            myReport.createReport(False)


            #add extra pages for Details
            COL_DATE = 1
            COL_TASK = 2
            COL_TIMEIN = 3
            COL_TIMEOUT = 4
            COL_HOURS = 5


            for eachJob in jobList:
                # print 'adding page for:', eachJob.name
                # myReport.addWorkSheet(each.TimeSheet.CrewName.name)
                myReport.addWorkSheet(eachJob.name)
                myReport.TotalCols = False
                myReport.TotalRows = False
                myReport.SeperatorCol = ''
                myReport.setReportName(eachJob.name,format_bold)
                myReport.setSubject(monthDate.strftime('%B, %d %Y'), link="""internal:'Sheet1'!A1""")
                myReport.setRowHeader('Employee', format_bold)

                myReport.SetColumnWidth(COL_DATE, 9)
                myReport.SetColumnWidth(COL_TASK, 14)
                myReport.addColHeader(COL_DATE, 'Date','Date')
                myReport.addColHeader(COL_TASK, 'Task', 'Task')
                myReport.addColHeader(COL_TIMEIN, 'Time In', 'Time In')
                myReport.addColHeader(COL_TIMEOUT, 'Time Out', 'Time Out')
                myReport.addColHeader(COL_HOURS, 'Hours','Hours')

                hourList = session.query(Hours).join(TimeSheet).filter(Hours.job_id == eachJob.id)
                hourList = hourList.filter(Hours.date == monthDate).all()

                #set up row names

                #put in data
                row = 0
                for eachHour in hourList:
                    # print "add row",employee.TimeSheet.CrewName.name,eachHour.Task.name
                    rowid = myReport.addRowName(eachHour.TimeSheet.CrewName.name, row)
                    myReport.addColData(rowid, COL_DATE, eachHour.date.strftime('%x'))
                    myReport.addColData(rowid, COL_TASK, eachHour.Task.name)
                    myReport.addColData(rowid, COL_TIMEIN, str(eachHour.time_start.strftime('%I:%M%p')))
                    myReport.addColData(rowid, COL_TIMEOUT, str(eachHour.time_finish.strftime('%I:%M%p')))
                    myReport.addColData(rowid, COL_HOURS, eachHour.number_seconds)
                    row += 1

                myReport.createReport(False)

            myReport.saveReport()

            # self.Destroy()

        else:
            MessageClass.Message('No Hour entries for this date.', Duration=3000)

    def ViewDetails(self, RowData):
        mywin = ReportJobWindow(self.parent, RowData)

    def cancel(self, e):
        self.Destroy()

import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
import decimal
from decimal import Decimal
from message_class import *

class Column(object):
    max_before_short = 7
    column_ids = list()
    used_cols = 0
    def __init__(self, col_id, name, short_name):
        if Column.valid_id(col_id):
            self.col_id = col_id
        self.name = name
        self.short_name = short_name
        self.Data = dict()
        self.Used = False
        self.column = None
        Column.used_cols = 0

    def addData(self, row_id, data):

        if isinstance(data, int):
            #get any previously added data or 0 if none
            previousData = self.Data.get(row_id, 0)
            self.Data[row_id] = previousData + data
        else:
            self.Data[row_id] = data

        #if we havent used this col before set it to used and add to the
        #used column value
        if not self.Used:
            self.Used = True
            Column.used_cols += 1
            # print 'used a column for first time', Column.used_cols


    def getData(self, row_id):
        if row_id in self.Data:
            if isinstance(self.Data[row_id], int):
                return Decimal(self.Data[row_id])/3600
            else:
                return self.Data[row_id]
        else:
            return ''

    def getColHeader(self, column):
        if Column.used_cols >= Column.max_before_short:
            ret_value = self.short_name
        else:
            ret_value = self.name
        self.column = column
        return ret_value

    @staticmethod
    def getNumCols():
        return Column.used_cols

    @staticmethod
    def valid_id(col_id):
        if col_id in Column.column_ids:
            MessageClass.Message('Column Id in use!', Type=MESS_PANIC)
            ret_value = True
        else:
            ret_value = False
        return ret_value

class Report(object):

    def __init__(self, window):
        # self.window = window
        self.initDefaults()

        #formats
        self.Formats = {'ReportName_Format': None,'Subject_Format': None,
            'RowHeader_Format': None,
            'RowNames_Format': None,
            'SeperatorColHeader_Format' : None,
            'SeperatorCol_Format': None,
            'ColHeaders_Format': None,
            'ColData_Format': None,
            'EvenRowData_Format' : None,
            'TotalRows_Format': None,
            'EvenTotalRows_Format' : None,
            'TotalCols_Format':  None}

        self.FormatsNotSetup = True

        self.Format_Objs = dict()
        self.Book = None
        decimal.getcontext().prec = 3

    def initDefaults(self):
        self.filename = ''
        self.ReportName = ''
        self.Subject = ''
        self.SubjectLink = None
        self.RowHeader = ''
        self.RowNames = dict()
        self.RowNameLinks = dict()
        self.SeperatorCol = 'Task:'
        self.Columns = dict()
        self.__ColumnWidths = dict()
        self.TotalRows = True
        self.TotalCols =  True
        self.ExtraCellData = dict()
        self.EvenRowBgColor = '#DDD9C3'

    def InitReport(self, filename):
        self.filename = filename
        self.Book = xlsxwriter.Workbook(self.filename)
        self.CurrentWorkSheet = self.Book.add_worksheet()

    def SetColumnWidth(self,col_id, width):
        self.__ColumnWidths[col_id] = width

    def SetupFormats(self):
        if self.FormatsNotSetup:
            format_even_row = self.Formats['ColData_Format'].copy()
            format_even_row['bg_color'] = self.EvenRowBgColor
            # format_even_row = self.Book.add_format(format_even_row)
            self.add_Format('EvenRowData_Format', format_even_row)
            self.FormatsNotSetup = False

        format_eventotal_row = self.Formats['TotalRows_Format'].copy()
        format_eventotal_row['bg_color'] = self.EvenRowBgColor
        self.add_Format('EvenTotalRows_Format', format_eventotal_row)
        self.add_Format('Subject_Format', {'align':'center'})

    def addCellData(self, row, col, data, Format=None):
        cell_list = (data, Format)
        self.ExtraCellData[(row,col)] = cell_list

    def addCellMergeData(self, row, col, row2, col2, data, Format=None):
        # print data
        cell_list = (data, Format)
        self.ExtraCellData[(row,col,row2,col2)] = cell_list

    def setReportName(self, name, Format=None):
        self.ReportName = name
        if not Format == None:
            self.add_Format('ReportName_Format', Format)

    def setSubject(self, name, Format=None, link=None):
        self.Subject = name
        if link:
            self.SubjectLink = link

        if Format:
            self.add_Format('Subject_Format', Format)

    def setRowHeader(self, name, Format=None):
        self.RowHeader = name
        if not Format == None:
            self.add_Format('RowHeader_Format', Format)

    def add_Format(self, Format_Name, Format):
        """Add a xlsxwriter format dict"""
        self.Formats[Format_Name] = Format

    def get_Format(self, Format_Name):
        """Retrun Xlsxwriter format object, Creating it if nessicary from
            the stored format string added with add_Format.  Return None if
            no string added"""

        ret_Format = None
        if Format_Name in self.Formats:
                if not self.Formats[Format_Name] == None:
                    if not Format_Name in self.Format_Objs:
                        self.Format_Objs[Format_Name] = self.Book.add_format(self.Formats[Format_Name])
                    ret_Format = self.Format_Objs[Format_Name]
        return ret_Format

    def addRowName(self, name, row_id, link=None):
        """Add a RowName (a row header) and return a rowid
            for use with addColData"""
        self.RowNames[row_id] = name
        if link:
            self.RowNameLinks[row_id] = link
        return row_id

    def addColHeader(self,Col_id, name, short_name):
        """Add a Column Header and return a index for use
            with addColData"""
        newColumn = Column(Col_id, name, short_name)
        self.Columns[Col_id] = newColumn

    def autofixRowHeaders(self):
        maxLen = 14

        for each in self.RowNames:
            strlen = len(self.RowNames[each]) + sum(1 for c in self.RowNames[each] if c.isupper())
            if strlen > maxLen:
                maxLen = strlen
            # print each, strlen
        # print maxLen
        return maxLen

    def getNumCols(self):
        """Return the Current number of ColUsed, does not count
            Columns created but without data added"""
        anyObj = self.Columns.itervalues().next()
        # print anyObj.getNumCols()
        return anyObj.getNumCols()

    def addColData(self, row_id, col_id, data):
        """Add data to a Column using rowid and index returned from
            the addRowName and addColHeader also sets ColUsed True"""
        # print 'saving data: ' + str(data)
        self.Columns[col_id].addData(row_id, data)

    def createReport(self, singlePage=True):
        """Takes all entered values and writes a report to excel file using Xlsxwriter"""

        wksheet = self.CurrentWorkSheet
        self.SetupFormats()
        self.writeHeader()

        row = 3
        self.RowIndex = dict()
        for key, each in self.RowNames.iteritems():
            self.RowIndex[key] = row
            if row%2 == 0:
                thisformat =  self.get_Format('EvenRowData_Format')
            elif self.get_Format('RowNames_Format') == None:
                    thisformat = None
                    # wksheet.write(row, 0, each)
            else:
                thisformat = self.get_Format('RowNames_Format')
            if key in self.RowNameLinks:
                print 'saving url for', each, self.RowNameLinks[key]
                wksheet.write_url(row, 0, self.RowNameLinks[key], thisformat, each)
            else:
                wksheet.write(row, 0, each, thisformat)
            row += 1

        if self.SeperatorCol == '':
            start_col = 1
        else:
            start_col = 2

        col = start_col
        for each in self.Columns:
            if self.Columns[each].Used:
                value = self.Columns[each].getColHeader(col)
                if each in self.__ColumnWidths:
                    col_width = self.__ColumnWidths[each]
                else:
                    col_width = len(value) + (sum(1 for c in value if c.isupper())/2)
                wksheet.set_column(col,col, col_width)
                if self.get_Format('ColHeaders_Format') == None:
                    wksheet.write( 2, col, value)
                else:
                    wksheet.write( 2, col, value, self.get_Format('ColHeaders_Format'))
                col += 1

        for eachCol in self.Columns:
            if self.Columns[eachCol].Used:
                col = self.Columns[eachCol].column
                for eachRow in self.RowNames:
                    data = self.Columns[eachCol].getData(eachRow)
                    row = self.RowIndex[eachRow]
                    if row%2 == 0:
                        wksheet.write(row, col, data, self.get_Format('EvenRowData_Format'))
                    else:
                        if self.get_Format('ColData_Format') == None:
                            wksheet.write(row, col, data)
                        else:
                            wksheet.write(row, col, data, self.get_Format('ColData_Format'))
                    # print 'Row:',row,'Data:',data
                if self.TotalCols:
                    total_string = '=SUM(' + str(xl_rowcol_to_cell(3,col)) + ':' + str(xl_rowcol_to_cell(2+len(self.RowNames),col)) + ')'
                    if  self.get_Format('TotalCols_Format') == None:
                        wksheet.write(3+len(self.RowNames), col, total_string)
                    else:
                        wksheet.write(3+len(self.RowNames), col, total_string, self.get_Format('TotalCols_Format'))
                col += 1


        if self.TotalRows:
            for rowid in range(0, len(self.RowNames)):
                row = rowid + 3
                total_string = '=SUM(' + str(xl_rowcol_to_cell(row,start_col)) + ':' + str(xl_rowcol_to_cell(row,start_col+self.getNumCols() - 1)) + ')'
                if row%2 == 0:
                    if self.get_Format('EvenTotalRows_Format') != None:
                        wksheet.write(row, start_col+self.getNumCols(), total_string, self.get_Format('EvenTotalRows_Format'))
                else:
                    if self.get_Format('TotalRows_Format') == None:
                        wksheet.write(row, start_col+self.getNumCols(), total_string)
                    else:
                        wksheet.write(row, start_col+self.getNumCols(), total_string, self.get_Format('TotalRows_Format'))

        #add Extra Cells to sheet.
        for each in self.ExtraCellData:
            if len(each) == 2:
                row,col = each
                cell_list = self.ExtraCellData[each]

                if cell_list[1] == None:
                    wksheet.write(row, col, cell_list[0])
                else:
                    newFormat = self.Book.add_format(cell_list[1])
                    wksheet.write(row,col,cell_list[0], newFormat)
            elif len(each) == 4:
                row1,col1,row2,col2 = each
                cell_list = self.ExtraCellData[each]

                if cell_list[1] == None:
                    # print "writing merge_range"
                    wksheet.merge_range(row1, col1, row2, col2, cell_list[0])
                else:
                    newFormat = self.Book.add_format(cell_list[1])
                    wksheet.merge_range(row1, col1, row2, col2, cell_list[0], newFormat)

        if singlePage:
            self.saveReport()

    def writeHeader(self):
        if self.get_Format('ReportName_Format') == None:
            self.CurrentWorkSheet.write('A1', self.ReportName)
        else:
            self.CurrentWorkSheet.write('A1', self.ReportName, self.get_Format('ReportName_Format'))

        self.CurrentWorkSheet.set_column('A:A',self.autofixRowHeaders())

        if self.get_Format('Subject_Format'):
            Subject_Format = self.get_Format('Subject_Format')
        else:
            Subject_Format = None
        if self.SubjectLink:
            self.CurrentWorkSheet.write_url('A2', self.SubjectLink, Subject_Format, self.Subject)
        else:
            self.CurrentWorkSheet.write('A2', self.Subject, Subject_Format)


        if self.get_Format('RowHeader_Format') == None:
            self.CurrentWorkSheet.write('A3', self.RowHeader)
        else:
            self.CurrentWorkSheet.write('A3', self.RowHeader, self.get_Format('RowHeader_Format'))

        if self.SeperatorCol != '':
            self.CurrentWorkSheet.set_column('B:B', 4.5)
            if self.get_Format('SeperatorColHeader_Format') == None:
                self.CurrentWorkSheet.write('B3', self.SeperatorCol)
            else:
                self.CurrentWorkSheet.write('B3', self.SeperatorCol , self.get_Format('SeperatorColHeader_Format'))
            if self.get_Format('SeperatorCol_Format') != None:
                for row in range(0, len(self.RowNames) + 1):
                    self.CurrentWorkSheet.write(row+3, 1, '', self.get_Format('SeperatorCol_Format'))

    def addWorkSheet(self, sheetname=None):
        self.initDefaults()

        if sheetname:
            wksheet = self.Book.add_worksheet(sheetname)
        else:
            wksheet = self.Book.add_worksheet()
        self.CurrentWorkSheet = wksheet
        return wksheet

    def saveReport(self):
        worked = True
        try:
            self.Book.close()
        except IOError:
            MessageClass.Message('Error: {} \n is not a valid path or filename.'.format(self.filename), Type=MESS_PANIC)
            # wx.MessageBox('Error: ' + self.filename + ' \nis not a valid path or filename.' , 'Error', wx.OK | wx.ICON_ERROR)
            worked = False
        except:
            worked = False

        if worked:
            MessageClass.Message('Report Saved!', Duration=3000)

        else:
            MessageClass.Message('Failed to save report.', Type=MESS_PANIC)

from datetime import datetime, time, date, timedelta
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Column, Integer, String, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import relationship, sessionmaker, backref
import os.path
# import logging
#
# logging.basicConfig()
# logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

Settings_Base = declarative_base()

class Settings(Settings_Base):
	__tablename__ = 'Settings'
	id = Column(Integer, primary_key=True)
	name = Column(String(255), nullable = False)
	value = Column(String(500))

settings_engine = create_engine("sqlite:///Settings.db")

Settings_Base.metadata.create_all(settings_engine)
Settings_Session = sessionmaker(bind=settings_engine)
settings = Settings_Session()


database_File = settings.query(Settings).filter_by(name='database_file').one()
database_File = database_File.value
DB_VERSION="2"
Base = declarative_base()

class CrewNames(Base):
	__tablename__ ="CrewNames"
	id = Column(Integer, primary_key=True)
	name = Column(String(255), nullable=False)
	active = Column(Boolean, default=True)
	TimeSheets = relationship('TimeSheet', backref=backref('CrewName', order_by=id))

class TaskList(Base):
	__tablename__ = "TaskList"
	id = Column(Integer, primary_key=True)
	name = Column(String(255), nullable=False)
	active = Column(Boolean, default=True)
	short_name = Column(String(5))

class TimeSheet(Base):
	__tablename__ = "TimeSheet"
	id = Column(Integer, primary_key=True)
	date = Column(DateTime, nullable=False)
	time_in = Column(DateTime)
	time_out = Column(DateTime)
	lunch = Column(Integer)
	CrewNames_id = Column(Integer, ForeignKey('CrewNames.id'))
	Hours = relationship('Hours')

class Hours(Base):
	__tablename__ = "Hours"
	id = Column(Integer, primary_key=True)
	number_seconds = Column(Integer)
	date = Column(DateTime)
	time_start = Column(DateTime)
	time_finish = Column(DateTime)
	job_id = Column(Integer, ForeignKey('Jobs.id'))
	task_id = Column(Integer, ForeignKey('TaskList.id'))
	timesheet_id = Column(Integer, ForeignKey('TimeSheet.id'))
	TimeSheet = relationship('TimeSheet')
	Job = relationship('Jobs', backref=backref('Hours', order_by=id))
	Task = relationship('TaskList')


class Jobs(Base):
	__tablename__ = "Jobs"
	id = Column(Integer, primary_key=True)
	name = Column(String(255), nullable=False)
	create_date = Column(DateTime, nullable=False)
	active = Column(Boolean, default=True)
	client_id = Column(Integer, ForeignKey('Client.id'))
	Client = relationship('Client')

class Version(Base):
	__tablename__ = 'Settings'
	id = Column(Integer, primary_key=True)
	name = Column(String(255), nullable = False)
	value = Column(String(500))

class Client(Base):
	__tablename__ = 'Client'
	id = Column(Integer, primary_key=True)
	name = Column(String(255), nullable = False)
	active = Column(Boolean, default=True)



if os.path.isfile(database_File):
	engine = create_engine("sqlite:///" + database_File)
else:
	# print "Could not load database"
	engine = False



if engine:
	Base.metadata.create_all(engine)
	Session = sessionmaker(bind=engine)
	session = Session()

# #verify DB version
# versionCheck = session.query(Version).filter_by(name='DB_VERSION').one()
#
# if versionCheck.value == DB_VERSION:
# 	DB_VERSION = (DB_VERSION, True)
# else:
# 	DB_VERSION = (DB_VERSION, False)





class Class_GlobalVars(object):
	def init(self):
		self.__JobSelection = None
		self.__dynamic_choices = None
		self.__reportPath = None


	@property
	def ReportDir(self):
	  return self.__reportPath
	@ReportDir.setter
	def ReportDir(self, value):
	  self.__reportPath = value

	@property
	def dynamic_choices(self):
	  return self.__dynamic_choices
	@dynamic_choices.setter
	def dynamic_choices(self, value):
	  self.__dynamic_choices = value

	@property
	def JobSelection(self):
		return self.__JobSelection

	@JobSelection.setter
	def JobSelection(self, value):
		# print "changing Global JobSelection to", value
		self.__JobSelection = value



GlobalVars = Class_GlobalVars()
# GlobalVars.JobSelection = None

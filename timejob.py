import wx

from database import *
from SubDatabaseWindow import *
from reports import *
from edit_timesheet import *
from datetime import datetime, timedelta
import wx.lib.agw.ultimatelistctrl as ULC
import sys
import utility_functions


from main_window import *

# wx signals
FILE_EDIT_TIMESHEET = wx.NewId()
FILE_OPTIONS = wx.NewId()
MENU_QUIT = wx.NewId()
JSUB_EDIT = wx.NewId()
JSUB_MAIN = wx.NewId()
JSUB_ARCHIVE = wx.NewId()
DATAMENU_CLIENT = wx.NewId()
DATAMENU_CHEIF = wx.NewId()
DATAMENU_TASK = wx.NewId()
DATAMENU_CLIENT = wx.NewId()
REPORTMENU_DAILY = wx.NewId()
REPORTMENU_TASK = wx.NewId()
REPORTMENU_MONTHLY = wx.NewId()
REPORTMENU_BYJOB = wx.NewId()


# row constants
ROW_START = 1
ROW_END = 3
ROW_JOB = 5
ROW_TASK = 7


class ArchiveJobWindow(wx.Dialog):
    def __init__(self, parent):
        super(ArchiveJobWindow, self).__init__(parent, title='Archive Old Jobs', style=wx.CAPTION, size=(675,350))

        self.parent = parent
        self.panel = wx.Panel(self, wx.ID_ANY)
        # panelSizer = wx.BoxSizer(wx.VERTICAL)
        topSizer = wx.BoxSizer(wx.VERTICAL)
        buttonSizer = wx.BoxSizer(wx.HORIZONTAL)

        dt_today = datetime.now()
        dt_30DaysAgo = dt_today - timedelta(days=30)

        ActiveJobsResults = session.query(Jobs).filter(Jobs.active == True).all()

        ActiveJobs = map(lambda job: [job.id, job.name, job.create_date.strftime('%B %d, %Y')], ActiveJobsResults)


        mylist = ULC.UltimateListCtrl(self.panel, agwStyle=ULC.ULC_REPORT|ULC.ULC_VRULES|ULC.ULC_HRULES|ULC.ULC_SINGLE_SEL, size=(550, 250))

        mylist.InsertColumn(0, 'Job')
        mylist.InsertColumn(1, 'Create Date')
        mylist.InsertColumn(2, 'Last Used')
        mylist.InsertColumn(3, 'Archive?')

        for index, eachJob in enumerate(ActiveJobs):
            result = session.query(Hours.date).filter(Hours.job_id == eachJob[0]).order_by(Hours.date.desc()).first()
            firetask = [7,8,9,10,16]
            fireCheck = session.query(Hours).join(Jobs).filter(Jobs.id == eachJob[0]).filter(Hours.task_id.in_(firetask)).count()
            mylist.InsertStringItem(index, eachJob[1])
            mylist.SetStringItem(index, 1, eachJob[2])
            if fireCheck > 0:
                mylist.SetItemBackgroundColour(index, wx.Colour(255,51,51))
            # else:
            #     mylist.SetItemBackgroundColour(index, wx.Colour(51,51,255))
            # print result[0]
            mylist.SetStringItem(index, 3, '', it_kind=1)
            if result:
                if result[0]:
                    mylist.SetStringItem(index, 2, result[0].strftime('%B %d, %Y'))
                    if(result[0] <= dt_30DaysAgo):
                        temp = mylist.GetItem(index, 3)
                        temp.Check(True)
                        mylist.SetItem(temp)
                else:
                    mylist.SetStringItem(index, 2, 'Not Used')
            else:
                mylist.SetStringItem(index, 2, 'Not Used')

            #set Job Id as item data
            mylist.SetItemData(index, eachJob[0])


        mylist.SetColumnWidth(0, 250)
        mylist.SetColumnWidth(1, 115)
        mylist.SetColumnWidth(2, 115)
        mylist.SetColumnWidth(3, 70)
        self.mylist = mylist




        ButSelectOver = wx.Button(self.panel, label='Select Over')
        self.OverDaysText = wx.TextCtrl(self.panel, size=(35,-1), value='30')
        self.OverDaysText.SetMaxLength(3)
        OverDaysStatic = wx.StaticText(self.panel, label='Days')
        ButSelectNone = wx.Button(self.panel, label='Select None')
        ButSelectUnused = wx.Button(self.panel, id=wx.ID_ANY, label="Select 'Not Used'")
        cancelbutton = wx.Button(self.panel, id=wx.ID_CANCEL)
        Okbutton = wx.Button(self.panel, id=wx.ID_OK)

        ButSelectUnused.Bind(wx.EVT_BUTTON, self.SelectUnused)
        ButSelectNone.Bind(wx.EVT_BUTTON, self.SelectNone)
        ButSelectOver.Bind(wx.EVT_BUTTON, self.SelectOver)
        Okbutton.Bind(wx.EVT_BUTTON, self.Archive)

        infoText = wx.StaticText(self.panel, label='Red Highlights are jobs with fire task.')


        # panelSizer.Add(self.panel, 1, wx.EXPAND)
        buttonSizer.AddStretchSpacer(6)
        buttonSizer.Add(ButSelectOver, 1, wx.ALIGN_RIGHT)
        buttonSizer.Add((5,0))
        buttonSizer.Add(self.OverDaysText, 1, wx.ALIGN_CENTER)
        buttonSizer.Add((5,0))
        buttonSizer.Add(OverDaysStatic, 1, wx.ALIGN_RIGHT)
        buttonSizer.Add((20,0))
        buttonSizer.Add(ButSelectNone, 1, wx.ALIGN_RIGHT)
        buttonSizer.Add((20,0))
        buttonSizer.Add(ButSelectUnused, 1, wx.ALIGN_RIGHT)
        buttonSizer.Add((5,0))
        buttonSizer.Add(cancelbutton, 1, wx.ALIGN_RIGHT, 0)
        buttonSizer.Add((20,0))
        buttonSizer.Add(Okbutton, 1, wx.ALIGN_RIGHT, 0)
        buttonSizer.AddStretchSpacer(1)

        topSizer.AddStretchSpacer(2)
        topSizer.Add(mylist, 1, wx.ALIGN_CENTER)
        topSizer.AddStretchSpacer(2)
        topSizer.Add(buttonSizer, 1, wx.ALIGN_RIGHT, 0)
        topSizer.Add((0,20))
        topSizer.Add(infoText, 1, wx.ALIGN_LEFT | wx.LEFT, 15)
        # panelSizer.Add(topSizer, 1, wx.EXPAND)
        self.panel.SetSizer(topSizer)


    def SelectOver(self, e):
        dt_DaysAgo = datetime.now() - timedelta(days=int(self.OverDaysText.GetValue()))
        for itemNumber in xrange(self.mylist.GetItemCount()):
            itemDate = self.mylist.GetItem(itemNumber, 2)
            try:
                if datetime.strptime(itemDate.GetText(), '%B %d, %Y') < dt_DaysAgo:
                    itemCheck = self.mylist.GetItem(itemNumber, 3)
                    itemCheck.Check(True)
                    self.mylist.SetItem(itemCheck)
            except ValueError:
                pass

    def SelectNone(self, e):
        for itemNumber in xrange(self.mylist.GetItemCount()):
            itemCheck = self.mylist.GetItem(itemNumber, 3)
            itemCheck.Check(False)
            self.mylist.SetItem(itemCheck)

    def SelectUnused(self, e):
        # print 'select unused'
        for itemNumber in xrange(self.mylist.GetItemCount()):
            itemDate = self.mylist.GetItem(itemNumber, 2)
            itemCheck = self.mylist.GetItem(itemNumber, 3)
            # print itemDate.GetText()
            if itemDate.GetText() == 'Not Used':
                itemCheck.Check(True)
                self.mylist.SetItem(itemCheck)

    def Archive(self, e):
        number_jobs = 0

        for itemNumber in xrange(self.mylist.GetItemCount()):
            itemCheck = self.mylist.GetItem(itemNumber, 3)
            if itemCheck.GetKind() == 1 and itemCheck.IsChecked():
                Job_Id = self.mylist.GetItem(itemNumber).GetData()
                print Job_Id
                number_jobs += 1
                thisJob = session.query(Jobs).filter(Jobs.id == Job_Id).one()
                thisJob.active = False

        session.commit()
        MessageClass.Message("Archived {} Jobs.".format(number_jobs), Duration=3000)
        e.Skip()


class OptionsDlg(wx.Dialog):
    def __init__(self, parent):
        super(OptionsDlg, self).__init__(parent, title='Options', style=wx.CAPTION, size=(450,150))

        DBquery = settings.query(Settings).all()
        self.DBoptions = dict()
        for each in DBquery:
            self.DBoptions[each.name] = each.value

        self.parent = parent
        self.panel = wx.Panel(self, wx.ID_ANY)
        topSizer = wx.BoxSizer(wx.VERTICAL)
        buttonSizer = wx.BoxSizer(wx.HORIZONTAL)
        reportsDirSizer = wx.BoxSizer(wx.HORIZONTAL)

        self.CheckActive = wx.CheckBox(self.panel, label='Filter Inactive Jobs?', style=wx.ALIGN_RIGHT)
        self.CheckActive.SetValue(self.parent.FilterInactive)
        self.OldFilterActive = self.parent.FilterInactive


        reportsDirtxt = wx.StaticText(self.panel, label="Report Save Dir: ")
        self.reportsDir = wx.TextCtrl(self.panel, value=self.DBoptions['Report_Path'], size=(200, -1))
        reportChangebut = wx.Button(self.panel, label='Change Dir',  id=wx.ID_ANY)


        Okbutton = wx.Button(self.panel, id=wx.ID_OK, label='Ok')
        savebutton = wx.Button(self.panel, id=wx.ID_ANY,label='Save Settings')

        reportsDirSizer.AddStretchSpacer(2)
        reportsDirSizer.Add(reportsDirtxt, 1, wx.ALIGN_CENTER_VERTICAL, 0)
        reportsDirSizer.Add(self.reportsDir, 1, wx.ALIGN_CENTER_VERTICAL, 0)
        reportsDirSizer.AddStretchSpacer(1)
        reportsDirSizer.Add(reportChangebut, 1, wx.ALIGN_CENTER_VERTICAL, 0)
        reportsDirSizer.AddStretchSpacer(2)

        buttonSizer.AddStretchSpacer(2)
        buttonSizer.Add(savebutton, 1, wx.ALIGN_CENTER_VERTICAL, 0)
        buttonSizer.AddStretchSpacer(1)
        buttonSizer.Add(Okbutton, 1, wx.ALIGN_CENTER_VERTICAL, 0)
        buttonSizer.AddStretchSpacer(2)

        topSizer.AddStretchSpacer(1)
        topSizer.Add(self.CheckActive, 1, wx.ALIGN_CENTER,0)
        topSizer.AddStretchSpacer(1)
        topSizer.Add(reportsDirSizer, 1, wx.ALIGN_CENTER, 0)
        topSizer.Add((10,0))
        topSizer.Add(buttonSizer, 1, wx.ALIGN_CENTER,0)
        topSizer.AddStretchSpacer(1)

        self.panel.SetSizer(topSizer)
        topSizer.Fit(self.panel)
        self.panel.Fit()

        Okbutton.Bind(wx.EVT_BUTTON, self.setOptions)
        savebutton.Bind(wx.EVT_BUTTON, self.SaveSettings)
        reportChangebut.Bind(wx.EVT_BUTTON, self.ChangeReportDir)

    def ChangeReportDir(self, e):
        utility_functions.ReportDirectory(self)
        thisResult = settings.query(Settings).filter_by(name="Report_Path").one()
        GlobalVars.ReportDir = thisResult.value
        self.reportsDir.ChangeValue(GlobalVars.ReportDir)

    def setOptions(self, e):
        if self.CheckActive.GetValue() != self.OldFilterActive:
            self.parent.FilterInactive = self.CheckActive.GetValue()
            wx.CallAfter(self.parent.timesheet.UpdateJobChoices)
        e.Skip()

    def SaveSettings(self, e):
        proceed = False
        result = settings.query(Settings).filter_by(name='Filter_InactiveJobs_TimeSheetWindow')
        if result.count() == 1:
            newSetting = result.first()
            # print 'one object!'
            proceed = True
        elif result.count() == 0:
            newSetting = Settings(name='Filter_InactiveJobs_TimeSheetWindow')
            settings.Add(newSetting)
            proceed = True
        else:
            wx.MessageBox('Settings Database is Corrupted.\nContact Doss ASAP!!','Database Error', wx.OK | wx.ICON_ERROR)

        if proceed:
            if self.CheckActive.GetValue():
                newSetting.value = 'True'
                # print 'True'
            else:
                newSetting.value = 'False'
                # print 'False'
            # print 'saved to Database'
            settings.flush()
            settings.commit()


class TimeSheetWindow(MainWindow):

    def __init__(self, parent, title, style):
        super(TimeSheetWindow, self).__init__(
            parent, style=style, title=title)

        # try:
        result = settings.query(Settings.value).filter_by(name='Filter_InactiveJobs_TimeSheetWindow').scalar()
        resultDir = settings.query(Settings).filter_by(name="Report_Path").one()
        GlobalVars.ReportDir = resultDir.value
        # except:
        #     MessageClass.Message("Error loading options.\nCheck Network Connection.", Type=MESS_PANIC)

        if result == 'False':
            self.FilterInactive = False
        else:
            self.FilterInactive = True
            # print 'TimeSheets > seting filter inactive'
        self.InitUI()
        self.Centre()
        self.Show(True)


    def InitUI(self):


        menubar = wx.MenuBar()
        fileMenu = wx.Menu()
        MessageClass.Setup(self, self.CreateStatusBar())
        # file menu items
        fmi_options = wx.MenuItem(fileMenu, FILE_OPTIONS, 'Options')
        fmi_edit_timesheet = wx.MenuItem(
            fileMenu, FILE_EDIT_TIMESHEET, 'Edit TimeSheet')
        qmi = wx.MenuItem(fileMenu, MENU_QUIT, '&Quit\tCtrl+Q')
        qmi.SetBitmap(wx.Bitmap('exit2.png'))

        # add file menu items
        fileMenu.AppendItem(fmi_options)
        fileMenu.AppendItem(fmi_edit_timesheet)
        fileMenu.AppendItem(qmi)

        # bind file menu events
        self.Bind(wx.EVT_MENU, self.editTimeSheet, id=FILE_EDIT_TIMESHEET)
        self.Bind(wx.EVT_MENU, self.OnQuit, id=MENU_QUIT)
        self.Bind(wx.EVT_MENU, self.Options, id=FILE_OPTIONS)

        # add report menu items
        reportMenu = wx.Menu()
        rmi_daily = wx.MenuItem(reportMenu, REPORTMENU_DAILY, 'Daily Report')
        rmi_task = wx.MenuItem(reportMenu, REPORTMENU_TASK, 'Task Report')
        rmi_monthly = wx.MenuItem(
            reportMenu, REPORTMENU_MONTHLY, 'Monthly Report')
        rmi_byjob = wx.MenuItem(reportMenu, REPORTMENU_BYJOB, 'Job Report')
        rmi_directory = wx.MenuItem(
            reportMenu, REPORTMENU_DIR, 'Set Save Directory')

        reportMenu.AppendItem(rmi_byjob)
        reportMenu.AppendItem(rmi_daily)
        reportMenu.AppendItem(rmi_task)
        reportMenu.AppendItem(rmi_monthly)

        # bind report menu events
        self.Bind(wx.EVT_MENU, self.ReportTask, id=REPORTMENU_TASK)
        self.Bind(wx.EVT_MENU, self.ReportDaily, id=REPORTMENU_DAILY)
        self.Bind(wx.EVT_MENU, self.ReportByJob, id=REPORTMENU_BYJOB)
        self.Bind(wx.EVT_MENU, self.ReportMonthly, id=REPORTMENU_MONTHLY)


        # Database menu items
        dataMenu = wx.Menu()
        client_mi = wx.MenuItem(dataMenu, DATAMENU_CLIENT, 'Edit Clients')
        cmi = wx.MenuItem(
            dataMenu, DATAMENU_CHEIF, 'Edit Employees')
        cmi.SetBitmap(wx.Bitmap('heroes.png'))
        # j_emi = wx.MenuItem(dataMenu, JOBMENU_EDIT, 'Edit Jobs')
        j_emi = wx.Menu()
        j_emi.Append(JSUB_EDIT, 'Edit Jobs')
        j_emi.Append(JSUB_ARCHIVE, 'Auto Archive (unused for 30 days)')
        t_dmi = wx.MenuItem(dataMenu, DATAMENU_TASK, 'Edit Task')

        # add data menu items
        dataMenu.AppendItem(client_mi)
        dataMenu.AppendItem(t_dmi)
        dataMenu.AppendMenu(JSUB_MAIN, 'Jobs ...', j_emi)
        dataMenu.AppendItem(cmi)

        # bind data menu events
        self.Bind(wx.EVT_MENU, self.EditTask, id=DATAMENU_TASK)
        self.Bind(wx.EVT_MENU, self.EditClient, id=DATAMENU_CLIENT)
        self.Bind(wx.EVT_MENU, self.EditEmployee, id=DATAMENU_CHEIF)
        self.Bind(wx.EVT_MENU, self.EditJob, id=JSUB_EDIT)
        self.Bind(wx.EVT_MENU, self.ArchiveJobs, id=JSUB_ARCHIVE)

        menubar.Append(fileMenu, '&File')
        menubar.Append(reportMenu, '&Reports')
        menubar.Append(dataMenu, '&Database')
        self.SetMenuBar(menubar)


        self.GridLabelValue = 'TimeSheet'
        self.newEntryPanel()

    def EditTimeSheet(self, timesheetId):
        # print 'timesheet id:',timesheetId
        # wx.CallAfter(self.ClosePanel, self.Edit)
        MessageClass.Message('Loading TimeSheet....',Duration=-1)
        self.ClosePanel(self.Edit)
        mywin = EditTimeSheet(self,style=wx.MINIMIZE_BOX | wx.RESIZE_BORDER
                                            | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX, title='TimeSheet Editor',timesheet_id=timesheetId, UpdateFunction=None)
        MessageClass.ClearStatusBar()

    def OnQuit(self, e):
        self.Close()

    def Options(self, e):
        mywin = OptionsDlg(self)
        mywin.CenterOnParent()
        mywin.ShowModal()


    def ArchiveJobs(self, e):
        jobwin = ArchiveJobWindow(self)
        jobwin.CenterOnParent()
        result = jobwin.ShowModal()
        if result == wx.ID_OK:
            wx.CallAfter(self.timesheet.UpdateJobChoices)


    def EditJob(self, e):
        jobwin = JobWindow(self)
        jobwin.CenterOnParent()
        jobwin.ShowModal()
        wx.CallAfter(self.timesheet.UpdateJobChoices)
        # self.refreshGrid()

    def editTimeSheet(self, e):
        mywin = EditTimeWindow(self)
        # mywin.CenterOnParent()
        mywin.ShowModal()

    def EditClient(self, e):
        mywin = ClientWindow(self)
        mywin.CenterOnParent()
        mywin.ShowModal()

    def EditTask(self, e):
        mywin = TaskWindow(self)
        mywin.CenterOnParent()
        mywin.ShowModal()

    def EditEmployee(self, e):
        cheifwin = CheifWindow(self)
        cheifwin.CenterOnParent()
        cheifwin.ShowModal()


    def ReportMonthly(self, e):
        """Make a report for a selected month"""
        reportWin = ReportMontlyWindow(self)
        reportWin.CenterOnParent()
        reportWin.ShowModal()

    def ReportByJob(self, e):
        """Make a report for selected job"""
        reportWin = ReportJobWindow(self)
        reportWin.CenterOnParent()
        reportWin.ShowModal()

    def ReportTask(self, e):
        reportWin = ReportTaskWindow(self)
        reportWin.CenterOnParent()
        reportWin.ShowModal()

    def ReportDaily(self, e):
        """Make Daily Report"""
        reportWin = ReportDailyWindow(self)
        reportWin.CenterOnParent()
        reportWin.ShowModal()


app = wx.App()
locale = wx.Locale(wx.LANGUAGE_ENGLISH_US)


#database Connection check
if engine:
    #database version check
    if True:
        msg = "Loading Program..."
        busyDlg = wx.BusyInfo(msg)
        window = TimeSheetWindow(None, style=wx.MINIMIZE_BOX | wx.NO_BORDER
                            | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX, title='Servpro Hours Tracker')
        busyDlg = None

        app.MainLoop()

        session.close()
    else:
        mymessage = wx.MessageDialog(None, "Wrong Database Version!\nFailed to Load Database!", "Now is a good time to Panic!", style=wx.OK|wx.ICON_ERROR)
        mymessage.ShowModal()
        mymessage.Destroy()
else:
    mymessage = wx.MessageDialog(None, "Failed to Load Database!\nCheck Network Connection!", "Now is a good time to Panic!", style=wx.OK|wx.ICON_ERROR)
    mymessage.ShowModal()
    mymessage.Destroy()

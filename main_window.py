import wx

from database import *
from message_class import *
from SubDatabaseWindow import *
from reports import *
from edit_timesheet import *
from time import clock
import re
from datetime import datetime, date, timedelta
from datetime import  time as dt_time
from timesheet_class import *

# wx signals
FILE_EDIT_TIMESHEET = wx.NewId()
MENU_QUIT = wx.NewId()
JOBMENU_EDIT = wx.NewId()
DATAMENU_CHEIF = wx.NewId()
DATAMENU_STATUS = wx.NewId()
DATAMENU_CLIENT = wx.NewId()
REPORTMENU_DAILY = wx.NewId()
REPORTMENU_MONTHLY = wx.NewId()
REPORTMENU_BYJOB = wx.NewId()
REPORTMENU_DIR = wx.NewId()



# row constants
ROW_START = 1
ROW_END = 3
ROW_JOB = 5
ROW_TASK = 7

class MainWindow(wx.Frame):

    def __init__(self, parent, title, style, pos=None):
        if pos == None:
            super(MainWindow, self).__init__(
                parent, style=style, title=title, size=(800, 650))
        else:
            super(MainWindow, self).__init__(
                parent, style=style, title=title, size=(800, 650),pos=pos)
        # self.control = wx.TextCtrl(self, style=wx.TE_MULTILINE)

        # class vars
        self.newjobDate = wx.DateTime.Now()
        self.Edit = False
        self.timesheet_id = None
        self.frameVbox = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.frameVbox)

        sit_id = wx.NewId()
        jit_id = wx.NewId()
        self.Bind(wx.EVT_MENU, self.Save, id=sit_id)
        self.Bind(wx.EVT_MENU, self.AddJob, id=jit_id)

        self.accel_tbl = wx.AcceleratorTable([(wx.ACCEL_CTRL, ord('S'), sit_id),
                                              (wx.ACCEL_CTRL, ord('J'), jit_id)
                                             ])
        self.SetAcceleratorTable(self.accel_tbl)

    def AddJob(self, e):
        CurrentWindow = wx.Window.FindFocus()
        addwin = AddJobWindow(self)
        addwin.CenterOnParent()
        # if addwin.ShowModal() == wx.ID_OK:
        addwin.ShowModal()
        if CurrentWindow.GetName() == "Job_Text":
            wx.CallAfter(self.JobBoxUpdate, CurrentWindow)
        else:
            wx.CallAfter(self.timesheet.UpdateJobChoices)

    def JobBoxUpdate(self, CurrentWindow):
        self.timesheet.UpdateJobChoices()
        CurrentWindow.SetValue(GlobalVars.JobSelection[1])
        GlobalVars.JobSelection = None

    def InitUI(self):
        pass

    def newEntryPanel(self):
        total_time1 = clock()
        self.panel = wx.Panel(self, style=wx.BORDER_NONE)
        # sheet_time1 = clock()
        self.timesheet = TimeSheet_Class(self, self.panel, self.timesheet_id)
        # sheet_time2 = clock()
        # MessageClass.Debug('Creating Timesheet Class in {} seconds'.format(sheet_time2-sheet_time1))
        self.frameVbox.Add(self.panel, 1, flag=wx.EXPAND)

        vbox = wx.BoxSizer(wx.VERTICAL)
        bottomRowSizer = wx.BoxSizer(wx.HORIZONTAL)
        gridLabel = wx.StaticText(self.panel, label=self.GridLabelValue)
        font = wx.Font(16, wx.MODERN, wx.NORMAL, wx.BOLD)
        gridLabel.SetFont(font)
        vbox.Add(gridLabel, 0, wx.ALIGN_CENTER, 1)
        vbox.AddSpacer(10)

         # add buttons

        if self.Edit:
            deleteButton = wx.Button(self.panel, label="Delete TimeSheet")
            deleteButton.Bind(wx.EVT_BUTTON, self.DeleteTimeSheet)
            cancelbutton = wx.Button(self.panel, id=wx.CANCEL, label="Cancel")
        else:
            cancelbutton = wx.Button(self.panel, id=wx.CANCEL, label="Clear Sheet")
        savebutton = wx.Button(self.panel, id=wx.OK, label="&Save")
        bottomRowSizer.Add(cancelbutton, flag=wx.RIGHT, border=10)
        bottomRowSizer.Add(savebutton, flag=wx.RIGHT, border=35)

        cancelbutton.Bind(wx.EVT_BUTTON, self.Cancel)
        savebutton.Bind(wx.EVT_BUTTON, self.Save)
        self.saveButton = savebutton

        vbox.Add(self.timesheet.Sizer, 0, wx.ALIGN_CENTER_HORIZONTAL,0)
        vbox.AddSpacer(25)


        # loop threw all entry rows and add their sizers to the main sizer
        for each in self.timesheet.Rows:
                vbox.AddSpacer(10)
                vbox.Add(self.timesheet.Rows[each].Sizer, 0, wx.ALIGN_CENTER, 1)

        vbox.AddStretchSpacer(1)
        vbox.Add(bottomRowSizer, 0, wx.ALIGN_RIGHT)
        vbox.Add(self.timesheet.widget_shophours, flag=wx.LEFT | wx.ALIGN_LEFT, border=15)
        vbox.AddStretchSpacer(1)
        self.panel.SetSizer(vbox)
        # self.choice_chief.SetFocus()
        total_time2 = clock()
        MessageClass.Debug('Loaded Timesheet in {} seconds'.format(total_time2-total_time1))

    def DeleteTimeSheet(self, e):
        #only for edits
        pass

    def keydownEVT(self, e):
        if e.GetKeyCode() == wx.WXK_RETURN or e.GetKeyCode() == wx.WXK_NUMPAD_ENTER:
            # print 'event called'
            obj = e.GetEventObject()
            objName = obj.GetName()
            rowId, objName = objName.split(':')
            newRowId = int(rowId)
            if objName == 'ROW_END':
                newRowId = int(rowId) + 1
                newObjName = 'ROW_START'
            if objName == 'ROW_START':
                newObjName = 'ROW_END'
            if newRowId > (len(self.Rows) - 1):
                newRowId = 0
            self.Rows[newRowId][eval(newObjName)].SetFocus()
        else:
            e.Skip()

    def Save(self, e):
        if self.Edit:
            ret = wx.MessageBox('Save Changes?', 'Comfirm Save', wx.YES_NO | wx.ICON_QUESTION)
            if ret == wx.NO:
                return
        MessageClass.Message('Saving TimeSheet.')
        ret_val = self.timesheet.save()
        if ret_val:
            try:
                session.commit()
                worked = True
            except:
                MessageClass.Message('Failed to Save TimeSheet', Type=MESS_PANIC)
                session.rollback()
                worked = False
            if worked:
                MessageClass.Message('Timesheet saved.')
                wx.CallLater(50, self.ClosePanel, self.Edit)
        else:
            session.rollback()
            MessageClass.Message('Failed to save TimeSheet!', Type=MESS_PANIC, Duration=2000)

    def EditTimeSheet(self, timesheetId):
        pass

    def fixTime(self, time, mydate):
        ret_time = datetime.strptime(time, '%H%M').time()
        # print datetime.combine(mydate, ret_time)
        return datetime.combine(mydate, ret_time)

    def Cancel(self, e):
        session.rollback()
        self.ClosePanel(self.Edit)

    def ClosePanel(self, CloseBool):
        if CloseBool:
            self.Destroy()
        else:
            self.Freeze()
            self.panel.Destroy()
            self.panel = None
            self.newEntryPanel()
            self.Layout()
            self.Thaw()

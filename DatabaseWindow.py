#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx

from database import *
from utility_functions import pydate2wxdate, GlobalColorPicker
from message_class import *

# define constants for ColInfo
COLUMN = 0
FIELDNAME = 1
TITLE = 2
TYPE = 3
COLOR = 4


# wx Ids
BUT_ADD = wx.NewId()


class DatabaseWindow2(wx.Dialog):

  def __init__(self, parent, title, DataObj, ColInfo):
    # self.DataObj = Jobs
    self.parent = parent
    # ColInfo = [
    #     #   col value              title         type            color if one
    #     (0, 'name', 'Name', 'Unicode'),
    #     (1, 'active','Active','CheckBox'),
    #     (2, 'delete.png', 'Delete', 'Delete')
    # ]

    super(DatabaseWindow2, self).__init__(parent, style=wx.CAPTION , title=title, size=(450,400))

    self.DataObj = DataObj
    self.ColInfo = ColInfo
    # print 'setup colinfo: ',self.ColInfo





  def Setup(self):
      self.panel = wx.Panel(self, wx.ID_ANY)

      topSize = wx.BoxSizer(wx.VERTICAL)
      topRowSizer = wx.BoxSizer(wx.HORIZONTAL)
      dateSizer = wx.BoxSizer(wx.HORIZONTAL)
      middleSizer = wx.BoxSizer(wx.HORIZONTAL)
      midButtonSizer = wx.BoxSizer(wx.VERTICAL)
      bottomRowSizer = wx.BoxSizer(wx.HORIZONTAL)
      buttonSizer = wx.BoxSizer(wx.HORIZONTAL)


      self.showInActive = False
      self.order_by = self.DataObj.name


      textTitle = wx.StaticText(self.panel, label="Search:")
      self.textbox = wx.TextCtrl(self.panel, size=(140, -1))
      self.activeBox = wx.CheckBox(self.panel, label='Show Inactive?')

      listTitle = wx.StaticText(self.panel, label='Jobs:')
      # self.listbox = wx.ListBox(self.panel, size=(300, 200))
      self.listbox = wx.ListCtrl(self.panel, size=(self.ListBoxWidth(), self.ListBoxHeight()), style=wx.LC_REPORT| wx.BORDER_SUNKEN)
      # self.listbox = ULC.UltimateListCtrl(self.panel,size=(self.ListBoxWidth, 200), agwStyle = wx.LC_REPORT | wx.LC_VRULES | wx.LC_HRULES)

      topRowSizer.AddStretchSpacer(1)
      topRowSizer.Add(textTitle, 1, wx.LEFT | wx.ALIGN_CENTER, 15)
      topRowSizer.Add(self.textbox, 1, wx.LEFT | wx.ALIGN_CENTER, 10)
      topRowSizer.AddStretchSpacer(3)
      topRowSizer.Add(self.activeBox, 1,  wx.ALIGN_CENTER,0)
      topRowSizer.AddStretchSpacer(1)

      topSize.AddSpacer(10)
      topSize.Add(topRowSizer, 1, 0,0)
      topSize.AddSpacer(10)


      AddButton = wx.Button(self.panel, wx.ID_ANY, 'Add')
      AddButton.SetFocus()

      DelButton = wx.Button(self.panel, wx.ID_ANY, 'Delete')
      EditButton = wx.Button(self.panel, wx.ID_ANY, 'Edit')
      InactiveButton = wx.Button(self.panel, wx.ID_ANY, 'Set Inactive')


      # midButtonSizer.AddStretchSpacer(1)
      midButtonSizer.AddSpacer(8)
      midButtonSizer.Add(AddButton, 1, 0, 0)
      midButtonSizer.AddStretchSpacer(1)
      midButtonSizer.Add(DelButton, 1, 0, 0)
      midButtonSizer.AddStretchSpacer(1)
      midButtonSizer.Add(EditButton, 1, 0, 0)
      midButtonSizer.AddStretchSpacer(1)
      midButtonSizer.Add(InactiveButton,1,0,0)

      middleSizer.AddStretchSpacer(1)
      middleSizer.Add(self.listbox, 1, 0, 0)
      middleSizer.AddSpacer(15)
      middleSizer.Add(midButtonSizer, 1, 0, 0)
      middleSizer.AddStretchSpacer(1)



      topSize.Add(listTitle, 1, wx.LEFT, 25)
      topSize.Add(middleSizer, 1, 0, 0)
      topSize.AddSpacer(5)

      exportbutton = wx.Button(self.panel, wx.ID_OK, "&Ok")
      buttonSizer.Add(exportbutton, 1, wx.ALL | wx.ALIGN_CENTER, 5)

      topSize.AddStretchSpacer(1)
      topSize.Add(buttonSizer, 1, wx.ALL | wx.ALIGN_CENTER, 1)
      topSize.AddStretchSpacer(1)

      self.panel.SetSizer(topSize)
      topSize.Fit(self.panel)
      self.panel.Fit()

      self.activeBox.Bind(wx.EVT_CHECKBOX, self.toggleActive)
      self.textbox.Bind(wx.EVT_TEXT, self.textEntered)
      self.listbox.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.EditJob)
      AddButton.Bind(wx.EVT_BUTTON, self.add)
      DelButton.Bind(wx.EVT_BUTTON, self.deleteItem)
      EditButton.Bind(wx.EVT_BUTTON, self.EditJob)
      InactiveButton.Bind(wx.EVT_BUTTON, self.setJobInactive)
      self.listbox.Bind(wx.EVT_LIST_COL_CLICK, self.headerClick)

      for each in self.ColInfo:
        self.listbox.InsertColumn(each['Column'], each['FieldName'])


      results = self.MakeQuery()
      results.all()
      index = 0
      for each in results:
          self.MakeRow(index, each)
          index += 1


      for each in self.ColInfo:
        self.listbox.SetColumnWidth(each['Column'], self.AutoSizeCol(each['Column']))

  def MakeRow(self, index, RowData):
      for eachCol in self.ColInfo:
        value = getattr(RowData, eachCol['Short Field'])
        if eachCol['Column'] == 0:
          self.listbox.InsertStringItem(index, unicode(value))
        elif eachCol['Type'] == 'Unicode':
          self.listbox.SetStringItem(index, eachCol['Column'], unicode(value))
        elif eachCol['Type'] == 'Date':
          self.listbox.SetStringItem(index, eachCol['Column'], unicode(value.strftime('%x')))
        elif eachCol['Type'] == 'Bool':
          if value:
            self.listbox.SetStringItem(index, eachCol['Column'], 'True')
          else:
            self.listbox.SetStringItem(index, eachCol['Column'], 'False')
        # self.listbox.InsertStringItem(index, each.name)
      self.listbox.SetItemData(index, RowData.id)

  def AutoSizeCol(self, Col):
    ret_val = -1
    if Col != 0:
      ret_val =  self.ColInfo[Col]['Width']
    else:
      Other_ColWidth = 0
      for each in self.ColInfo:
        if each['Column'] != 0:
          Other_ColWidth += each['Width']
        ret_val = self.ListBoxWidth() - Other_ColWidth - 25

    return ret_val

  def ListBoxHeight(self):
      return 200

  def ListBoxWidth(self):
    return 300



  def MakeQuery(self):
      results = session.query(self.DataObj).order_by(self.order_by)
      if self.showInActive == False:
          results = results.filter_by(active=True)
      return results

  def headerClick(self, e):

        old_orderby = self.order_by

        self.order_by = eval(self.ColInfo[e.GetColumn()]['Field'])

        if self.order_by == old_orderby:
          self.order_by = eval(self.ColInfo[e.GetColumn()]['Field']).desc()

        self.textEntered(1)

  def EditJob(self, e):

         if self.listbox.GetSelectedItemCount() == 1:
            currentItem = self.listbox.GetNextSelected(-1)
            self.EditFunction()
            wx.CallAfter(self.textEntered,1)

         else:
             MessageClass.Message('You can only Edit one item at a time.', Type=MESS_PANIC)
        #    wx.MessageBox('You can only Edit one item at a time.','Error', wx.OK | wx.ICON_ERROR)


  def EditFunction(self):
    pass


  def dateChanged(self, e):
      wx.CallAfter(self.textEntered, 1)

  def toggleActive(self, e):
      self.showInActive = self.activeBox.GetValue()
      wx.CallAfter(self.textEntered, 1)


  def setJobInactive(self, e):
    currentItem = -1
    while self.listbox.GetNextSelected(currentItem) != -1:
       currentItem = self.listbox.GetNextSelected(currentItem)
       Job_Id = self.listbox.GetItemData(currentItem)

       jobObj = session.query(self.DataObj).filter(self.DataObj.id == Job_Id).first()
       if jobObj.active:
          jobObj.active = False
          session.commit()
          wx.CallAfter(self.textEntered,1)
          message = 'Item {} set inactive.'.format(jobObj.name)
          MessageClass.Message(message)


  def textEntered(self, e):
      value = self.textbox.GetValue()

      # print timesheetList.count()

      timesheetList = self.MakeQuery().filter(self.DataObj.name.like('%' + value + '%'))


      # print timesheetList.count()
      timesheetList.all()
      self.listbox.ClearAll()

      for each in self.ColInfo:
        self.listbox.InsertColumn(each['Column'], each['FieldName'])

      index = 0
      for each in timesheetList:
          self.MakeRow(index, each)
          index += 1

      for each in self.ColInfo:
        self.listbox.SetColumnWidth(each['Column'], self.AutoSizeCol(each['Column']))


  def deleteItem(self, e):
    wx.MessageBox('Delete function not override.','Error', wx.OK | wx.ICON_ERROR)

  def add(self, e):
    wx.MessageBox('Add function not overriden.', 'Error', wx.OK|wx.ICON_ERROR)

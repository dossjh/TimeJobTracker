import wx

DEBUG_MODE = True

MESS_PANIC = 0
MESS_ERROR = 1
MESS_INFO = 2


class ClassMessageClass(object):
    def __init__(self):
        self.statusbar = None
        self.window = None
        self.Ready = False

    def Setup(self, window, statusbar):
        self.Ready = True
        self.statusbar = statusbar
        self.window = window

    def Message(self, NewMessage, Duration=1000, Type=MESS_INFO, Caption='Error'):
        if self.Ready:
            if Type == MESS_PANIC:
                if Duration == 1000:
                    Duration = 2500
            if self.statusbar:
                if self.statusbar.GetStatusText() != '':
                    # print self.statusbar.GetStatusText()
                    self.statusbar.PushStatusText(NewMessage)
                    if Duration != 0 and Duration != -1:
                        wx.CallLater(Duration, self.PopMessage)
                else:
                    self.statusbar.SetStatusText(NewMessage)
                    if Duration != 0 and Duration != -1:
                        wx.CallLater(Duration, self.ClearStatusBar)

            if Type == MESS_PANIC:
                mymessage = wx.MessageDialog(self.window, NewMessage, Caption, style=wx.OK|wx.ICON_ERROR)
                mymessage.ShowModal()
                mymessage.Destroy()
        else:
            print 'MessageClass Not ready! >',NewMessage

    def PopMessage(self):
        self.statusbar.PopStatusText()

    def ClearStatusBar(self):
        self.statusbar.SetStatusText('')

    def Debug(self, Message):
        if DEBUG_MODE:
            print Message


MessageClass = ClassMessageClass()

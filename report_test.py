from report_class import Report

class window(object):
    def SetStatusBarTime(self, value, time):
        print value

mywindow = window()
myReport = Report(mywindow)

myReport.filename = r'C:\Users\technician\Desktop\Monthly.xlsx'
myReport.ReportName = 'Monthly Hour Report'
myReport.Subject = 'June'
myReport.RowHeader = 'Job Names'

format_bold = {'bold' : True,'border' : 1}

myReport.add_Format('SeperatorColHeader_Format', {'bold' : True,'border' : 1, 'align': 'right'})
myReport.add_Format('RowHeader_Format', {'align' : 'center', 'bold': True, 'border' : 1})
myReport.add_Format('RowNames_Format', {'align': 'center', 'border' : 1})
myReport.add_Format('ColData_Format',{'align':'center', 'border': 1})
myReport.add_Format('TotalRows_Format', {'align' : 'center', 'border': 1})
myReport.add_Format('TotalCols_Format', {'align' : 'center', 'border': 1})
myReport.add_Format('SeperatorCol_Format', {'bg_color' : '#D8D8D8'})
myReport.add_Format('ColHeaders_Format', {'align' : 'center', 'bold': True, 'border' : 1})

row1 = myReport.addRowName('Mikey Mike')
row2 = myReport.addRowName('Vicky Skky')
row3 = myReport.addRowName('William Phisser')
myReport.SeperatorCol = 'Task'
col1 = myReport.addColHeader('Water Damage','WD')
col2 = myReport.addColHeader('WD Check','WD CK')
col3 = myReport.addColHeader('Mold','Mold')
col4 = myReport.addColHeader('OnLo','OnLo')

myReport.addColData(row1, col1, 2*3600)
myReport.addColData(row1, col1, 12*3600)
myReport.addColData(row2, col2, 1*3600)
myReport.addColData(row2, col3, 3*3600)
myReport.addColData(row3, col1, 8*3600)



myReport.saveReport()

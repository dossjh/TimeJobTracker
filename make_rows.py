from database import *

session.add_all(
[
CrewNames(name='Helper'),
CrewNames(name='Doss Gunter',active=True),
CrewNames(name='Jeff Masters', active=True),
CrewNames(name='Brandon Davis',active=True),
CrewNames(name='Severn Boyer',active=True),
CrewNames(name='Josh Gorman', active=True),
CrewNames(name='Calvin Cross',active=True),
CrewNames(name='Dana Cook'),
CrewNames(name='Cheryl Cross'),
CrewNames(name='Kerry Keeley')]
)

session.add_all(
[TaskList(name='Water Damage',active=True),
TaskList(name='WD Check', active=True),
TaskList(name='Mold',active=True),
TaskList(name='Carpet Clean',active=True),
TaskList(name='Duct Clean'),
TaskList(name='Bio'),
TaskList(name='OnLo',active=True),
TaskList(name='Packout',active=True),
TaskList(name='Move Back',active=True),
TaskList(name='Fire Other', active=True)]
)

session.add_all([
Settings(name='Report_Path',value='C:\\')

])


#temp stuff
# from datetime import *
# session.add_all([
# Jobs(name='Mikey Mike', create_date=datetime.now()),
# Jobs(name='Vicky Mathers', create_date=datetime.now())
# ])

session.commit()

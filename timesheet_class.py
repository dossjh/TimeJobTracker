import wx
from database import *
import re
from datetime import datetime, date, timedelta
from datetime import  time as dt_time
from utility_functions import pydate2wxdate
from message_class import *
from SubDatabaseWindow import *
from sqlalchemy.exc import IntegrityError, InvalidRequestError
from time import clock
from TextCtrlAutoComplete import mapDatatoChoices, TextCtrlAutoComplete

TIMESHEET_VALID = 0
TIMESHEET_NOTVALID = 1
TIMESHEET_NOTUSED = -1

class DynamicFilteredTextCrtl(TextCtrlAutoComplete):
    def __init__(self, panel, colNames, size):

        super(DynamicFilteredTextCrtl, self).__init__(panel, multiChoices=GlobalVars.dynamic_choices, colNames=colNames, colFetch=0, size=size)
        # self._ctrl = TextCtrlAutoComplete(panel, multiChoices=self.dynamic_choices, colFetch=0, size=size)

        # self._ctrl.SetMultipleChoices(self.dynamic_choices, colFetch = 0)
        self.SetEntryCallback(self.setDynamicChoices)
        self.SetMatchFunction(self.match)


    # def getCtrl(self):
    #     return self._ctrl

    def match(self, text, choice):
        '''
        Demonstrate "smart" matching feature, by ignoring http:// and www. when doing
        matches.
        '''
        t = text.lower()
        c = choice[0].lower()

        return c.startswith(t)

    def setDynamicChoices(self):
        # ctrl = self._ctrl
        text = self.GetValue().lower()
        current_choices = self.GetChoices()
        choices = [choice for choice in GlobalVars.dynamic_choices if self.match(text, choice)]
        if len(choices) < 1:
            self._showDropDown(False)
        else:
            if choices != current_choices:
                self.SetMultipleChoices(choices)

class TimeSheet_Hour(object):

    def __init__(self, parent, hour_id=None, row_id=None, current_date=None):
        # print 'creating hour object'
        self.parent = parent
        self.window = parent.parent
        self.panel = parent.panel
        self.row_id = row_id
        self.temp_Jobid = -1

        self.__date = None
        self.__time_start = None
        self.__time_finish = None
        self.__job = None
        self.__task = None
        self.__number_seconds = None

        if hour_id:
            self.Edit = True
            self.__hourObj__ = session.query(Hours).filter_by(id=hour_id).first()
            self.__date = self.__hourObj__.date
            self.__time_start = self.__hourObj__.time_start
            self.__time_finish = self.__hourObj__.time_finish
            self.__job = self.__hourObj__.Job
            self.__task = self.__hourObj__.Task
            self.__number_seconds = self.__hourObj__.number_seconds
        else:
            self.Edit = False
            self.__hourObj__ = Hours()



        self.current_date = current_date
        self.widget_time_start = None
        self.widget_time_finish = None
        self.widget_job = None
        self.widget_task = None
        self.Sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.InitUI()

    @property
    def date(self):
      return self.__date
    @date.setter
    def date(self, value):
        self.__date = value


    @property
    def number_seconds(self):
      return self.__number_seconds
    @number_seconds.setter
    def number_seconds(self, value):
      self.__number_seconds = value

    @property
    def task(self):
      return self.__task
    @task.setter
    def task(self, value):
        if isinstance(value, int):
            self.__hourObj__.task_id = session.query(TaskList).filter_by(id == vaule).first()
        elif isinstance(value, TaskList):
            self.__task = value
        else:
            MessageClass.Message('Failed to set Hour Task, wrong object type.', Duration=2000)


    @property
    def job(self):
      return self.__job
    @job.setter
    def job(self, value):
        if isinstance(value, int):
            self.__job = session.query(Jobs).filter_by(id=value).first()
        elif isinstance(value, Jobs):
            self.__job = value
        else:
            MessageClass.Message('Failed to set Hour Job, wrong object type.', Duration=2000)


    @property
    def time_start(self):
      return self.__time_start
    @time_start.setter
    def time_start(self, value):
        self.__time_start = value

    @property
    def time_finish(self):
      return self.__time_finish
    @time_finish.setter
    def time_finish(self, value):
      self.__time_finish = value

    def InitUI(self):

        #get our row id and increment it
        rowId = self.row_id

        # 0
        self.Sizer.Add(wx.StaticText(self.panel, label="Start:"), flag=wx.ALIGN_CENTER_VERTICAL)
        self.widget_time_start = wx.TextCtrl(self.panel, name=str(rowId)+':ROW_START', size=(53, -1), style=wx.TE_PROCESS_ENTER)
        self.widget_time_start.SetMaxLength(6)
        if self.Edit:
            self.widget_time_start.ChangeValue(self.time_start.strftime('%H%M'))
        # 1
        self.Sizer.Add(self.widget_time_start, flag=wx.ALIGN_CENTER_VERTICAL | wx.RIGHT, border=25)
        # 2
        self.Sizer.Add(wx.StaticText(self.panel, label="End:"), wx.ALIGN_CENTER_VERTICAL)
        self.widget_time_finish = wx.TextCtrl(self.panel, name=str(rowId)+':ROW_END', size=(53, -1), style=wx.TE_PROCESS_ENTER)
        self.widget_time_finish.SetMaxLength(6)
        if self.Edit:
            self.widget_time_finish.ChangeValue(self.time_finish.strftime('%H%M'))
        # 3
        self.Sizer.Add(self.widget_time_finish, flag=wx.ALIGN_CENTER_VERTICAL | wx.RIGHT, border=25)
        self.Sizer.Add(wx.StaticText(self.panel, label="Job:"), flag=wx.ALIGN_CENTER_VERTICAL | wx.RIGHT, border=5)
        # self.jobwidgetObject = DynamicFilteredTextCrtl(self.panel, size=(200, -1))
        colNames = ["Job Name", "Create Date", "Job Id"]
        self.widget_job = DynamicFilteredTextCrtl(self.panel, colNames=colNames, size=(250, -1))#self.jobwidgetObject.getCtrl()
        self.widget_job.SetSelectCallback(self.ChoiceChanged)


        if self.Edit:
            self.widget_job.SetValue(self.job.name)


        self.Sizer.Add(self.widget_job, flag=wx.ALIGN_CENTER_VERTICAL | wx.RIGHT, border=25)
        self.Sizer.Add(wx.StaticText(self.panel, label="Task:"), flag=wx.ALIGN_CENTER_VERTICAL)
        self.widget_task = wx.Choice(self.panel, id=wx.ID_ANY)
        for each in self.parent.widget_task_choices:
            self.widget_task.Append(each[0], each[1])

        if self.Edit:
            self.widget_task.SetSelection(self.widget_task.FindString(self.task.name))
        self.Sizer.Add(self.widget_task, flag=wx.ALIGN_CENTER_VERTICAL| wx.LEFT, border=3)

        clearButton = wx.Button(self.panel, style=wx.BU_EXACTFIT, label='Clear')
        self.Sizer.AddSpacer(15)
        self.Sizer.Add(clearButton, flag=wx.ALIGN_CENTER_VERTICAL)


        self.widget_task.Bind(wx.EVT_CHOICE, self.TaskChanged)
        self.widget_time_start.Bind(wx.EVT_KILL_FOCUS, self.onChangeTime)
        self.widget_time_start.Bind(wx.EVT_KEY_DOWN, self.keydownEVT)
        self.widget_time_finish.Bind(wx.EVT_KILL_FOCUS, self.onChangeTime)
        self.widget_time_start.Bind(wx.EVT_SET_FOCUS, self.UpdateShopHours)
        self.widget_time_finish.Bind(wx.EVT_KEY_DOWN, self.keydownEVT)
        clearButton.Bind(wx.EVT_BUTTON, self.ClearRow)
        clearButton.Bind(wx.EVT_SET_FOCUS, lambda e: e.GetEventObject().Navigate())

    def ClearRow(self, e):
        deleterow = True
        try:
            if self.Edit:
                session.delete(self.__hourObj__)
            else:
                session.expunge(self.__hourObj__)
        except InvalidRequestError:
            deleterow = False
            MessageClass.Message('Cannot delete unused row.')

        if deleterow:
            self.__hourObj__ = None
            self.__hourObj__ = Hours()
            self.widget_time_start.ChangeValue('')
            self.widget_time_finish.ChangeValue('')
            self.widget_job.SetSelection(wx.NOT_FOUND)
            self.widget_task.SetSelection(wx.NOT_FOUND)

    def TaskChanged(self, e):
        widget = self.widget_task
        index = widget.GetSelection()
        self.task = session.query(TaskList).filter_by(id=widget.GetClientData(index)).first()
        MessageClass.Debug(self.task.name)

    def RefreshTimes(self):
        if self.widget_time_start.GetValue() != '':
            self.time_start = datetime.combine(self.parent.GetDate(), datetime.strptime(self.widget_time_start.GetValue(), '%H%M').time())
            # MessageClass.Debug(self.time_start)
        if self.widget_time_finish.GetValue() != '':
            self.time_finish = datetime.combine(self.parent.GetDate(), datetime.strptime(self.widget_time_finish.GetValue(), '%H%M').time())
            # MessageClass.Debug(self.time_finish)

    def onChangeTime(self, e):
        TextBlank = e.GetEventObject()
        time = TextBlank.GetValue()
        processedtime = self.parent.processTime(time)
        if processedtime:
            TextBlank.ChangeValue(processedtime)
            RowDatetimeObj = datetime.combine(self.parent.GetDate(), datetime.strptime(processedtime, '%H%M').time())
            if 'ROW_START' in TextBlank.GetName():
                self.time_start = RowDatetimeObj
                MessageClass.Debug(self.time_start)
            if 'ROW_END' in TextBlank.GetName():
                #verify Row_End is after Row_Start:
                if self.time_start:
                    if not self.time_start < RowDatetimeObj:
                        #start time is not before finish time add 12 hours to see if that helps
                        RowDatetimeObj = RowDatetimeObj + timedelta(hours=12)
                        TextBlank.ChangeValue(RowDatetimeObj.strftime('%H%M'))
                        message = 'Time entered was before start time. \nAdjusted by 12 hours verify it is correct.'
                        MessageClass.Message(message, Caption='Invalid Time', Type=MESS_PANIC)
                self.time_finish = RowDatetimeObj
                MessageClass.Debug(self.time_finish)
        else:
            TextBlank.ChangeValue('')
        e.Skip()

    def keydownEVT(self, e):
        if e.GetKeyCode() == wx.WXK_RETURN or e.GetKeyCode() == wx.WXK_NUMPAD_ENTER:
            # print 'event called'
            obj = e.GetEventObject()
            objName = obj.GetName()
            rowId, objName = objName.split(':')
            newRowId = int(rowId)
            if objName == 'ROW_END':
                newRowId = int(rowId) + 1
                newObjName = 'ROW_START'
            if objName == 'ROW_START':
                newObjName = 'ROW_END'
            if newRowId > (len(self.parent.Rows) - 1):
                newRowId = 0
            self.parent.Rows[newRowId].SetFocus(newObjName)
        else:
            e.Skip()

    def SetFocus(self, objectname):
        if objectname == 'ROW_START':
            self.widget_time_start.SetFocus()
        if objectname == 'ROW_END':
            self.widget_time_finish.SetFocus()

    def ChoiceChanged(self, values):
        # print "calling ChoiceChanged", values
        # control = e.GetEventObject()
        # #verify index to avoid errors when no item selected
        # index = control.GetSelection()
        # if index != wx.NOT_FOUND:
        #     if control.GetStringSelection() == '[ Add New Job ]':
        #         e.Skip()
        #     else:
        self.job = session.query(Jobs).filter_by(id=int(values[2])).first()
        MessageClass.Debug(self.job.name)

    def UpdateChoiceJobs(self):
        self.widget_job.SetMultipleChoices(GlobalVars.dynamic_choices)

    def Validate(self, Show_Errors = True):
        ret_val = TIMESHEET_VALID
        # MessageClass.Debug('validating hour row')
        # MessageClass.Debug('times {} {}'.format(self.time_start,self.time_finish))
        if ret_val == TIMESHEET_VALID and not self.time_start and not self.time_finish:
            ret_val = TIMESHEET_NOTUSED
        else:
            #Check times are valid
            if self.time_start > self.time_finish:
                ret_val = TIMESHEET_NOTVALID


        if ret_val == TIMESHEET_VALID and not self.job:
            ret_val = TIMESHEET_NOTVALID
            if Show_Errors:
                MessageClass.Message('No job selected for row {}'.format(self.row_id+1), Type=MESS_PANIC, Caption='Validation Errors')

        if ret_val == TIMESHEET_VALID and not self.task:
            if self.widget_task.GetSelection() != wx.NOT_FOUND:
                self.TaskChanged(1)
            if not self.task:
                ret_val = TIMSHEET_NOTVALID
                if Show_Errors:
                    MessageClass.Message('No Task selected for row {}.'.format(self.row_id+1), Type=MESS_PANIC, Caption='Validation Error')

        return ret_val

    def SaveRow(self):
        RowStatus =  self.Validate()
        if RowStatus == TIMESHEET_VALID:
            # print 'adding Row'
            self.date = self.parent.GetDate()
            td_time = self.time_finish - self.time_start
            self.number_seconds = td_time.seconds
            self.__hourObj__.date = self.date
            self.__hourObj__.time_start = self.time_start
            self.__hourObj__.time_finish = self.time_finish
            self.__hourObj__.number_seconds = self.number_seconds
            self.__hourObj__.Task = self.task
            self.__hourObj__.Job = self.job

            self.parent.__timesheetObj__.Hours.append(self.__hourObj__)
        return RowStatus

    def UpdateShopHours(self, e):
        self.parent.ShowShopHours()
        e.Skip()

class TimeSheet_Class(object):
    def __init__(self, parent, panel, timesheet_id):
        #set Hour row counter to zero we have started a new timesheet
        TimeSheet_Hour.NextRow = 0
        self.panel = panel
        self.parent = parent
        self.Sizer = wx.BoxSizer(wx.HORIZONTAL)

        GlobalVars.dynamic_choices = map(mapDatatoChoices, session.query(Jobs.id, Jobs.name,Jobs.create_date).filter_by(active=True).all())

        self.__lunch = None
        self.__date = None
        self.__time_in = None
        self.__time_out = None
        self.__employee = None

        loadsheet_time1 = clock()
        if timesheet_id:
            self.__timesheetObj__ = session.query(TimeSheet).filter_by(id=timesheet_id).first()
            self.LoadTimeSheet()
            self.Edit = True
        else:
            self.__timesheetObj__ = TimeSheet()
            self.Edit = False

        loadsheet_time2 = clock()
        MessageClass.Debug('Timesheet Data Loaded in {} seconds'.format(loadsheet_time2 - loadsheet_time1))

        timeinit_1 = clock()
        self.InitUI()
        timeinit_2 = clock()

        MessageClass.Debug('Init Timesheet in {} seconds'.format(timeinit_2 - timeinit_1))

    def LoadTimeSheet(self):
        self.__date = self.__timesheetObj__.date
        self.__employee = self.__timesheetObj__.CrewName
        self.__time_in = self.__timesheetObj__.time_in
        self.__time_out = self.__timesheetObj__.time_out
        self.__lunch = self.__timesheetObj__.lunch


    @property
    def time_in(self):
      return self.__time_in
    @time_in.setter
    def time_in(self, value):
      self.__time_in = value

    @property
    def time_out(self):
      return self.__time_out
    @time_out.setter
    def time_out(self, value):
      self.__time_out = value

    @property
    def lunch(self):
      return self.__lunch
    @lunch.setter
    def lunch(self, value):
      self.__lunch = value


    @property
    def date(self):
      return self.__date
    @date.setter
    def date(self, value):
        self.__date = datetime.combine(value, dt_time(0,0,0))

    @property
    def employee(self):
        if self.__employee:
              return self.__employee
        else:
            # print 'Error no employee loaded'
            return None


    @employee.setter
    def employee(self, value):
      if isinstance(value, int):
        #   Number_TimeSheets = session.query(TimeSheet).join(CrewNames).filter(CrewNames.id == value).filter(TimeSheet.date == self.date).count()
        #   if Number_TimeSheets < 1:
              self.__employee = session.query(CrewNames).filter(CrewNames.id == value).one()
      if isinstance(value, CrewNames):
        #   Number_TimeSheets = session.query(TimeSheet).join(CrewNames).filter(CrewNames.id == value.id).filter(TimeSheet.date == self.date).count()
        #   if Number_TimeSheets < 1:
              self.__employee = value
    #   if Number_TimeSheets >= 1:
    #       MessageClass.Message('A Timesheet already exist for this employee on this date.', Type=MESS_PANIC)
    #       self.__employee = None
    #       self.Widget_employee.SetSelection(wx.NOT_FOUND)

    def InitUI(self):
        results = session.query(TaskList).order_by(TaskList.name).filter_by(active=True).all()
        self.widget_task_choices = list()
        for each in results:
            self.widget_task_choices.append((each.name, each.id))

        self.Widget_employee = wx.Choice(self.panel, id=wx.ID_ANY)
        if self.Edit:
            results = session.query(CrewNames).order_by(CrewNames.name).all()
        else:
            results = session.query(CrewNames).order_by(CrewNames.name).filter_by(active=True).all()
            
        for each in results:
            self.Widget_employee.Append(each.name, each.id)

        #set choice_chief
        #print self.timesheetObj.CrewName.name
        if self.Edit:
            # print "loading init employee"
            nameIndex = self.Widget_employee.FindString(self.employee.name)
            self.Widget_employee.SetSelection(nameIndex)


        employeelabel = wx.StaticText(self.panel, label='Name:')

        datelabel = wx.StaticText(self.panel, label="Date:")

        if self.Edit:
            dateValue = pydate2wxdate(self.date)
        else:
            dateValue = self.parent.newjobDate

        self.Widget_date = wx.DatePickerCtrl(self.panel, dt=dateValue, style=wx.DP_DROPDOWN)
        if not self.Edit:
            self.date = self.GetDate()
        starttimetext = wx.StaticText(self.panel, label="Time In:")

        if self.Edit:
            startValue = self.time_in.time().strftime('%H%M')
        else:
            startValue = '0745'
            self.time_in = datetime.combine(self.GetDate(), datetime.strptime(startValue, '%H%M').time())
            # print self.time_in

        self.starttime = wx.TextCtrl(self.panel, value=startValue, size=(53, -1))
        self.starttime.SetMaxLength(6)

        outtimetext = wx.StaticText(self.panel, label="Time Out:")

        if self.Edit:
            outValue = self.time_out.time().strftime('%H%M')
        else:
            outValue = '1630'
            self.time_out = datetime.combine(self.GetDate(), datetime.strptime(outValue, '%H%M').time())
            # print self.time_out
        self.outtime = wx.TextCtrl(self.panel, value=outValue, size=(53, -1))
        self.outtime.SetMaxLength(6)

        self.widget_lunch = wx.CheckBox(self.panel, label="Lunch? Minutes:")

        if self.Edit:
            if self.lunch > 0:
                self.widget_lunch.SetValue(1)
            else:
                self.widget_lunch.SetValue(0)
        else:
            self.lunch = 45
            self.widget_lunch.SetValue(1)
        self.widget_lunch_time = wx.TextCtrl(self.panel, value=str(self.lunch), size=(25,-1))
        self.widget_lunch_time.Enable(self.widget_lunch.GetValue())

        self.widget_shophours = wx.StaticText(self.panel, label='Shop Hours: No Data')

        # Add Top Row elements to Sizer
        self.Sizer.Add(starttimetext, flag=wx.RIGHT , border=4)
        self.Sizer.Add(self.starttime, flag=wx.RIGHT , border=24)
        self.Sizer.Add(outtimetext, flag=wx.RIGHT , border=4)
        self.Sizer.Add(self.outtime, flag=wx.RIGHT )
        self.Sizer.Add((50,0), proportion=1)
        self.Sizer.Add(employeelabel, flag=wx.RIGHT , border=4)
        self.Sizer.Add(self.Widget_employee, flag=wx.RIGHT , border=4)
        self.Sizer.Add(datelabel, flag=wx.RIGHT , border=4)
        self.Sizer.Add(self.Widget_date, flag=wx.RIGHT , border=24)
        self.Sizer.Add(self.widget_lunch)
        self.Sizer.Add(self.widget_lunch_time)


        self.Widget_date.Bind(wx.EVT_DATE_CHANGED, self.dateChanged)
        self.starttime.Bind(wx.EVT_KILL_FOCUS, self.onChangeInTime)
        self.outtime.Bind(wx.EVT_KILL_FOCUS, self.onChangeOutTime)
        self.Widget_employee.Bind(wx.EVT_KILL_FOCUS, self.onChangeEmployee)
        self.widget_lunch.Bind(wx.EVT_CHECKBOX, self.ChangeLunch)



        # add 8 Entry Rows maybe we can make this varable later
        self.Rows = dict()
        num_rows = 0

        init_time1 = clock()
        if self.Edit:
            for each in self.__timesheetObj__.Hours:
                self.Rows[num_rows] = TimeSheet_Hour(self, each.id, row_id=num_rows)
                num_rows += 1

        while num_rows < 10:
            self.Rows[num_rows] = TimeSheet_Hour(self, row_id=num_rows, current_date=self.Widget_date.GetValue().GetTicks())
            num_rows += 1
        init_time2 = clock()
        MessageClass.Debug('Init inside seconds: {}'.format(init_time2 - init_time1))


        self.ShowShopHours()

        self.Widget_employee.SetFocus()


    def ShowShopHours(self):
        my_timein = self.time_in
        my_timeout = self.time_out

        td_day = my_timeout - my_timein

        # print "total day td: ", td_day

        td_usedDay = timedelta()

        for each in self.Rows:
            if self.Rows[each].Validate(Show_Errors=False) == TIMESHEET_VALID:
                # MessageClass.Debug('Row is Valid')
                my_time_start = self.Rows[each].time_start
                my_time_finish = self.Rows[each].time_finish
                td_thisRow = my_time_finish - my_time_start
                # MessageClass.Debug('This Row Time: {}'.format(td_thisRow))
                td_usedDay += td_thisRow
                # MessageClass.Debug('Used Time Now: {}'.format(td_usedDay))

        ShopHours = td_day - td_usedDay
        if self.widget_lunch.GetValue():
            ShopHours = ShopHours - timedelta(minutes=int(self.widget_lunch_time.GetValue()))
        if ShopHours < timedelta(days=0): ShopHours = timedelta(days = 0)
        self.widget_shophours.SetLabel('Shop Hours: {}'.format(ShopHours))
        # MessageClass.Message('Shop Hours: {}'.format(ShopHours), Duration=0)


    def save(self):
        if self.employee:
            Number_TimeSheets = session.query(TimeSheet).join(CrewNames).filter(CrewNames.id == self.employee.id).filter(TimeSheet.date == self.date).count()
            if self.Edit:
                limit = 2
            else:
                limit = 1

            if Number_TimeSheets < limit:
                if not self.employee:
                    ValidSheet = False
                elif not self.date:
                    ValidSheet = False
                elif not self.time_in:
                    ValidSheet = False
                elif not self.time_out:
                    ValidSheet = False
                elif self.lunch == None:
                    ValidSheet = False
                else:
                    if self.AppendHours():
                        ValidSheet = True
                        self.__timesheetObj__.CrewName = self.employee
                        self.__timesheetObj__.date = self.date
                        # print self.__timesheetObj__.date
                        self.__timesheetObj__.time_in = self.time_in
                        self.__timesheetObj__.time_out = self.time_out
                        if self.widget_lunch.GetValue():
                            self.__timesheetObj__.lunch = int(self.widget_lunch_time.GetValue())
                        else:
                            self.__timesheetObj__.lunch = 0
                        print int(self.widget_lunch_time.GetValue())
                    else:
                        ValidSheet = False
            else:
                MessageClass.Message('A TimeSheet already exist for this Employee on this date.', Type=MESS_PANIC)
                ValidSheet = False

        return ValidSheet

    def AppendHours(self):
        ret_val = True
        for each in self.Rows:
            RowStatus =  self.Rows[each].SaveRow()
            if RowStatus == TIMESHEET_NOTVALID:
                ret_val = False
                break
        return ret_val

    def onChangeEmployee(self, e):
        print "changing Employee"
        widget = e.GetEventObject()
        index = widget.GetSelection()
        if index != wx.NOT_FOUND:
            try:
                self.employee = widget.GetClientData(index)
                if self.employee:
                    MessageClass.Debug(self.employee.name)
                session.flush()
            except IntegrityError:
                widget.SetSelection(wx.NOT_FOUND)
                session.rollback()
                MessageClass.Message('This Employee Already has a TimeSheet for this Date.', Type=MESS_PANIC)

    def ChangeLunch(self, e):
        # self.lunch = self.widget_lunch.GetValue()
        self.widget_lunch_time.Enable(self.widget_lunch.GetValue())

    def UpdateJobChoices(self):
        # print "called UpdateJobChoices"
        self.busyDlg = wx.BusyInfo("Please wait. Reloading Job Dropboxes.")
        results = session.query(Jobs.id,Jobs.name,Jobs.create_date)
        if self.parent.FilterInactive:
            results = results.filter_by(active=True)
        GlobalVars.dynamic_choices = None
        GlobalVars.dynamic_choices = map(mapDatatoChoices, results.all())
        myRows = self.Rows
        for each in myRows:
            myRows[each].UpdateChoiceJobs()
        self.busyDlg = None

    def dateChanged(self, e):
        self.parent.newjobDate = self.Widget_date.GetValue()
        self.date = self.GetDate()
        # print self.date

        #adjust times to new date
        self.time_in = datetime.combine(self.GetDate(), datetime.strptime(self.starttime.GetValue(), '%H%M').time())
        # print self.time_in
        self.time_out = datetime.combine(self.GetDate(), datetime.strptime(self.outtime.GetValue(), '%H%M').time())
        # print self.time_out

        #Change date part of hour times to match sheet date
        self.UpdateHourTimes()

    def GetDate(self):
        return datetime.fromtimestamp(self.Widget_date.GetValue().GetTicks()).date()

    def UpdateHourTimes(self):
        for each in self.Rows:
            self.Rows[each].RefreshTimes()

    def onChangeInTime(self, e):
        TextBlank = e.GetEventObject()
        time = TextBlank.GetValue()
        processedtime = self.processTime(time)
        TextBlank.ChangeValue(processedtime)
        self.time_in = datetime.combine(self.GetDate(), datetime.strptime(self.starttime.GetValue(), '%H%M').time())
        e.Skip()

    def onChangeOutTime(self, e):
        TextBlank = e.GetEventObject()
        time = TextBlank.GetValue()
        processedtime = self.processTime(time)
        TextBlank.ChangeValue(processedtime)
        self.time_out = datetime.combine(self.GetDate(), datetime.strptime(self.outtime.GetValue(), '%H%M').time())
        e.Skip()

    def processTime(self, time):
        itsaPM = False

        if 'p' in time or 'P' in time or 'pm' in time or 'PM' in time:
            itsaPM = True

        time = time.strip()
        time = re.sub('AM|am|A|a|pm|PM|P|p|:', '', time)

        if len(time) == 3:
            time = '0' + time

        hour_time = time[:2]
        min_time = time[2:]

        if itsaPM:
            hour_time = str(int(hour_time) + 12)

        time = hour_time + min_time
        try:
            datetime.strptime(time, '%H%M')
        except:
            # print 'not a time'
            time = False
        return time
